unit UCreateSql;

interface

uses system.SysUtils, FMX.Dialogs, System.StrUtils;

//const //ssss = '���,����,��,��1,��1,��2,��3,��4,��5,��6,��7 ��� ���=(361000);����=36:10:000000;��=0,1000;��1=true,1;��1=true,0;��2=true,0;��3=true,0;��4=true,0;��5=true,0;��6=true,0;��7=true,0;';
      //ssss = '����,��,����� ��� ���=(360100);��4=true,0;���=(1);';
      //ssss = '���,��, ��� ��� ���=(361400);���=(6);��=0,14.3;';
      //ssss = '����, ��, �����,��1,��2,��3,��4,��5,��6,��7 ��� ���=(360100); ������=(false,-,-,-);';
     //ssss = '����,��,�����,��1,��2,��3,��4,��5,��6,��7 ��� ���=(360100); ���=false; ���=(1);';
     //ssss = '���,����,��,����� ��� ���=(361400);���=(1,2);���=(5);';
     //ssss = '���,����,��,��1,��1,��2,��3,��4,��5,��6,��7 ��� �����=�(1,2)�(2,3);';
     //ssss = '���,����,��,��1,��1,��2,��3,��4,��5,��6,��7 ��� ����=�(1,2)�(2,3);';
var
  str_select, str_from, str_where : string;
  function CreateSqlbyString(myStr : string):string;

implementation
// uses UMain;

procedure CreateSelect(ss:string; var stSelect:string);
begin
  if trim(ss) = '���'  then stSelect := stSelect+'lv.Raion,'
  else
  if trim(ss) = '����' then stSelect := stSelect+'lv.cadnum,'
  else
  if trim(ss) = '��'   then stSelect := stSelect+'lv.larea,'
  else
  if trim(ss) = '���'  then stSelect := stSelect+'lv.KOLHOZ,'
  else
  if trim(ss) = '��'   then stSelect := stSelect+'lv.SETTLE,'
  else
  if trim(ss) = '��1'  then stSelect := stSelect+'lv.AGROUP,'
  else
  if trim(ss) = '��2'  then stSelect := stSelect+'lv.AGROUP2,'
  else
  if trim(ss) = '��3'  then stSelect := stSelect+'lv.AGROUP3,'
  else
  if trim(ss) = '��4'  then stSelect := stSelect+'lv.AGROUP4,'
  else
  if trim(ss) = '��5'  then stSelect := stSelect+'lv.AGROUP5,'
  else
  if trim(ss) = '��6'  then stSelect := stSelect+'lv.AGROUP6,'
  else
  if trim(ss) = '��7'  then stSelect := stSelect+'lv.AGROUP7,'
  else
  if trim(ss) = '��1'  then stSelect := stSelect+'lv.ATYPE,'
  else
  if trim(ss) = '��2'  then stSelect := stSelect+'lv.ATYPE2,'
  else
  if trim(ss) = '��3'  then stSelect := stSelect+'lv.ATYPE3,'
  else
  if trim(ss) = '��4'  then stSelect := stSelect+'lv.ATYPE4,'
  else
  if trim(ss) = '��5'  then stSelect := stSelect+'lv.ATYPE5,'
  else
  if trim(ss) = '��6'  then stSelect := stSelect+'lv.ATYPE6,'
  else
  if trim(ss) = '��7'  then stSelect := stSelect+'lv.ATYPE7,'
  else
  if trim(ss) = '���'  then stSelect := stSelect+'lv.CATEGORY,'
  else
  if trim(ss) = '�����'  then stSelect := stSelect+'lv.CADPRICE,'
  else
  if trim(ss) = '���'    then stSelect := stSelect+'lv.LUSENAME,'
  else
  if trim(ss) = '����'   then stSelect := stSelect+'lv.LFACTUAL_USE,'
//  else
//  if trim(ss) = '����'   then stSelect := stSelect+' SobName,'
  else
  if trim(ss) = '�����'  then stSelect := stSelect+'rt.Name RightName,'  //???????????
//  else
//  if trim(ss) = '������' then stSelect := stSelect+'''��'''+' Arenda ,'
//  else
//  if trim(ss) = '�����'  then stSelect := stSelect+' aSobName,'   //?/
//  else
//  if trim(ss) = '���'  then stSelect := stSelect+'Period,'   //?/
 // else
 // if Trim(ss) = '���' then  stSelect := stSelect+'Kuch,'
 // else
 // if Trim(ss) = '����' then  stSelect := stSelect+'UFRS,'
  else
    stSelect := stSelect+'';
end;

function CountPos(const subtext: string; Text: string): Integer;
begin
  if (Length(subtext) = 0) or (Length(Text) = 0) or (Pos(subtext, Text) = 0) then
    Result := 0
  else
    Result := (Length(Text) - Length(StringReplace(Text, subtext, '', [rfReplaceAll]))) div
      Length(subtext);
end;

procedure CreateWhere(ss:string; ss1: string; var stWhere:string; allSS:string);
var _s1,_s2, _s3:string; _i1, _i2 : integer;
begin
  if trim(ss) = '���'  then stWhere := stWhere+' and (lv.NID in '+ss1+')'
  else
  if trim(ss) = '����' then stWhere := stWhere+' and (lv.cadnum like '+ss1+'%'')'
  else
  if trim(ss) = '��'   then
     begin
       _s1 := Copy(ss1,1,Pos(',',ss1)-1);
       _s2 := Copy(ss1,Pos(',',ss1)+1,Length(ss1));
       if (_s1 = '0') and (_s2 <>'+') then stWhere := stWhere+ ' and (lv.Larea <'+_s2+')'
       else
       if (_s2 = '+') then stWhere := stWhere+ ' and (lv.larea >'+_s1+')'
       else
       stWhere := stWhere+' and (lv.larea >'+_s1+') and (lv.Larea <'+_s2+')'
     end
  else
  if trim(ss) = '���'  then stWhere := stWhere+' and (lv.KID in '+ss1+')'
  else
  if trim(ss) = '��'   then stWhere := stWhere+' and (lv.SID in '+ss1 +')'
  else
  if trim(ss) = '��1'  then
    begin
      _s1 := Copy(ss1,1,Pos(',',ss1)-1);
      _s2 := Copy(ss1,Pos(',',ss1)+1,Length(ss1));
      if _s2 = '1' then _s3 := ' and '
                   else _s3 := ' or ';
      if _s1 = 'true' then   stWhere := stWhere+_s3+' (lv.AGROUP<>0)'
                    else   stWhere := stWhere+_s3 +' (lv.AGROUP=0)'
    end
  else
  if trim(ss) = '��2'  then
    begin
      _s1 := Copy(ss1,1,Pos(',',ss1)-1);
      _s2 := Copy(ss1,Pos(',',ss1)+1,Length(ss1));
      if _s2 = '1' then _s3 := ' and '
                   else _s3 := ' or ';
      if _s1 = 'true' then   stWhere := stWhere+_s3+' (lv.AGROUP2<>0)'
                    else   stWhere := stWhere+_s3+' (lv.AGROUP2=0)'
    end
  else
  if trim(ss) = '��3'  then
    begin
      _s1 := Copy(ss1,1,Pos(',',ss1)-1);
      _s2 := Copy(ss1,Pos(',',ss1)+1,Length(ss1));
      if _s2 = '1' then _s3 := ' and '
                   else _s3 := ' or ';
      if _s1 = 'true' then   stWhere := stWhere+_s3+' (lv.AGROUP3<>0)'
                    else   stWhere := stWhere+_s3+' (lv.AGROUP3=0)'
    end
  else
  if trim(ss) = '��4'  then
    begin
      _s1 := Copy(ss1,1,Pos(',',ss1)-1);
      _s2 := Copy(ss1,Pos(',',ss1)+1,Length(ss1));
      if _s2 = '1' then _s3 := ' and '
                   else _s3 := ' or ';
      if _s1 = 'true' then   stWhere := stWhere+_s3+'(lv.AGROUP4<>0)'
                    else   stWhere := stWhere+_s3+'(lv.AGROUP4=0)'
    end
  else
  if trim(ss) = '��5'  then
    begin
      _s1 := Copy(ss1,1,Pos(',',ss1)-1);
      _s2 := Copy(ss1,Pos(',',ss1)+1,Length(ss1));
      if _s2 = '1' then _s3 := ' and '
                   else _s3 := ' or ';
      if _s1 = 'true' then   stWhere := stWhere+_s3+'(lv.AGROUP5<>0)'
                    else   stWhere := stWhere+_s3+'(lv.AGROUP5=0)'
    end
  else
  if trim(ss) = '��6'  then
    begin
      _s1 := Copy(ss1,1,Pos(',',ss1)-1);
      _s2 := Copy(ss1,Pos(',',ss1)+1,Length(ss1));
      if _s2 = '1' then _s3 := ' and '
                   else _s3 := ' or ';
      if _s1 = 'true' then   stWhere := stWhere+_s3+'(lv.AGROUP6<>0)'
                    else   stWhere := stWhere+_s3+'(lv.AGROUP6=0)'
    end

  else
  if trim(ss) = '��7'  then
    begin
      _s1 := Copy(ss1,1,Pos(',',ss1)-1);
      _s2 := Copy(ss1,Pos(',',ss1)+1,Length(ss1));
      if _s2 = '1' then _s3 := ' and '
                   else _s3 := ' or ';
      if _s1 = 'true' then   stWhere := stWhere+_s3+'(lv.AGROUP7<>0)'
                    else   stWhere := stWhere+_s3+'(lv.AGROUP7=0)'
    end
  else
  if trim(ss) = '��1'  then
    begin
      _s1 := Copy(ss1,1,Pos(',',ss1)-1);
      _s2 := Copy(ss1,Pos(',',ss1)+1,Length(ss1));
      if _s2 = '1' then _s3 := ' and '
                   else _s3 := ' or ';
      if _s1 = 'true' then   stWhere := stWhere+_s3+'(lv.ATYPE<>0)'
                    else   stWhere := stWhere+_s3+'(lv.ATYPE=0)'
    end
  else
  if trim(ss) = '��2'  then
    begin
      _s1 := Copy(ss1,1,Pos(',',ss1)-1);
      _s2 := Copy(ss1,Pos(',',ss1)+1,Length(ss1));
      if _s2 = '1' then _s3 := ' and '
                   else _s3 := ' or ';
      if _s1 = 'true' then   stWhere := stWhere+_s3+'(lv.ATYPE2<>0)'
                    else   stWhere := stWhere+_s3+'(lv.ATYPE2=0)'
    end
  else
  if trim(ss) = '��3'  then
    begin
      _s1 := Copy(ss1,1,Pos(',',ss1)-1);
      _s2 := Copy(ss1,Pos(',',ss1)+1,Length(ss1));
      if _s2 = '1' then _s3 := ' and '
                   else _s3 := ' or ';
      if _s1 = 'true' then   stWhere := stWhere+_s3+'(lv.ATYPE3<>0)'
                    else   stWhere := stWhere+_s3+'(lv.ATYPE3=0)'
    end
  else
  if trim(ss) = '��4'  then
    begin
      _s1 := Copy(ss1,1,Pos(',',ss1)-1);
      _s2 := Copy(ss1,Pos(',',ss1)+1,Length(ss1));
      if _s2 = '1' then _s3 := ' and '
                   else _s3 := ' or ';
      if _s1 = 'true' then   stWhere := stWhere+_s3+'(lv.ATYPE4<>0)'
                    else   stWhere := stWhere+_s3+'(lv.ATYPE4=0)'
    end
  else
  if trim(ss) = '��5'  then
    begin
      _s1 := Copy(ss1,1,Pos(',',ss1)-1);
      _s2 := Copy(ss1,Pos(',',ss1)+1,Length(ss1));
      if _s2 = '1' then _s3 := ' and '
                   else _s3 := ' or ';
      if _s1 = 'true' then   stWhere := stWhere+_s3+'(lv.ATYPE5<>0)'
                    else   stWhere := stWhere+_s3+'(lv.ATYPE5=0)'
    end
  else
  if trim(ss) = '��6'  then
    begin
      _s1 := Copy(ss1,1,Pos(',',ss1)-1);
      if _s2 = '1' then _s3 := ' and '
                   else _s3 := ' or ';
      _s2 := Copy(ss1,Pos(',',ss1)+1,Length(ss1));
      if _s1 = 'true' then   stWhere := stWhere+_s3+'(lv.ATYPE6<>0)'
                    else   stWhere := stWhere+_s3+'(lv.ATYPE6=0)'
    end
  else
  if trim(ss) = '��7'  then
    begin
      _s1 := Copy(ss1,1,Pos(',',ss1)-1);
      _s2 := Copy(ss1,Pos(',',ss1)+1,Length(ss1));
      if _s2 = '1' then _s3 := ' and '
                   else _s3 := ' or ';
      if _s1 = 'true' then   stWhere := stWhere+_s3+'(lv.ATYPE7<>0)'
                    else   stWhere := stWhere+_s3+'(lv.ATYPE7=0)'
    end
  else
  if trim(ss) = '���'  then stWhere := stWhere+' and (lv.LCID in ' + ss1+')'
  else
  if trim(ss) = '�����'  then
     begin
       _s1 := Copy(ss1,1,Pos(',',ss1)-1);
       _s2 := Copy(ss1,Pos(',',ss1)+1,Length(ss1));
       stWhere := stWhere+' and (lv.CADPRICE >'+_s1+') and (lv.CADPRICE <'+_s2+')'
     end
  else
  if trim(ss) = '���'    then stWhere := stWhere+' and (lv.LUSEID in ' + ss1+')'
  else
  if trim(ss) = '����'   then stWhere := stWhere+' and (lv.LFACTUAL_USE like '''+'%'+ss1+'%'')'
  else
  if trim(ss) = '����'   then
   begin
//      _s1 := Copy(allSS,1,Pos('���',allss));
      _s2 := Copy(allSS,Pos('���',allss)+3, Length(allss));
//      _i1 := countpos('�����',_s1);
      _i2 := CountPos('�����',_s2);
      if (_i2=0) then stWhere := stWhere + ' and (r.right_type_id <>7)' ;

     _s1 := '';
     _s2 := '';
     _i1 :=  Pos('�',ss1);
     _i2 :=  Pos('�',ss1);
     if ( _i1 <> 0) and (_i2 <>0) then
       begin
         _s1 := Copy(ss1,_i1+1,_i2-2);
         _s2 := Copy(ss1,_i2+1, Length(ss1));
       end
     else
       if _i1 <> 0 then  _s1 := Copy(ss1,2,Length(ss1))
       else
       if _i2 <> 0 then  _s2 := Copy(ss1,2,Length(ss1));

     if (_i1 <> 0) then stWhere := stWhere+' and (ro.enterprise_id in '+_s1+ ')';//+' ';
     if (_i2 <> 0) then stWhere := stWhere+' and (ro.person_id in '+_s2+')';//+' ';

    // stWhere := stWhere + ' and (r.right_type_id <>7)' ;
   end
  else
  if trim(ss) = '�����'  then
    begin
     //stWhere := stWhere+' ,' ;
     //stwhere := stwhere + 'and not exists(Select lrr.id from  t_land_right lrr left join t_right RR on (lrr.right_id = rr.id) where (rr.right_type_id = 7) and (lv.lid = lrr.land_id) )'
     //stwhere := stwhere + ' and lr.id in (Select lrr.id from  t_land_right lrr left join t_right RR on (lrr.right_id = rr.id) where (rr.right_type_id in '+ss1+ ') and (lr.id = lrr.id) and (lrr.status<>3) and (lrr.identify<>3) and (rr.status <> 3) and (rr.identify<>3))'
     stwhere := stwhere + ' and (r.right_type_id in '+ss1+ ')';
    end
  else
  if trim(ss) = '������' then
    begin
    if ss1 = 'true' then str_select := str_select +','+ '''��'''+' Arenda'
                    else str_select := str_select +','+ '''���'''+' Arenda' ;
//      stWhere := stWhere+' and(lv.lid = lr.land_id) ';
        _i1 := Pos(',',ss1);
        _s1 := Copy(ss1,_i1+1,posEx(',',ss1,_i1+1)-_i1-1);
        _i1 := posEx(',',ss1, _i1+1);
        _s2 := Copy(ss1,_i1+1,posEx(',',ss1,_i1+1)-_i1-1);
        _i1 := posEx(',',ss1, _i1+1);
        _s3 := Copy(ss1,_i1+1,posEx(',',ss1,_i1+1)-_i1-1);
//      if (_s1 <> '-') and (_s2 <> '-') then
//

      if Pos('false',ss1)<>0 then stwhere := stwhere + 'and not exists(Select lrr.id from  t_land_right lrr left join t_right RR on (lrr.right_id = rr.id) where (rr.right_type_id = 7) and (lv.lid = lrr.land_id) and (lrr.status<>3) and (lrr.identify<>3) and (rr.status <> 3) and (rr.identify<>3))'
                             else stwhere := stwhere + 'and exists(Select lrr.id from  t_land_right lrr left join t_right RR on (lrr.right_id = rr.id) where (rr.right_type_id = 7) and (lv.lid = lrr.land_id) and (lrr.status<>3) and (lrr.identify<>3) and (rr.status <> 3) and (rr.identify<>3))'
    end
  else
  if trim(ss) = '�����'  then
   begin
     //      _s1 := Copy(allSS,1,Pos('���',allss));
      _s2 := Copy(allSS,Pos('���',allss)+3, Length(allss));
//      _i1 := countpos('�����',_s1);
      _i2 := CountPos('������',_s2);
      if (_i2=0) then stWhere := stWhere + ' and (r.right_type_id =7)' ;

     _s1 := '';
     _s2 := '';
     _i1 :=  Pos('�',ss1);
     _i2 :=  Pos('�',ss1);
     if ( _i1 <> 0) and (_i2 <>0) then
       begin
         _s1 := Copy(ss1,_i1+1,_i2-2);
         _s2 := Copy(ss1,_i2+1, Length(ss1));
       end
     else
       if _i1 <> 0 then  _s1 := Copy(ss1,2,Length(ss1))
       else
       if _i2 <> 0 then  _s2 := Copy(ss1,2,Length(ss1));

     if (_i1 <> 0) then stWhere := stWhere+' and (ro.enterprise_id in '+_s1+')';//+' ';
     if (_i2 <> 0) then stWhere := stWhere+' and (ro.person_id in '+_s2+')';//+' ';
//     stWhere := stWhere + ' and (r.right_type_id =7)' ;
   end
  else
  if trim(ss) = '���' then
    begin
      // ����� ���������� ���������� ���������� ���� � ������
      if ss1 = 'true' then str_select := str_select +','+ '''��'''+' kuch'
                      else str_select := str_select +','+ '''���'''+' kuch' ;

    {  if Pos('t_land_right lrr', stWhere) = 0 // ����� �� ���� ������� � ��� ���� � ������� ����. �� ���� ��� ����� �� �����, ������� �� ������
      then
      begin
       stWhere := stwhere + ' and lr.id in (Select lrr.id from  t_land_right lrr left join t_right RR on (lrr.right_id = rr.id) where (rr.right_type_id <>7) and (lr.id = lrr.id) and (lrr.status<>3) and (lrr.identify<>3) and (rr.status <> 3) and (rr.identify<>3))'
      end;         }

   {   _i1 := Pos('lv.lid = lrr.land_id', stWhere)  ;
      if _i1 <> 0 then
       begin
        if ss1 = 'true' then stWhere := Copy(stWhere,1, _i1+length('lv.lid = lrr.land_id'))+ ' and  exists(Select id from t_right_registration_type Rrt where (rrt.registration_type_id =1) and (rrt.right_id = rr.id) and (rrt.status<>3) and (rrt.identify<>3))' + Copy(stWhere,_i1+length('lv.lid = lrr.land_id')+1, Length(stWhere))
                        else stWhere := Copy(stWhere,1, _i1+length('lv.lid = lrr.land_id'))+ ' and  not exists(Select id from t_right_registration_type Rrt where (rrt.registration_type_id =1) and (rrt.right_id = rr.id) and (rrt.status<>3) and (rrt.identify<>3))' + Copy(stWhere,_i1+length('lv.lid = lrr.land_id')+1, Length(stWhere));
       end
      else
        begin
          _i1 := Pos('lr.id = lrr.id', stWhere)  ;
          if _i1 <> 0 then
           begin
            if ss1 = 'true' then stWhere := Copy(stWhere,1, _i1+length('lr.id = lrr.id'))+ ' and  exists(Select id from t_right_registration_type Rrt where (rrt.registration_type_id =1) and (rrt.right_id = rr.id) and (rrt.status<>3) and (rrt.identify<>3))' + Copy(stWhere,_i1+length('lr.id = lrr.id')+1, Length(stWhere))
                            else stWhere := Copy(stWhere,1, _i1+length('lr.id = lrr.id'))+ ' and  not exists(Select id from t_right_registration_type Rrt where (rrt.registration_type_id =1) and (rrt.right_id = rr.id) and (rrt.status<>3) and (rrt.identify<>3))' + Copy(stWhere,_i1+length('lr.id = lrr.id')+1, Length(stWhere));
           end
        end;      }
     // if (Pos('���=true',allss)=0) or() then
      if ((Pos('����=true', allss) <> 0) And (Pos('���=true', allss) <> 0)) or ((Pos('����=false', allss) <> 0) And (Pos('���=false', allss) <> 0)) then begin  end
      else
      begin
      if (Pos('r.right_type_id in',stWhere) =0) and (Pos('and (r.right_type_id <>7)',stWhere)=0) then
          stWhere :=stWhere +  'and (r.right_type_id <>7)';
      if ss1 = 'true' then stWhere :=stWhere +' and (rrt.registration_type_id =1)'
                     else stWhere :=stWhere +' and  not exists(Select id from t_right_registration_type Rrt2 where (rrt2.registration_type_id =1) and (rrt2.right_id = r.id) and (rrt2.status<>3) and (rrt2.identify<>3))';
      end;
    end
  else
  if trim(ss) = '����' then
    begin
     // ����� ���������� ���������� ���������� ���� � ������
      if ss1 = 'true' then str_select := str_select +','+ '''��'''+' ufrs'
                      else str_select := str_select +','+ '''���'''+' ufrs' ;

 {    if Pos('t_land_right lrr', stWhere) = 0 // ����� �� ���� ������� � ��� ���� � ������� ����. �� ���� ��� ����� �� �����, ������� �� ������
      then
      begin
       stWhere := stwhere + ' and exists(Select lrr.id from  t_land_right lrr left join t_right RR on (lrr.right_id = rr.id) where (rr.right_type_id <>7) and (lr.id = lrr.id) and (lrr.status<>3) and (lrr.identify<>3) and (rr.status <> 3) and (rr.identify<>3)'
      end;        }

 {   _i1 := Pos('lv.lid = lrr.land_id', stWhere)  ;
    if _i1 <> 0 then
      begin
      if ss1 = 'true' then stWhere := Copy(stWhere,1, _i1+length('lv.lid = lrr.land_id'))+ ' and  exists(Select id from t_right_registration_type Rrt where (rrt.registration_type_id =2) and (rrt.right_id = rr.id) and (rrt.status<>3) and (rrt.identify<>3))' + Copy(stWhere,_i1+length('lv.lid = lrr.land_id')+1, Length(stWhere))
                      else stWhere := Copy(stWhere,1, _i1+length('lv.lid = lrr.land_id'))+ ' and  not exists(Select id from t_right_registration_type Rrt where (rrt.registration_type_id =2) and (rrt.right_id = rr.id) and (rrt.status<>3) and (rrt.identify<>3))' + Copy(stWhere,_i1+length('lv.lid = lrr.land_id')+1, Length(stWhere));
      end
    else
      begin
        _i1 := Pos('lr.id = lrr.id', stWhere)  ;
        if _i1 <> 0 then
         begin
          if ss1 = 'true' then stWhere := Copy(stWhere,1, _i1+length('lr.id = lrr.id'))+ ' and  exists(Select id from t_right_registration_type Rrt where (rrt.registration_type_id =2) and (rrt.right_id = rr.id) and (rrt.status<>3) and (rrt.identify<>3))' + Copy(stWhere,_i1+length('lr.id = lrr.id')+1, Length(stWhere))
                          else stWhere := Copy(stWhere,1, _i1+length('lr.id = lrr.id'))+ ' and  not exists(Select id from t_right_registration_type Rrt where (rrt.registration_type_id =2) and (rrt.right_id = rr.id) and (rrt.status<>3) and (rrt.identify<>3))' + Copy(stWhere,_i1+length('lr.id = lrr.id')+1, Length(stWhere));
         end
      end  ;     }
      if ((Pos('����=true', allss) <> 0) And (Pos('���=true', allss) <> 0)) or ((Pos('����=false', allss) <> 0) And (Pos('���=false', allss) <> 0)) then begin  end
      else
      begin
       if (Pos('r.right_type_id in',stWhere) =0) and (Pos('and (r.right_type_id <>7)',stWhere)=0) then
       stWhere :=stWhere +  'and (r.right_type_id <>7)';
       if ss1 = 'true' then begin stWhere :=stWhere +' and (rrt.registration_type_id =2)' end
                       else stWhere :=stWhere +' and  not exists(Select id from t_right_registration_type Rrt2 where (rrt2.registration_type_id =2) and (rrt2.right_id = r.id) and (rrt2.status<>3) and (rrt2.identify<>3))';
      end;
    end
  else
  if Trim(ss)= '���' then
  begin
   if Pos('������', allss) <> 0 then

   if Pos('0.',ss1)=0 then
    begin
      _i1 := Pos('(rr.right_type_id = 7)',stWhere);
      _i2 := _i1+ 21;
      _s1 := Copy(stWhere,1,_i2);
      _s2 := Copy(stWhere,_i2+1, Length(stWhere));
      stWhere:= _s1+ ' and (((rr.right_begin_date  is not null) and (rr.right_end_date  is not null) and (coalesce(rr.RIGHT_END_DATE-RIGHT_BEGIN_DATE,0)/366='+ss1+'))';
      stWhere:= stWhere + ' or ((rr.right_begin_date is not null) and (rr.right_end_date is null) and (((rr.PERIOD=12*'+ss1+') and (rr.UNIT_TYPE_id=120)) or ((rr.period='+ss1;
      stWhere:= stWhere + ') and (rr.unit_type_id=123)) or (((rr.period =366*'+ss1+') and (rr.unit_type_id =117))) ))';
      stWhere:= stWhere + ' or ((rr.right_begin_date is null) and (rr.right_end_date is null) and (((rr.PERIOD=12*'+ss1+') and (rr.UNIT_TYPE_id=120)) or ((rr.period='+ss1+')';
      stWhere:= stWhere + ' and (rr.unit_type_id=123)) or ((rr.period=366*'+ss1+') and (rr.unit_type_id =117)))))'+_s2;
    end
   else
    begin
      ss1 := Copy(ss1,3,Length(ss1));
      _i1 := Pos('(rr.right_type_id = 7)',stWhere);
      _i2 := _i1+ 21;
      _s1 := Copy(stWhere,1,_i2);
      _s2 := Copy(stWhere,_i2+1, Length(stWhere));
      stWhere:= _s1+ ' and (((rr.right_begin_date  is not null) and (rr.right_end_date  is not null) and (coalesce(rr.RIGHT_END_DATE-RIGHT_BEGIN_DATE,0)/30='+ss1+'))';
      stWhere:= stWhere + ' or ((rr.right_begin_date is not null) and (rr.right_end_date is null) and (((rr.PERIOD='+ss1+') and (rr.UNIT_TYPE_id=120)) or ((rr.period='+ss1+'/12';
      stWhere:= stWhere + ') and (rr.unit_type_id=123)) or (((rr.period ='+ss1+'/30) and (rr.unit_type_id =117))) ))';
      stWhere:= stWhere + ' or ((rr.right_begin_date is null) and (rr.right_end_date is null) and (((rr.PERIOD='+ss1+') and (rr.UNIT_TYPE_id=120)) or ((rr.period='+ss1+'/12)';
      stWhere:= stWhere + ' and (rr.unit_type_id=123)) or ((rr.period='+ss1+'/30) and (rr.unit_type_id =117))))) '+_s2;
    end
   else
    ShowMessage('�� �� ������� ������, ������� ������ ������ � ������ �� ����� �������!');
  end
  else
    stWhere := stWhere+'';
end;



function CreateSqlbyString(myStr : string):string;
var  i,i1, i2,i3, _i4 : Integer;
     ssBlock,ss2,ss3,ss4, ss5 , ss6, ss7, stf,sts,stw,stf2,sts2,stw2, _s1,_s2,ssSobName: string;
     Fl1, fl2, _ff : Boolean;
   //  str_select, str_from, str_where : string;
begin
  str_select :='';
  str_from   :='';
  str_where  :='';
  sts := '';
  stf := '';
  stw := '';

  i := Pos('���', myStr);
  if i <> 0 then
    begin
      ssBlock := Copy(myStr,1,i-1); // for select
      fl1 := False;
      i2 := 1;
      // select
      while not fl1 do
      begin
        if i2 = 1 then i1 := Pos(',',ssBlock)
                  else i1 := PosEx(',',ssBlock,i2+1);
        if i1 = 0 then Fl1 := True
        else
          begin
            if i2 = 1 then ss2 := Trim(Copy(ssBlock, i2, i1-i2))
                      else ss2 := Trim(Copy(ssBlock, i2+1, i1-i2-1));
            i2 := i1;

            CreateSelect(ss2,ss3);
          end;
      end;
       if ((ssBlock[1]='''') and (i2=1)) or (i2 <> 1)
         then   ss2 := Trim(Copy(ssBlock,i2+1, Length(ssBlock)))
         else   ss2 := Trim(Copy(ssBlock,i2, Length(ssBlock))) ;
       CreateSelect(ss2,ss3);
       ss3 := trim(ss3);
       if ss3[length(ss3)] = ',' then ss2 := Copy(ss3,1,Length(ss3)-1);
       str_select := 'select '+ss2;

     // from
       str_from := ' from T_TEMP_LANDVIEW LV ';//' from LandView LV ';
       if ((Pos('������', myStr) <> 0) or (Pos('�����', myStr) <> 0) or (Pos('����', myStr) <> 0) or (Pos('���', myStr) <> 0)  or (Pos('�����', myStr) <> 0) or (Pos('����', myStr) <> 0))
           and (Pos('t_land_right lr', myStr) = 0)
         then
           begin
             str_from  := str_from + ', t_land_right lr';
             str_Where := str_Where+' where (lv.lid = lr.land_id)';
             str_Where := str_Where+' and (lr.status<>3) and (lr.identify<>3)'
           end;

       if (Pos('�����', myStr) <> 0) or (Pos('������', myStr) <> 0)
         then
           begin
             str_from  := str_from + ', t_right r, t_right_type rt';
             str_Where := str_Where+' and (lr.right_id = r.id) and (r.right_type_id = rt.id)';
             str_Where := str_Where+' and (r.status<>3) and (r.identify<>3)';
           end;

       if (Pos('����', myStr) <> 0) or (Pos('���', myStr) <> 0)
         then
           begin
             if Pos(', t_right r', str_from)=0 then
              begin
                str_from  := str_from + ', t_right r';
                str_Where := str_Where+' and (lr.right_id = r.id)';
                str_Where := str_Where+' and (r.status<>3) and (r.identify<>3)';
              end;
              if (Pos('����=true', myStr) <> 0) or (Pos('���=true', myStr) <> 0) then
              begin
                str_from  := str_from + ', t_right_registration_type Rrt';
                str_Where := str_Where+' and (rrt.right_id = r.id)';
                str_Where := str_Where+' and (rrt.status<>3) and (rrt.identify<>3)';
              end;

           end;

       if (Pos('�����', myStr) <> 0) or (Pos('����', myStr) <> 0)
         then
           begin
             if Pos('t_right r', str_from)<>0
               then
                 begin
                   str_from  := str_from + ', t_right_owner ro';
                   str_Where := str_Where+' and (ro.land_right_id = lr.id) and (ro.status <> 3) and (ro.identify <> 3)';
                 end
               else
             if (Pos('t_land_right lr', str_From) <> 0)
               then
                 begin
                   str_from  := str_from + ', t_right r, t_right_owner ro';
                   str_Where := str_Where+'and (lr.right_id = r.id) and (ro.land_right_id = lr.id) ';
                   str_Where := str_Where+' and (r.status<>3) and (r.identify<>3) and (ro.status <> 3) and (ro.identify <> 3)'
                 end
               else
                 begin
                   str_from  := str_from + ', t_land_right lr, t_right r, t_right_owner ro';
                   str_Where := str_Where+' and (lv.lid = lr.land_id) and (lr.right_id = r.id) and (ro.land_right_id = lr.id) ';
                   str_Where := str_Where+' and (lr.status<>3) and (lr.identify<>3) and (r.status<>3) and (r.identify<>3) and (ro.status <> 3) and (ro.identify <> 3)' ;
                 end
           end;

     // where
     ssBlock := Copy(myStr,i+3,Length(myStr)); // for where

     Fl1 := False;
     i2 := 1;
     while not Fl1  do
     begin
       if i2=1  then i1 := Pos(';',ssBlock)
                else i1 := PosEx(';',ssBlock,i2+1);
       if i1=0  then Fl1 := True
       else
       begin
        if i2 = 1 then ss2 := Trim(Copy(ssBlock,i2,i1-i2))
                  else ss2 := Trim(Copy(ssBlock,i2+1,i1-i2-1));

        ss3 := Copy(ss2,1,Pos('=',ss2)-1);          // �� �����
        ss4 := Copy(ss2,Pos('=',ss2)+1,Length(ss2));// ����� �����
        CreateWhere(ss3,ss4,ss5,myStr);
        //--- ��� ������������
       end;

       i2 := i1;
     end;

      if (Pos('����=true', myStr) <> 0) And (Pos('���=true', myStr) <> 0) then
        begin
          str_Where := str_Where+' and 2=(Select count(id) from t_right_registration_type Rrt2 where (rrt2.right_id = r.id) and (rrt2.status<>3) and (rrt2.identify<>3))'
        end
      else
      if (Pos('����=false', myStr) <> 0) And (Pos('���=false', myStr) <> 0) then
        begin
          str_Where := str_Where+' and 0=(Select count(id) from t_right_registration_type Rrt2 where (rrt2.right_id = r.id) and (rrt2.status<>3) and (rrt2.identify<>3))'
        end  ;

     // ������ and ����� where
     // if (Pos('������', myStr) = 0) and (Pos('�����', myStr) = 0) and (Pos('����', myStr) =0) and (Pos('���', myStr) =0)
     // then  ss5 := Copy(ss5,5, Length(ss5));

      // ����� ������ ���������� ������ OR �� AND
      i3 := Pos('or',ss5);
      i1 := Pos('lv.AGROUP',ss5);
      if (i1-i3 <6) and (i1-i3 > 1) then
      begin
        _s1 := Copy(ss5,1,i3-1);
        _s2 := Copy(ss5,i3+2,Length(ss5));
        ss5 := _s1+'and'+_s2;
      end;
     // ����������� ������ 1
     i3 := CountPos('lv.AGROUP',ss5);
      if i3 > 1
        then
        begin
          i1 := Pos('lv.AGROUP',ss5);
          ss5 := Copy(ss5,1,i1-1) + '('+copy(ss5,i1, Length(ss5));
          i1:= i1+3;
          for I := 2 to i3 do
          begin
           i2 := PosEx('lv.AGROUP',ss5, i1+1);
           i1 := i2;
          end;
          i1 := PosEx('or', ss5, i2+1);     // �������� ���� �� ��� ������� ��� ����� ������
          i2 := PosEx('and', ss5, i2+1);
          if i1 <> 0 then  ss5 := Copy(ss5,1,i1-1) + ')'+copy(ss5,i1, length(ss5))
          else
          if i2 <> 0 then  ss5 := Copy(ss5,1,i2-1) + ')'+copy(ss5,i2, length(ss5))
          else  ss5 := ss5 +')';
        end;



      // ����� ������ ���������� ������ OR �� AND
      _ff := False;
      i1 := Pos('lv.ATYPE',ss5);
      i3 := Pos('or',ss5);
      while not _ff do
      begin
        if (i1-i3 <6) and (i1-i3 > 1) then
        begin
          _s1 := Copy(ss5,1,i3-1);
          _s2 := Copy(ss5,i3+2,Length(ss5));
          ss5 := _s1+'and'+_s2;
         _ff := True;
        end;
        i3 := PosEx('or',ss5, i3+1);
        if i3 =0 then _ff:=True;
      end;
      // ����������� ������ 2
      i3 := CountPos('lv.ATYPE',ss5);
      if i3 > 1
        then
        begin
          i1 := Pos('lv.ATYPE',ss5);
          ss5 := Copy(ss5,1,Pos('lv.ATYPE',ss5)-1) + '('+copy(ss5,Pos('lv.ATYPE',ss5), Length(ss5));
          i1:= i1+3;
          for I := 2 to i3 do
          begin
           i2 := PosEx('lv.ATYPE',ss5, i1+1);
           i1 := i2;
          end;
          i1 := PosEx('or', ss5, i2+1);     // �������� ���� �� ��� ������� ��� ����� ������
          i2 := PosEx('and', ss5, i2+1);
        {  if i1 <> 0 then  ss5 := Copy(ss5,1,i1-1) + ')'+copy(ss5,i1, length(ss5))
          else
          if i2 <> 0 then  ss5 := Copy(ss5,1,i2-1) + ')'+copy(ss5,i2, length(ss5))
          else  ss5 := ss5 +')';   }

          if (i2<>0) and (i1<>0) then
             if i2<i1 then ss5 := Copy(ss5,1,i2-1) + ')'+copy(ss5,i2, length(ss5))
                      else ss5 := Copy(ss5,1,i1-1) + ')'+copy(ss5,i1, length(ss5))
          else
           if i2<>0 then ss5 := Copy(ss5,1,i2-1) + ')'+copy(ss5,i2, length(ss5))
           else
           if i1<>0 then ss5 := Copy(ss5,1,i1-1) + ')'+copy(ss5,i1, length(ss5))
           else
            ss5 := ss5 +')';

        end;


     // if (Pos('������', myStr) = 0) and (Pos('�����', myStr) = 0) and (Pos('����', myStr) =0) and (Pos('���', myStr) =0)
     // then  str_where := ' Where '+ss5
     // else
      str_where := str_where + ss5;

      if (Pos(LowerCase('Where'), LowerCase(str_where)) =0) then str_where := ' Where'+str_where;


      Fl1 := False;
      i1 := Pos(' (ro.enterprise_id in',str_where);
      i2 := Pos(' (ro.person_id in',str_where);
      Fl1 := ((i1<>0) and (i2<>0));
      if Pos('�����', myStr)<>0 then ssSobName := 'aSobName'
       else
        if Pos('����', myStr)<>0 then ssSobName := 'SobName';

      // � ������ ��������� � ��������� �� ������������ � ���������� - ���� ����� UNION
      if fl1 then
      begin

       sts := str_select + ', ee.full_name  '+ssSobName;
       sts2 := str_select + ', coalesce(pp.FAMILY, '''')||'+''' '''+ '||coalesce(pp.NAME, '''')||'+''' '''+'||coalesce(pp.LAST_NAME, '''') '+ssSobName ;   //',   pp.family||'+' '+ '||pp.name||'+' '+'||pp.last_name SobName';
       stf := str_from + ', t_enterprise ee ';
       stf2 := str_from + ', t_person pp ';
       if i1 < i2 then
                   begin
                     // �������� ������� - � �����
                     ss2 := Copy(str_where,1,i1-1);
                     if Copy(ss2, Length(ss2)-2,3)='and' then ss2 := Copy(ss2,1,Length(ss2)-3);

                     ss4 := Copy(str_where, i1, i2-i1-3);
                     ss3 := Copy(str_where, i2, Length(str_where));
                     if Pos('and', ss4)<> 0 then  stw := ss2+ ss4
                                            else  stw := ss2 +' and '+ ss4;         //��
                     if Pos('and', ss3)<> 0 then  stw2 := ss2 + ss3
                                            else  stw2 := ss2 + ' and ' +ss3;       //��
                     stw2 := stw2 + ' and (pp.id = ro.person_id) ' ;
                     stw := stw + ' and (ee.id = ro.enterprise_id) ';
                   end
                  else  // �������� ������� - ������ ������
                    begin
                     ss2 := Copy(str_where,1,i1-1);
                     if Copy(ss2, Length(ss2)-2,3)='and' then ss2 := Copy(ss2,1,Length(ss2)-3);
                     ss4 := Copy(str_where, i1, i2-i1-1);
                     ss3 := Copy(str_where, i2, Length(str_where));
                     if Pos('and', ss3)<> 0 then stw := ss2+ ss3
                                            else stw := ss2+' and '+ ss3;          //��
                     if Pos('and', ss4)<> 0 then stw2 := ss2 + ss4
                                            else stw2 := ss2 + ' and ' +ss4;       //��
                     stw2 := stw2 + ' and (pp.id = ro.person_id) ' ;
                     stw := stw + ' and (ee.id = ro.enterprise_id) ';
                    end ;

      end
      else
       if (i1<>0)
         then
           begin
             sts := str_select + ',  ee.full_name  '+ssSobName;
             stf := str_from + ' , t_enterprise ee ';
             stw := str_where + ' and (ee.id = ro.enterprise_id) ';
           end
         else
         if (i2<>0)
         then
          begin
             sts2 := str_select + ', coalesce(pp.FAMILY, '''')||'+''' '''+ '||coalesce(pp.NAME, '''')||'+''' '''+'||coalesce(pp.LAST_NAME, '''') '+ssSobName;
             stf2 := str_from + ' , t_person pp ';
             stw2 := str_where + ' and (pp.id = ro.person_id) ' ;
          end
         else
          if ((pos('�����', myStr)<>0) and (Pos('�����=', myStr)=0))  or ((pos('����', myStr)<>0) and (Pos('����=', myStr)=0))   // ������, ����� ��� ����� � �������� �� ���� ���
          then
            begin
              // ��� ��� �������� � ������� ��� - �� ����� �����
              sts := str_select + ', ee.full_name  '+ssSobName;
              sts2 := str_select + ', coalesce(pp.FAMILY, '''')||'+''' '''+ '||coalesce(pp.NAME, '''')||'+''' '''+'||coalesce(pp.LAST_NAME, '''') '+ssSobName ;   //',   pp.family||'+' '+ '||pp.name||'+' '+'||pp.last_name SobName';
              stf := str_from + ', t_enterprise ee ';
              stf2 := str_from + ', t_person pp ';
              stw2 := str_where + ' and (pp.id = ro.person_id) ' ;
              stw := str_where + ' and (ee.id = ro.enterprise_id) ';
            end ;


    // where and/or
    if str_where<>'' then
     begin
       i1 := Pos('and', str_where);
       if (i1 > 6) and (i1 <10 ) then
        str_where  := Copy(str_where,1,i1-1)+copy(str_where,i1+3, Length(str_where));
     end;
     if str_where<>'' then
     begin
       i1 := Pos('or', str_where);
       if (i1 > 6) and (i1 <10 ) then
        str_where  := Copy(str_where,1,i1-1)+copy(str_where,i1+2, Length(str_where));
     end;
    if stw<>'' then
     begin
       i1 := Pos('and', stw);
       if (i1 > 6) and (i1 <10 ) then
        stw  := Copy(stw,1,i1-1)+copy(stw,i1+3, Length(stw));
     end;
    if stw2<>'' then
     begin
       i1 := Pos('and', stw2);
       if (i1 > 6) and (i1 <10 ) then
        stw2  := Copy(stw2,1,i1-1)+copy(stw2,i1+3, Length(stw2));
     end;
     if stw<>'' then
     begin
       i1 := Pos('or', stw);
       if (i1 > 6) and (i1 <10 ) then
        stw  := Copy(stw,1,i1-1)+copy(stw,i1+2, Length(stw));
     end;
    if stw2<>'' then
     begin
       i1 := Pos('or', stw2);
       if (i1 > 6) and (i1 <10 ) then
        stw2  := Copy(stw2,1,i1-1)+copy(stw2,i1+2, Length(stw2));
     end;

   if (sts<>'') and (stf<>'') and (stw<>'') and (sts2<>'') and (stf2<>'') and (stw2<>'') then
     Result := sts + stf+ stw + ' UNION '+ sts2+stf2+stw2
   else
   if (sts<>'') and (stf<>'') and (stw<>'') and (sts2='') and (stf2='') and (stw2='') then
     Result := sts + stf+ stw
   else
   if (sts='') and (stf='') and (stw='') and (sts2<>'') and (stf2<>'') and (stw2<>'') then
     Result := sts2 + stf2+ stw2
   else
     Result := str_select + str_from+ str_where;
  end
  else
    begin
      Showmessage('Error! ���������� ������� � sql - ������ �� �����');
      Result :='';
    end;
end;


end.
