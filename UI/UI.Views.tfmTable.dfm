object fmTable: TfmTable
  Left = 0
  Top = 0
  Caption = #1055#1088#1086#1089#1084#1086#1090#1088' '#1088#1077#1079#1091#1083#1100#1090#1072#1090#1072
  ClientHeight = 654
  ClientWidth = 1021
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object spl1: TSplitter
    Left = 982
    Top = 0
    Height = 619
    Align = alRight
    OnMoved = spl1Moved
    ExplicitLeft = 992
    ExplicitTop = 152
    ExplicitHeight = 100
  end
  object dbgrdResult: TDBGrid
    Left = 0
    Top = 0
    Width = 982
    Height = 619
    Align = alClient
    DataSource = dm.dsResult
    DrawingStyle = gdsGradient
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Calibri'
    Font.Style = []
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
    ParentFont = False
    PopupMenu = pmResult
    TabOrder = 0
    TitleFont.Charset = RUSSIAN_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -15
    TitleFont.Name = 'Calibri'
    TitleFont.Style = [fsBold]
    OnCellClick = dbgrdResultCellClick
    OnDrawColumnCell = dbgrdResultDrawColumnCell
    OnTitleClick = dbgrdResultTitleClick
  end
  object pnlTools: TPanel
    Left = 0
    Top = 619
    Width = 1021
    Height = 35
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object btnNext: TButton
      Left = 866
      Top = 0
      Width = 155
      Height = 35
      Align = alRight
      BiDiMode = bdRightToLeft
      Caption = 'btnNext'
      ImageAlignment = iaRight
      ImageIndex = 1
      Images = fmImages.img32
      ParentBiDiMode = False
      TabOrder = 0
      WordWrap = True
      OnClick = btnNextClick
    end
    object btnBack: TButton
      Left = 0
      Top = 0
      Width = 155
      Height = 35
      Align = alLeft
      Caption = 'btnBack'
      ImageIndex = 0
      Images = fmImages.img32
      TabOrder = 1
      WordWrap = True
      OnClick = btnBackClick
    end
  end
  object pnl1: TPanel
    Left = 985
    Top = 0
    Width = 23
    Height = 619
    Align = alRight
    DockSite = True
    TabOrder = 2
    OnDockDrop = pnl1DockDrop
    OnDockOver = pnl1DockOver
    OnGetSiteInfo = pnl1GetSiteInfo
    OnResize = pnl1Resize
    OnUnDock = pnl1UnDock
  end
  object dcktbst1: TDockTabSet
    Left = 1008
    Top = 0
    Width = 13
    Height = 619
    Align = alRight
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    Style = tsModernTabs
    TabPosition = tpRight
    DestinationDockSite = pnl1
    OnDockDrop = dcktbst1DockDrop
    OnGetSiteInfo = dcktbst1GetSiteInfo
    OnTabAdded = dcktbst1TabAdded
    OnTabRemoved = dcktbst1TabRemoved
  end
  object pmResult: TPopupMenu
    Left = 920
    Top = 112
    object N6: TMenuItem
      Caption = #1055#1086#1089#1084#1086#1090#1088#1077#1090#1100' '#1091#1095#1072#1089#1090#1086#1082
      OnClick = N6Click
    end
    object N1: TMenuItem
      Caption = #1054#1090#1082#1088#1099#1090#1100' '#1082#1072#1088#1090#1091' '#1085#1072' '#1087#1086#1088#1090#1072#1083#1077
      Visible = False
      OnClick = N1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Excel1: TMenuItem
      Caption = #1069#1082#1089#1087#1086#1088#1090' '#1074' Excel'
      object N8: TMenuItem
        Caption = #1042#1099#1075#1088#1091#1079#1080#1090#1100' '#1088#1077#1079#1091#1083#1100#1090#1072#1090#1099' '#1079#1072#1087#1088#1086#1089#1072
        OnClick = Excel1Click
      end
      object N9: TMenuItem
        Caption = '-'
        OnClick = Excel1Click
      end
      object N7: TMenuItem
        Caption = #1059#1082#1072#1079#1072#1090#1100' '#1087#1072#1087#1082#1091' '#1076#1083#1103' '#1089#1086#1093#1088#1072#1085#1077#1085#1080#1103' '#1086#1090#1095#1077#1090#1086#1074
        OnClick = N7Click
      end
    end
    object N3: TMenuItem
      Caption = #1055#1072#1089#1087#1086#1088#1090' '#1047#1059
      OnClick = N3Click
    end
    object N5: TMenuItem
      Caption = '-'
    end
    object N4: TMenuItem
      Caption = #1057#1082#1086#1087#1080#1088#1086#1074#1072#1090#1100' '#1050#1053
      OnClick = N4Click
    end
  end
  object dlgSave1: TSaveDialog
    DefaultExt = '.csv'
    FileName = 'D:\'#1041#1077#1079#1048#1084#1077#1085#1080'.csv'
    Filter = 'Excel file|.csv'
    InitialDir = 'd:\'
    Options = [ofHideReadOnly, ofPathMustExist, ofEnableSizing]
    Title = #1059#1082#1072#1078#1080#1090#1077' '#1087#1072#1087#1082#1091' '#1076#1083#1103' '#1089#1086#1093#1088#1072#1085#1077#1085#1080#1103
    Left = 920
    Top = 168
  end
end
