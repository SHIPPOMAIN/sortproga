object fTablePicture: TfTablePicture
  Left = 0
  Top = 0
  Align = alCustom
  BorderStyle = bsSizeToolWin
  Caption = #1055#1088#1086#1089#1084#1086#1090#1088' '#1079#1077#1084#1077#1083#1100#1085#1086#1075#1086' '#1091#1095#1072#1089#1090#1082#1072
  ClientHeight = 574
  ClientWidth = 796
  Color = clBtnFace
  DragKind = dkDock
  DragMode = dmAutomatic
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object img1: TImage
    Left = 0
    Top = 0
    Width = 796
    Height = 574
    Align = alClient
    ExplicitWidth = 680
    ExplicitHeight = 544
  end
  object pnl1: TPanel
    Left = 0
    Top = 0
    Width = 65
    Height = 49
    ParentBackground = False
    ShowCaption = False
    TabOrder = 0
    object btn1: TSpeedButton
      Left = 0
      Top = 0
      Width = 35
      Height = 35
      Align = alCustom
      Caption = '+'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -21
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      OnClick = btn1Click
    end
    object btn2: TSpeedButton
      Left = 31
      Top = 0
      Width = 35
      Height = 35
      Align = alCustom
      Caption = '-'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -21
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      OnClick = btn2Click
    end
    object lbl1: TLabel
      Left = 25
      Top = 34
      Width = 16
      Height = 13
      Caption = 'lbl1'
      Transparent = True
    end
    object lbl2: TLabel
      Left = 2
      Top = 34
      Width = 21
      Height = 13
      Caption = 'M 1:'
      Transparent = True
    end
  end
end
