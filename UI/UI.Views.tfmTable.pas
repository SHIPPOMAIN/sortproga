unit UI.Views.tfmTable;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Grids, Vcl.DBGrids, Vcl.Menus,  Vcl.Buttons,
  Vcl.ExtCtrls, {Vcl.Imaging.pngimage,} Vcl.StdCtrls,
  UI.uGeneral,
  {frxClass, frxDBSet,frxExportRTF, frxExportPDF,}
  CLIPBrd,
  //JSON, system.generics.collections,
  {IdIOHandler, IdIOHandlerSocket, IdIOHandlerStack, IdSSL, IdSSLOpenSSL,
  IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient, IdHTTP,}
  uPasportCreatePicture,  UI.VIEWS.uTABLEShowKArta,
  uDMReportPicture,
  uDockForm, Vcl.Tabs, Vcl.DockTabSet  ;

type
  tOnSelParcel = procedure(const pCadNum: string) of object;

  TfmTable = class(TForm)
    dbgrdResult: TDBGrid;
    pmResult: TPopupMenu;
    Excel1: TMenuItem;
    dlgSave1: TSaveDialog;
    N1: TMenuItem;
    N2: TMenuItem;
    pnlTools: TPanel;
    btnNext: TButton;
    btnBack: TButton;
    N3: TMenuItem;
    N4: TMenuItem;
    N5: TMenuItem;
    N6: TMenuItem;
    pnl1: TPanel;
    spl1: TSplitter;
    dcktbst1: TDockTabSet;
    N7: TMenuItem;
    N8: TMenuItem;
    N9: TMenuItem;
    procedure Excel1Click(Sender: TObject);
    procedure dbgrdResultCellClick(Column: TColumn);
    procedure dbgrdResultDrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure dbgrdResultTitleClick(Column: TColumn);
    procedure N1Click(Sender: TObject);
    procedure btnBackClick(Sender: TObject);
    procedure btnNextClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure N3Click(Sender: TObject);
    procedure N4Click(Sender: TObject);
    procedure N6Click(Sender: TObject);
    procedure pnl1DockDrop(Sender: TObject; Source: TDragDockObject; X,
      Y: Integer);
    procedure pnl1DockOver(Sender: TObject; Source: TDragDockObject; X,
      Y: Integer; State: TDragState; var Accept: Boolean);
    procedure pnl1GetSiteInfo(Sender: TObject; DockClient: TControl;
      var InfluenceRect: TRect; MousePos: TPoint; var CanDock: Boolean);
    procedure pnl1UnDock(Sender: TObject; Client: TControl;
      NewTarget: TWinControl; var Allow: Boolean);
    procedure dcktbst1DockDrop(Sender: TObject; Source: TDragDockObject; X,
      Y: Integer);
    procedure dcktbst1GetSiteInfo(Sender: TObject; DockClient: TControl;
      var InfluenceRect: TRect; MousePos: TPoint; var CanDock: Boolean);
    procedure dcktbst1TabAdded(Sender: TObject);
    procedure dcktbst1TabRemoved(Sender: TObject);
    procedure N7Click(Sender: TObject);
    procedure spl1Moved(Sender: TObject);
    procedure pnl1Resize(Sender: TObject);
  private
    fback: tNamedProc;
    fnext: tNamedProc;
    fOnSelParcel: tOnSelParcel;
    procedure setBack(const Value: tNamedProc);
    procedure setNext(const Value: tNamedProc);

    { Private declarations }
  public
    { Public declarations }

   pictureCreator : TPictureReport;
   pCadNUm : string; // ��� �������� �������� ������������ ������ � ������������ ��������
   //img : TImage;
   procedure GridSetting;
   property back: tNamedProc read fback write setBack;
   property next: tNamedProc read fnext write setNext;
   property OnSelParcel: tOnSelParcel read fOnSelParcel write fOnSelParcel;
   function DefDockPanelHeight: Integer;
   function DefDockPanelWidth: Integer;
   procedure ShowDockPanel(APanel: TWinControl; MakeVisible: Boolean; Client: TControl);
   procedure AfterConstruction; override;
   procedure CreatePictureForm;
   function CreateAndShowPicturebyCadNUm(pKN:string):boolean;
//   function  getfTablePicture: TfTablePicture;
//   procedure reSizeTABLEPICTURE();
  end;

//  tSizeForm = record
//    fH,fW : Integer;
//    fTop, fLeft : Integer;
//  end;

var
  fmTable: TfmTable;

implementation
uses uDM, uText, uParser, UI.Views.tfmWebMap, Units.DMImages, Units.Paths;
{$R *.dfm}

//var pCadNUm : string; // ��� �������� �������� ������������ ������ � ������������ ��������
   // FSZ : tSizeForm;


procedure TfmTable.Excel1Click(Sender: TObject);
begin
//  if dlgSave1.Execute then
//   begin
    try
    //TSpeedButton(Sender).Cursor := crSQLWait;
    fmTable.Cursor := crSQLWait;
    //dm.CreateExcel(dlgSave1.FileName);
    //dm.CreateExcel(ExtractFileDir(Application.ExeName)+'\Reports\');
    //TSpeedButton(Sender).Cursor := crArrow;
    dm.CreateExcel(AnPAths.Reports_path);
    fmTable.Cursor := crArrow;
    ShowMessage(rsEndLoad);
    except
    ShowMessage(rsErrorLoad);
    end;
//   end;
end;

procedure TfmTable.FormClose(Sender: TObject; var Action: TCloseAction);
begin
 Application.MainForm.Close;
 if Assigned(pictureCreator) then  FreeAndNil(pictureCreator);
end;

procedure TfmTable.FormShow(Sender: TObject);
begin
  GridSetting;  //���� ������ ������� �� ������� ����� ��� ��������������
  // 9-08-16 -- ���� ������� ����� ���������� ������ �������
  if dbgrdResult.DataSource.DataSet.Fields[0].FieldName = 'CADNUM' then Self.pCadNUm := dbgrdResult.DataSource.DataSet.Fields[0].AsString;
end;


procedure TfmTable.AfterConstruction;
begin
  self.dcktbst1.Width:=0;
  self.dcktbst1.Visible:=False;
  self.pnl1.Width:=0;
  self.spl1.Visible:=False;
end;

procedure TfmTable.btnBackClick(Sender: TObject);
begin
 if Assigned(fTablePicture) then
   if fTablePicture.Showing
     then fTablePicture.Close;

 Self.pCadNUm :='';
 self.fback.go();
end;

procedure TfmTable.btnNextClick(Sender: TObject);
begin
 self.fnext.go();
end;

procedure TfmTable.dbgrdResultCellClick(Column: TColumn);
begin
  if Assigned(self.fOnSelParcel)  then
   if dbgrdResult.DataSource.DataSet.Fields[0].FieldName = 'CADNUM' then self.fOnSelParcel(dbgrdResult.DataSource.DataSet.Fields[0].AsString);

  // 9-08-16
  if dbgrdResult.DataSource.DataSet.Fields[0].FieldName = 'CADNUM' then Self.pCadNUm := dbgrdResult.DataSource.DataSet.Fields[0].AsString;
  //22-08-16
  if self.n6.checked then
      begin
        Self.Cursor := crHourGlass;
        CreateAndShowPicturebyCadNUm(Self.pCadNUm);
        Self.Cursor := crDefault;
      end;
end;

procedure TfmTable.dbgrdResultDrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
// ������ �������� ������
	IF TDBGrid(Sender).DataSource.DataSet.RecNo mod 2 = 1
	Then TDBGrid(Sender).Canvas.Brush.Color:=clGradientInactiveCaption; //RGB(188,184,190);

	// ��������������� ��������� ������� ������� �������
	IF  gdSelected   IN State
	Then Begin
		TDBGrid(Sender).Canvas.Brush.Color:= clHighLight;
		TDBGrid(Sender).Canvas.Font.Color := clHighLightText;
	End;
	// ������ GRID �������������� ������
	TDBGrid(Sender).DefaultDrawColumnCell(Rect,DataCol,Column,State);
end;

procedure TfmTable.GridSetting;
var _att : tAttributs;  i:Integer;
begin
  if dm.ibqryResult.Active then
  begin

  for i := 0 to dm.ibqryResult.FieldCount-1 do
   begin
     for _att :=Low(tAttributs)  to  High(tAttributs) do

     if dm.ibqryResult.Fields[i].FieldName=cDBFields[_att] then
     begin
       dm.ibqryResult.Fields[i].DisplayLabel := cCaptions[_att];

       case _att of
         aCadNum :  dbgrdResult.Columns[i].Width := 150;
         aRAION,aSP,aKLH,aISP,aFISP,aRight :  dbgrdResult.Columns[i].Width := 250;
         aKTG, aSobst, aASOBST  :  dbgrdResult.Columns[i].Width := 200;
         aAREA,aCADCOST,aGR1,aGR2,aGR3,aGR4,aGR5,aGR6,aGR7,aUG1,aUG2,aUG3,aUG4,aUG5,aUG6,aUG7  :  dbgrdResult.Columns[i].Width := 80;
         aKUCH, aUFRS,aArenda  : dbgrdResult.Columns[i].Width := 30;
         aPeriod: dbgrdResult.Columns[i].Width := 50;
       end;
       break;
     end;
   end;
end;
end;

procedure TfmTable.N1Click(Sender: TObject);
begin
if not Assigned(fWebMap) then
   Application.CreateForm(TfmWebMap, fWebMap);
 if fWebMap.Visible then
  fWebMap.Close
 else
  fWebMap.Show
end;


procedure TfmTable.N3Click(Sender: TObject);
begin
    dmRP.OpenReport(Self.pcadnum);
end;

procedure TfmTable.N4Click(Sender: TObject);
begin
 Clipboard.AsText:=Self.pCadNUm;
end;

Function TfmTable.CreateAndShowPicturebyCadNUm(pKN:string):boolean;
var _H_mm, _W_mm : Integer;
begin
  // ���������� ������������� �������.
  try
  if not  Assigned(pictureCreator) then  pictureCreator := TPictureReport.Create;
    _H_mm := Round((fTablePicture.Height/96)*25.4);
    _W_mm := Round((fTablePicture.Width/96)*25.4);
    pictureCreator.GETPIcture(pKN, _H_mm, _W_mm, 1);
    fTablePicture.img1.Picture.Graphic := pictureCreator.ReportImage;
    fTablePicture.lbl1.Caption := pictureCreator.ReportMashtab;
    FreeAndNil(pictureCreator);
    Result := True;
  except //on E : Exception do
    FreeAndNil(pictureCreator);
    Result := False;
  end;
end;

procedure TfmTable.CreatePictureForm;
begin
  if NOT Assigned(fTablePicture)
     then Application.CreateForm(TfTablePicture, fTablePicture);
  if fTablePicture.Showing then fTablePicture.Close
  else
   begin
   { if (fTablePicture.HostDockSite is TWinControl) then
      begin
       self.ShowDockPanel(TWinControl(fTablePicture.HostDockSite), True, nil);
       fTablePicture.Show;
      end
    else
    if fTablePicture.HostDockSite is TPanel then
      begin
         self.ShowDockPanel(fTablePicture.HostDockSite as TPanel, True, fTablePicture);
         fTablePicture.Show;
      end
    else  fTablePicture.Show; }
    fTablePicture.Show;
    fTablePicture.ManualDock(self.pnl1, nil, TAlign.alClient);
   end;
end;


procedure TfmTable.N6Click(Sender: TObject);
begin
if Self.pCadNUm = '' then begin
                     ShowMessage(mbBeforeSeePicture);
                     Abort;
                     end;


 if N6.Checked then N6.Checked := False
  else
   if not N6.Checked then N6.Checked := true ;

  Self.CreatePictureForm;
 if N6.Checked then
   begin
     if not CreateAndShowPicturebyCadNUm(Self.pCadNUm)
      then fTablePicture.Close ;//Self.CreatePictureForm;;
   end;
end;

procedure TfmTable.N7Click(Sender: TObject);
begin
  if AnPAths.Reports_path <> ''
    then dlgSave1.InitialDir :=AnPAths.Reports_path;

  if dlgSave1.Execute
    then AnPAths.Reports_path := ExtractFileDir(dlgSave1.FileName)+'\';

end;

procedure TfmTable.pnl1DockDrop(Sender: TObject; Source: TDragDockObject; X, Y: Integer);
begin
 if (Sender as TPanel).DockClientCount = 1 then ShowDockPanel(Sender as TPanel, True, nil);
  (Sender as TPanel).DockManager.ResetBounds(True);
end;

procedure TfmTable.pnl1DockOver(Sender: TObject; Source: TDragDockObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
var ARect: TRect;
begin
  Accept := Source.Control is TDockableForm;
  if Accept then begin
    // Modify the DockRect to preview dock area.
    ARect.TopLeft := self.pnl1.ClientToScreen(Point(-self.DefDockPanelWidth, 0));
    ARect.BottomRight := self.pnl1.ClientToScreen(Point(0, self.pnl1.Height));
    Source.DockRect := ARect;
  end;

end;

procedure TfmTable.pnl1GetSiteInfo(Sender: TObject; DockClient: TControl;
  var InfluenceRect: TRect; MousePos: TPoint; var CanDock: Boolean);
begin
 CanDock := DockClient is TDockableForm;
end;

procedure TfmTable.pnl1Resize(Sender: TObject);
begin
// if (Self.pnl1.Width > 0) and fTablePicture.Visible then
// Self.reSizeTABLEPICTURE;
end;

procedure TfmTable.pnl1UnDock(Sender: TObject; Client: TControl;
  NewTarget: TWinControl; var Allow: Boolean);
begin
  // OnUnDock gets called BEFORE the client is undocked, in order to optionally
  // disallow the undock. DockClientCount is never 0 when called from this event.
  if ((Sender as TPanel).DockClientCount = 1) then ShowDockPanel(Sender as TPanel, false, nil);
//  spl1.Visible := false;
end;


//procedure TfmTable.reSizeTABLEPICTURE;
//var _H_mm, _W_mm : Integer;
//begin
//  _H_mm := Round((self.pnl1.Height/96)*25.4);//Round((fTablePicture.Height/96)*25.4);
//  _W_mm := Round((Self.pnl1.Width/96)*25.4);//Round((fTablePicture.Width/96)*25.4);
//  if Assigned(fTablePicture.img1.Picture.Graphic) and (fTablePicture.Height >0) and (fTablePicture.Width>0) then
//     fTablePicture.img1.Picture.Graphic:=TCreatePicteru.CreatePictureUSERMASHAB(0,_H_mm, _W_mm, self.pnl1.Height, Self.pnl1.Width);
//end;

procedure TfmTable.setBack(const Value: tNamedProc);
begin
  fback := Value;
  self.btnBack.Caption:= Value.title;
end;

procedure TfmTable.setNext(const Value: tNamedProc);
begin
  self.fnext := Value;
  if Assigned(Value.go) then begin
   Self.btnNext.Caption:= Value.title;
   self.btnNext.Visible:= True;
  end else
   self.btnNext.Visible:= False;
end;

procedure TfmTable.ShowDockPanel(APanel: TWinControl; MakeVisible: Boolean;
  Client: TControl);
begin
  // Client - the docked client to show if we are re-showing the panel.
  // Client is ignored if hiding the panel.

  // Since docking to a non-visible docksite isn't allowed, instead of setting
  // Visible for the panels we set the width to zero. The default InfluenceRect
  // for a control extends a few pixels beyond it's boundaries, so it is possible
  // to dock to zero width controls.

  // Don't try to hide a panel which has visible dock clients.
  if not MakeVisible and (APanel.VisibleDockClientCount > 1) then exit;

  if APanel = self.pnl1 then self.spl1.Visible := MakeVisible;

  if MakeVisible then begin
    if APanel = pnl1 then begin
      // APanel.Height := self.DefDockPanelHeight;
     APanel.Width:= (Self.Width div 3);//fTablePicture.width;//680;//self.DefDockPanelHeight;
     APanel.Height := Self.height;//544;

     APanel.Left := 0;
     self.spl1.Left := 0;
    end;
  end else begin
    if APanel = pnl1 then APanel.Width := 0;
  end;

  if MakeVisible and (Client <> nil) then Client.Show;
end;

procedure TfmTable.spl1Moved(Sender: TObject);
begin
//  Self.reSizeTABLEPICTURE;
end;

procedure TfmTable.dbgrdResultTitleClick(Column: TColumn);  //���������� �� ������
var _s1, _s2:string; ss : TArray<string>;
dil1 : string;
fName : string;
begin
 with dm do
 begin
  _s2 := '';
  dil1 := 'order';
  //fName :=  Column.FieldName ;
  ss:= ibqryResult.SQL.Text.Split(dil1,ExcludeEmpty);
  _s1 := Trim(ss[0]);
  _s2:= _s1+' order by '+ inttostr(Column.Index+1);//fName;
  dm.inquiry(_s2);
  GridSetting;
 end;
end;


procedure TfmTable.dcktbst1DockDrop(Sender: TObject; Source: TDragDockObject; X,
  Y: Integer);
var
  SendingTab: TDockTabSet;
begin
  SendingTab := Sender as TDockTabSet;
  if Source.Control.Visible then SendingTab.ShowDockClient(Source.Control);
end;

procedure TfmTable.dcktbst1GetSiteInfo(Sender: TObject; DockClient: TControl;
  var InfluenceRect: TRect; MousePos: TPoint; var CanDock: Boolean);
var
  SendingTab: TDockTabSet;
begin
  SendingTab := Sender as TDockTabSet;
  InfluenceRect.Right := SendingTab.DestinationDockSite.ClientWidth;
  CanDock := (DockClient is TDockableForm) and (SendingTab.Tabs.Count > 0);
end;

procedure TfmTable.dcktbst1TabAdded(Sender: TObject);
var
  SendingTab: TDockTabSet;
const
  cDockTabHeight = 23;
begin
  SendingTab := Sender as TDockTabSet;
  if SendingTab.Tabs.Count = 1 then begin
    SendingTab.Visible := True;
    if SendingTab.Align in [alBottom, alTop] then begin
      SendingTab.Height := cDockTabHeight;
      if SendingTab.Align = alTop then SendingTab.Top := 0
      else begin
        SendingTab.Top := ClientHeight;
        self.pnl1.Top := ClientHeight;
      end
    end else begin
      SendingTab.Width := cDockTabHeight;
      if SendingTab.Align = alLeft then SendingTab.Left := 0
      else SendingTab.Left := ClientWidth;
    end;
  end;
end;

procedure TfmTable.dcktbst1TabRemoved(Sender: TObject);
var
  SendingTab: TDockTabSet;
begin
  SendingTab := Sender as TDockTabSet;
  if SendingTab.Tabs.Count = 0 then begin
    if SendingTab.Align in [alBottom, alTop] then SendingTab.Height := 0
    else begin
      SendingTab.Width := 0;
    end;
    SendingTab.Visible := false;
  end;
end;

function TfmTable.DefDockPanelHeight: Integer;
begin
  Result := self.Height div 7;
end;

function TfmTable.DefDockPanelWidth: Integer;
begin
 Result := self.Width div 7;
end;


end.
