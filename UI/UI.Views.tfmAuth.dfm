object fmAuth: TfmAuth
  Left = 0
  Top = 0
  BorderStyle = bsNone
  Caption = #1044#1083#1103' '#1074#1093#1086#1076#1072' '#1074' '#1089#1080#1089#1090#1077#1084#1091' '#1042#1072#1084' '#1085#1077#1086#1073#1093#1086#1076#1080#1084#1086' '#1074#1074#1077#1089#1090#1080' '#1089#1074#1086#1080' '#1091#1095#1077#1090#1085#1099#1077' '#1076#1072#1085#1085#1099#1077
  ClientHeight = 175
  ClientWidth = 348
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object lbl1: TLabel
    Left = 24
    Top = 77
    Width = 93
    Height = 13
    Caption = #1048#1084#1103' '#1087#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1103
  end
  object lbl2: TLabel
    Left = 24
    Top = 112
    Width = 37
    Height = 13
    Caption = #1055#1072#1088#1086#1083#1100
  end
  object Label1: TLabel
    AlignWithMargins = True
    Left = 3
    Top = 3
    Width = 342
    Height = 32
    Align = alTop
    Alignment = taCenter
    Caption = 
      #1048#1085#1092#1086#1088#1084#1072#1094#1080#1086#1085#1085#1072#1103' '#1089#1080#1089#1090#1077#1084#1072' '#1091#1095#1077#1090#1072' '#1080' '#1084#1086#1085#1080#1090#1086#1088#1080#1085#1075#1072' '#1079#1077#1084#1077#1083#1100' '#1089#1077#1083#1100#1089#1082#1086#1093#1086#1079#1103#1081#1089#1090 +
      #1074#1077#1085#1085#1086#1075#1086' '#1085#1072#1079#1085#1072#1095#1077#1085#1080#1103
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    WordWrap = True
    ExplicitWidth = 331
  end
  object Label2: TLabel
    Left = 0
    Top = 38
    Width = 348
    Height = 16
    Align = alTop
    Alignment = taCenter
    Caption = '('#1084#1086#1076#1091#1083#1100' '#1072#1085#1072#1083#1080#1090#1080#1082#1080')'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    WordWrap = True
    ExplicitWidth = 117
  end
  object edtPassword: TEdit
    Left = 123
    Top = 109
    Width = 198
    Height = 21
    PasswordChar = '*'
    TabOrder = 0
  end
  object btnConnect: TButton
    Left = 24
    Top = 142
    Width = 113
    Height = 25
    Caption = #1042#1086#1081#1090#1080
    Default = True
    TabOrder = 1
    OnClick = btnConnectClick
  end
  object btnExit: TButton
    Left = 200
    Top = 142
    Width = 121
    Height = 25
    Caption = #1054#1090#1084#1077#1085#1072
    TabOrder = 2
    OnClick = btnExitClick
  end
  object cbbLogin: TComboBox
    Left = 123
    Top = 74
    Width = 198
    Height = 21
    TabOrder = 3
    OnChange = cbbLoginChange
    OnExit = cbbLoginExit
  end
end
