unit UI.Views.tfmStat;

interface

uses
  Winapi.Windows, Winapi.Messages, System.Generics.Collections  ,System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ComCtrls, Vcl.ExtCtrls,
  Vcl.Grids, Vcl.DBGrids, Vcl.Menus, Vcl.Buttons,PngImage,UI.Views.tfmTable;

type

  TfmStat = class(TForm)
    pnl1: TPanel;
    spbGoToLibrary: TSpeedButton;
    spbGoToQuickRequest: TSpeedButton;
    dbgrdStat: TDBGrid;
    lbl1: TLabel;

    procedure spbGoToLibraryClick(Sender: TObject);
    procedure spbGoToQuickRequestClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    flibrqst: TProc;
    fqrqst: TProc;
    procedure CMVisibleChanged(var Message: TMessage); message CM_VISIBLECHANGED;
  public
    { Public declarations }
    fMasterVisible: boolean;
    property librqst: TProc read flibrqst write flibrqst; //����������
    property qrqst: TProc read fqrqst write fqrqst; //������� ������
  end;



var
  fmStat: TfmStat;

implementation
uses uDM, ScopeAppSettings, uText, uRedactorSHb;
{$R *.dfm}



procedure TfmStat.CMVisibleChanged(var Message: TMessage);
begin
 if not self.fMasterVisible then self.Visible:= false
 else
  inherited;
end;


procedure TfmStat.FormShow(Sender: TObject);
begin
self.spbGoToLibrary.Parent:= self.dbgrdStat;
self.spbGoToQuickRequest.Parent:= self.dbgrdStat;
end;

procedure TfmStat.spbGoToLibraryClick(Sender: TObject);
begin
 self.librqst();
end;

procedure TfmStat.spbGoToQuickRequestClick(Sender: TObject);
begin
 if Assigned(fRedactShbl) then fRedactShbl.MenuItem2.Click;  // 02-09-16

 self.qrqst();
end;

end.
