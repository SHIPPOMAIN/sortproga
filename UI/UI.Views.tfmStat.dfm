object fmStat: TfmStat
  Left = 0
  Top = 0
  AutoSize = True
  Caption = #1043#1083#1072#1074#1085#1086#1077' '#1084#1077#1085#1102
  ClientHeight = 491
  ClientWidth = 1034
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnl1: TPanel
    Left = 0
    Top = 0
    Width = 1034
    Height = 491
    Align = alClient
    TabOrder = 0
    object spbGoToLibrary: TSpeedButton
      Left = 776
      Top = 408
      Width = 209
      Height = 22
      Caption = #1055#1077#1088#1077#1081#1090#1080' '#1074' '#1073#1080#1073#1083#1080#1086#1090#1077#1082#1091' '#1079#1072#1087#1088#1086#1089#1086#1074' >'
      OnClick = spbGoToLibraryClick
    end
    object spbGoToQuickRequest: TSpeedButton
      Left = 776
      Top = 436
      Width = 209
      Height = 22
      Caption = #1042#1099#1087#1086#1083#1085#1080#1090#1100' '#1073#1099#1089#1090#1088#1099#1081' '#1079#1072#1087#1088#1086#1089' >'
      OnClick = spbGoToQuickRequestClick
    end
    object lbl1: TLabel
      Left = 1
      Top = 1
      Width = 1032
      Height = 18
      Align = alTop
      Alignment = taCenter
      Caption = 
        '                                                 '#1057#1042#1054#1044#1053#1040#1071' '#1058#1040#1041#1051#1048#1062#1040 +
        ' '#1055#1051#1054#1065#1040#1044#1045#1049' '#1055#1054' '#1042#1054#1056#1054#1053#1045#1046#1057#1050#1054#1049' '#1054#1041#1051#1040#1057#1058#1048' ('#1075#1077#1082#1090#1072#1088#1099')'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Calibri'
      Font.Style = []
      ParentFont = False
      ExplicitWidth = 606
    end
    object dbgrdStat: TDBGrid
      Left = 1
      Top = 19
      Width = 1032
      Height = 471
      Align = alClient
      DataSource = dm.dsStat
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      TabOrder = 0
      TitleFont.Charset = RUSSIAN_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -15
      TitleFont.Name = 'Calibri'
      TitleFont.Style = []
    end
  end
end
