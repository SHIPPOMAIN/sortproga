unit UI.Views.tfmSplashForm;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, SplashForm;

type
  TfmSplashForm = class(TSpashForm)
    tmrWoit: TTimer;
    procedure tmrWoitTimer(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmSplashForm: TfmSplashForm;

implementation

{$R *.dfm}



procedure TfmSplashForm.tmrWoitTimer(Sender: TObject);
begin
 self.tmrWoit.Enabled:= false;
 self.Close;

end;

end.
