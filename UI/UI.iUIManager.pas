unit UI.iUIManager;

interface

uses
 System.SysUtils,
 System.Generics.Collections,
 Units.Utils;
procedure initnavigate;

implementation

 uses
  VCL.Dialogs,
  VCL.Forms,
  VCL.Controls,
  UI.Views.tfmStat,
  UI.Views.tfmTable,
  uRedactorSHb,
  uWorkShablon,
  UI.uGeneral;

 resourcestring
  rsqrqst_caption = '������� ������';
  rsrqstedit_caption = '�������������� ������� (%s)';
type

  {$region '���������� ����������� ����'}
  showmanager = record
    class var
     form: TForm;
     class procedure show(form: tform); static;
  end;
  {$Endregion}

  {$REGION '���� ������������ �� ������'}
  uistack = record
    class var
     stack: TArray<TProc>; //���� �������
     class procedure push(value: tproc);overload; static;
     class procedure push(nproc: tNamedProc; proc: tproc<tNamedProc>); overload; static;
     class procedure pop; static;
  end;
  {$ENDRegion}

  {$REGION '����� ����������'}
  formscope = record
   class function stat: TfmStat; static; //����� ����������
   class function rqeditor: TfRedactShbl; static; //������� ��������
   class function rtable: TfmTable; static; //������� �����������
   class function rqlibrary: TForm5; static; //���������� ��������
  end;
  {$ENDREGION}

  {$REGION '����� ���������'}
  //����������
  Statistic = record
   class procedure goto_(next: TProc<tNamedProc>); static; //������� �
   class procedure gotoLibRqst; static; //���������� ��������
   class procedure gotoQRqst; static; //������� ������
   class procedure show; static; //���������� �������
  end;

  //������� ������
  QuickRequest = record
   class procedure goto_(next: TProc<tNamedProc>); static; //��� ��������� ����
   class procedure gotoRTable; static; //������� �����������
   class procedure show(back: tNamedProc); static;
  end;

  //������� ����������� (��� �������� � ���������)
  ResultTable_final = record
   class procedure show(back: tNamedProc); static;
  end;

  //���������� ��������
  LibRequest = record
   class procedure goto_(next: TProc<tNamedProc>); static; //��� ��������� ����
   class procedure gotoRTable; static; //������� �����������
   class procedure gotoRqEditor; static; // �������� ��������
   class procedure show(back: tNamedProc); static;
  end;

  //������� ��������
  RequestEditor = record
   class procedure goto_(next: TProc<tNamedProc>{; const NameSh : string = ''}); static;
   class procedure gotoResultTable; static; //������� �����������
   class procedure show(back: tNamedProc); static;
  end;

  //������� �����������
  ResultTable = record
   class procedure goto_(next: TProc<tNamedProc>); static;
   class procedure gotoRequestEditor; static; //������� � �������������� �������
   class procedure show(back: tNamedProc); static;
  end;

  //�������� �������� ��������� �� ������� ����������
  RequestEditor_final = record
   class procedure show(back: tNamedProc); static;
  end;

  {$endregion}
procedure initnavigate;
begin
 Statistic.show;
end;

{ formscope }

class function FormScope.rqlibrary: TForm5;   // ���������� ��������
begin
  if not Assigned(Form5) then
  Application.CreateForm(TForm5,Form5);
 Result:= Form5;
end;

class function FormScope.rqeditor: TfRedactShbl;  // �������� ��������
begin
 if not Assigned(fRedactShbl) then
  Application.CreateForm(TfRedactShbl,fRedactShbl);
 Result:= fRedactShbl;
end;

class function FormScope.stat: TfmStat;
begin
 if not Assigned(fmStat) then
  Application.CreateForm(TfmStat,fmStat);       //����������
 Result:= fmStat;
end;

class function FormScope.rtable: TfmTable;
begin
 if not Assigned(fmTable) then
  Application.CreateForm(tfmTable,fmTable);     //���������
 Result:= fmTable;
end;


{ statscope }

class procedure Statistic.goto_(next: TProc<tNamedProc>);
begin
 uistack.push(Statistic.show);
 next(tNamedProc.Construct(formscope.stat.Caption,uistack.pop));
end;

class procedure Statistic.gotoLibRqst;
begin
 { TODO -c���� :
������ ��������� �������.
�������� ������� �/� ������� ������������ ���������� }
 //if  MessageDlg('�� �������??? �������� ������ ��� �������� ��������',mtConfirmation,mbYesNo,-1) =  mrYes then

 Statistic.goto_(LibRequest.show);
end;

class procedure Statistic.gotoQRqst;
begin
  Statistic.goto_(QuickRequest.show);
end;

class procedure Statistic.show;
begin
 formscope.stat.librqst:= Statistic.gotoLibRqst;
 formscope.stat.qrqst:= Statistic.gotoQRqst;
 showmanager.show(formscope.stat);
end;

{ qrqstscope }


class procedure QuickRequest.goto_(next: TProc<tNamedProc>);
begin
 uistack.push(formscope.rqeditor.back,QuickRequest.show);
 next(tNamedProc.Construct(formscope.rqeditor.Caption,uistack.pop));
end;

class procedure QuickRequest.gotoRTable;
begin
 requestscope.CreateAndRunSql(formscope.rqeditor.Query);         //4-07-16
 QuickRequest.goto_(ResultTable_final.show);
end;

class procedure QuickRequest.show(back: tNamedProc);
var
 _f: TfRedactShbl;
begin
 _f:= formscope.rqeditor;
 //_f.MenuItem2.Click;   // 02-09-16 ������
 _f.Caption:= '-������� ������-';
 _f.back:= back;
 _f.next:= tNamedProc.Construct('��������� ������',QuickRequest.gotoRTable);
 showmanager.show(_f);
end;

{ navigatorscope }

class procedure showmanager.show(form: tform);
var
 _form: TForm;
begin
 _form:= nil;
 if Assigned(showmanager.form) then begin
   _form:= showmanager.form;
   form.Left:= _form.Left;
   form.Top:= _form.Top;
   form.Width:= _form.Width;
   form.Height:= _form.Height;
 end;

 showmanager.form:= form;
 showmanager.form.Show;
 showmanager.form.BringToFront;
 if Assigned(_form) then
  _form.Hide;
end;


{ librqstscope }

class procedure LibRequest.goto_(next: TProc<tNamedProc>);
begin
 uistack.push(formscope.rqlibrary.back,LibRequest.show);
 next(tNamedProc.Construct(formscope.rqlibrary.Caption,uistack.pop));
end;

class procedure LibRequest.gotoRqEditor;
begin
 if formscope.rqlibrary._strSHName <>'' then                     //5 -07-16
    begin
      formscope.rqeditor._strSHName := formscope.rqlibrary._strSHName;
      formscope.rqlibrary._strSHName := '';
    end;
 if formscope.rqlibrary._gr_id <> 0 then                         //5 -07-16
    begin
      formscope.rqeditor._GR_ID := formscope.rqlibrary._gr_id;
      formscope.rqlibrary._gr_id := 0;
      formscope.rqeditor.N7.Visible := True;
    end;

 if  formscope.rqlibrary.QueryValue <>'' then                                // ����� ���� � 2-� ������� - � �������� �� ����������
  formscope.rqeditor.Query:= formscope.rqlibrary.QueryValue;      //4-07-16                              - ������ ����� ��� �������� �������

 LibRequest.goto_(RequestEditor.show);
end;


class procedure LibRequest.gotoRTable;
begin
 requestscope.CreateAndRunSql(formscope.rqlibrary.QueryValue);    //4-07-16
 LibRequest.goto_(ResultTable.show);
end;

class procedure LibRequest.show(back: tNamedProc);
var
 _f: TForm5;
begin
 _f:= formscope.rqlibrary;
 _f.Caption:= '-���������� ��������-';
 _f.back:= back;
 _f.next:= tNamedProc.Construct('��������� ������',LibRequest.gotoRTable);
 _f.rqeditor:= tNamedProc.Construct('�������� ������',LibRequest.gotoRqEditor);
 showmanager.show(_f);
end;

{ rtablescope }


class procedure ResultTable.gotoRequestEditor;
begin
//�������� ������ (�������) � ��������
 //if formscope.rqeditor.Query ='' then                          //1-09-16 - ������������
    formscope.rqeditor.Query := formscope.rqlibrary.QueryValue; //5 -07-16
 ResultTable.goto_(RequestEditor_final.show);
end;

class procedure ResultTable.goto_(next: TProc<tNamedProc>);
begin
 uistack.push(formscope.rtable.back,
  procedure (val: tNamedProc) begin
   requestscope.CreateAndRunSql(formscope.rqeditor.Query);
   ResultTable.show(val);
  end);
 next(tNamedProc.Construct(formscope.rtable.Caption,uistack.pop));
end;

class procedure ResultTable.show(back: tNamedProc);
var
 _f: TfmTable;
begin
 _f:= formscope.rtable;
 _f.back:= back;
 _f.next:= tNamedProc.Construct('�������� ��������� �������', ResultTable.gotoRequestEditor);
 showmanager.show(_f);
end;

{ uistack }

class procedure uistack.pop;
begin
 if Length(uistack.stack)<>0 then begin
  uistack.stack[high(uistack.stack)]();
  SetLength(uistack.stack,high(uistack.stack));
 end;
end;

class procedure uistack.push(value: tproc);
begin
 setlength(uistack.stack,Length(uistack.stack)+1);
 uistack.stack[high(uistack.stack)]:= value;
end;

class procedure uistack.push(nproc: tNamedProc; proc: tproc<tNamedProc>);
begin
  uistack.push(procedure begin proc(nproc) end);
end;

{ rqeditorscope }

class procedure RequestEditor.gotoResultTable;
begin
 // ����� ���� ����� ���� ����� ���������� �������� - �� ����� ������� ��� ����� ��� ���������, ������� ����� � ����� ���������
 requestscope.CreateAndRunSql(formscope.rqeditor.Query);         //4-07-16
 RequestEditor.goto_(ResultTable.show);
end;

class procedure RequestEditor.goto_(next: TProc<tNamedProc>{; const NameSh : string = ''});
begin
 //if NameSh <> '' then formscope.rqlibrary.QueryValue := NameSh;

 uistack.push(formscope.rqeditor.back,RequestEditor.show);
 next(tNamedProc.Construct(formscope.rqlibrary.Caption,uistack.pop));
end;

class procedure RequestEditor.show(back: tNamedProc);
var
 _f: TfRedactShbl;
begin
 _f:= formscope.rqeditor;
 _f.Caption:= Format(rsrqstedit_caption,[formscope.rqeditor._strSHName]);//rsrqstedit_caption;
 _f.Back:= back;
 _f.next:= tNamedProc.Construct('��������� ������',RequestEditor.gotoResultTable);
 showmanager.show(_f);
end;

{ rqeditor_quickscope }

class procedure RequestEditor_final.show(back: tNamedProc);
var
 _f: TfRedactShbl;
begin
 _f:= formscope.rqeditor;
 _f.Caption:= '��������� ���������� �������';
 _f.back:= back;
 _f.next:= tNamedProc.ConstructEmpty;
 showmanager.show(_f);
end;


{ rtable_withouteditor }

class procedure ResultTable_final.show(back: tNamedProc);
var
 _f: TfmTable;
begin
 _f:= formscope.rtable;
 _f.back:= back;
 _f.next:= tNamedProc.ConstructEmpty;
 showmanager.show(_f);
end;


end.
