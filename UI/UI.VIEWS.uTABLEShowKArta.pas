unit UI.VIEWS.uTABLEShowKArta;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.Buttons,
  DockingUtils, uConjoinHost, uDockForm, uTabHost, Vcl.StdCtrls;

type
  TfTablePicture = class(TDockableForm)
    img1: TImage;
    pnl1: TPanel;
    btn1: TSpeedButton;
    btn2: TSpeedButton;
    lbl1: TLabel;
    lbl2: TLabel;
    procedure btn1Click(Sender: TObject);
    procedure btn2Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
    procedure reSizeTABLEPICTURE();
  end;

var
  fTablePicture: TfTablePicture;

implementation
 uses uPasportCreatePicture, UI.Views.tfmTAble;
{$R *.dfm}



procedure TfTablePicture.btn1Click(Sender: TObject);
var _H_mm, _W_mm, _ii: Integer;
begin
  _H_mm := Round((self.Height/96)*25.4);
  _W_mm := Round((self.Width/96)*25.4);
 // Self.img1.Picture.Graphic:= TCreatePicteru.CreatePictureUSERMASHAB(5000,_H_mm, _W_mm);
 Self.img1.Picture.Graphic:=TCreatePicteru.CreatePictureUSERMASHAB(-5000,_H_mm, _W_mm,self.Height,self.Width);
 _ii := StrToInt(Self.lbl1.Caption)-5000;
 Self.lbl1.Caption := _ii.ToString;
end;

procedure TfTablePicture.btn2Click(Sender: TObject);
var _H_mm, _W_mm, _ii : Integer;
begin
  _H_mm := Round((self.Height/96)*25.4);
  _W_mm := Round((self.Width/96)*25.4);
 // Self.img1.Picture.Graphic:=TCreatePicteru.CreatePictureUSERMASHAB(-5000,_H_mm, _W_mm);
  Self.img1.Picture.Graphic:= TCreatePicteru.CreatePictureUSERMASHAB(5000,_H_mm, _W_mm,self.Height,self.Width);
  _ii := StrToInt(Self.lbl1.Caption)+5000;
  Self.lbl1.Caption := _ii.ToString;
end;

procedure TfTablePicture.FormClose(Sender: TObject; var Action: TCloseAction);
begin
   fmTable.N6.Checked := false;
end;

procedure TfTablePicture.FormCreate(Sender: TObject);
begin
  if Assigned(UI.Views.tfmTAble.fmTable) then
  begin
   self.Top:=UI.Views.tfmTAble.fmTable.Top;
   self.Left := UI.Views.tfmTAble.fmTable.pnl1.Left;
   self.Width:=(UI.Views.tfmTAble.fmTable.Width div 3);
   self.Height:=UI.Views.tfmTAble.fmTable.Height;
  end;


  inherited;

end;

procedure TfTablePicture.FormResize(Sender: TObject);
begin
 if (Self.Width > 0) and Self.Visible then
 Self.reSizeTABLEPICTURE;
  inherited;

end;

procedure TfTablePicture.reSizeTABLEPICTURE;
var _H_mm, _W_mm : Integer;
begin
//  _H_mm := Round((self.Height/96)*25.4);//Round((fTablePicture.Height/96)*25.4);
//  _W_mm := Round((Self.Width/96)*25.4);//Round((fTablePicture.Width/96)*25.4);
//  if Assigned(self.img1.Picture.Graphic) and (self.Height >0) and (self.Width>0) then
//     Self.img1.Picture.Graphic:=TCreatePicteru.CreatePictureUSERMASHAB(0,_H_mm, _W_mm, self.Height, Self.Width);
 if Assigned(self.img1.Picture.Graphic) and (self.Height >0) and (self.Width>0) then
  fmTable.CreateAndShowPicturebyCadNUm(fmTable.pCadNUm);
end;

end.
