unit UI.Views.tfmAuth;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls,
  System.IniFiles,
  system.IOUtils;

type
  TfmAuth = class(TForm)
    lbl1: TLabel;
    lbl2: TLabel;
    edtPassword: TEdit;
    btnConnect: TButton;
    btnExit: TButton;
    cbbLogin: TComboBox;
    Label1: TLabel;
    Label2: TLabel;
    procedure btnConnectClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnExitClick(Sender: TObject);
    procedure cbbLoginChange(Sender: TObject);
    procedure cbbLoginExit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    function ConnectEstablished: Boolean;
  end;

var
  fmAuth: TfmAuth;
  //inif : TInifile;


implementation
 uses uDM{, uMain},
      uDMFDAC,
      Units.Paths,
      uDMReportPicture;

 var
  NameF : TextFile;
  stpass : TStringList;

 {$R *.dfm}

procedure  SaveUsertofile(UserName, userPassword:string);
var
 Fl : Boolean;
 ss1 : string;
 _aFilename : string;
begin
//if FileExists(ExtractFilePath(Application.ExeName)+'names.txt') then
//begin
   //AssignFile(NameF,ExtractFilePath(Application.ExeName)+'names.txt');
   //Reset(nameF);
  _aFilename := TPath.Combine(ACFG.HomePath,cFileNAMESIni);
  AssignFile(NameF, _aFilename);
  if not FileExists(_aFilename) then  Rewrite(NameF) else Reset(NameF);

   fl := False;
   while (not Eof(nameF)) and (not fl) do
   begin
     Readln(nameF,ss1);
     if (Pos(UserName, ss1)<> 0)
       then Fl :=True;
    // Next;
   end;
   CloseFile(nameF);

   if not fl then
    begin
     Append(nameF);
     Writeln(nameF, UserName, ' ',userPassword);
     CloseFile(nameF);
    end;
//end;
end;

procedure TfmAuth.btnConnectClick(Sender: TObject);
var UserName, userPassword, basepath , _r: string;

begin
// ������ ��������� �� ��� �����
{inif := TIniFile.Create(ExtractFilePath(Application.ExeName)+'Ini.ini');
basepath := Inif.ReadString('Connect','path','');
Inif.Free;  }

UserName := Trim(cbbLogin.Text);
userPassword := Trim(edtPassword.Text);

_r:= dm.LogIn(UserName,userPassword,{basepath}AnPAths.db_Lands_path);
if not _r.IsEmpty then begin
 MessageDlg(_r,mtError,[mbOK],-1);
 self.ModalResult:= mrNone;
end else begin
 // dmFD.conPostGres.Connected := True;   //16-08-16
 // ������������ � ���������
 _r:= dmFD.LogIn;
 if _r<>'' then MessageDlg(_r,mtError,[mbOK],-1);

 SaveUsertofile(UserName,userPassword); // ��������� ���� ���� ��� �����
 dmRP.LoadShablon; // ���������� ������ ������
 dmRP.INItProxy;  //��������� ��������� ��� ������ ���� ����

 self.ModalResult:= mrOk;
 self.CloseModal;
end;
end;

procedure TfmAuth.btnExitClick(Sender: TObject);
begin
 if dm.ibdtbs1.Connected then
 begin
   if  dm.ibqryResult.Active then dm.ibqryResult.Close;
   if dm.ibtrnsctn1.Active then dm.ibtrnsctn1.Commit;
   dm.ibdtbs1.Connected := false;
 end;
 stpass.Free;
 self.Close;
end;

procedure TfmAuth.cbbLoginChange(Sender: TObject);
begin
// edtPassword.Text := stpass.Strings[Self.cbbLogin.ItemIndex]
end;

procedure TfmAuth.cbbLoginExit(Sender: TObject);
begin
  if (Self.cbbLogin.ItemIndex <> -1) and ((Self.cbbLogin.ItemIndex < stpass.Count) or (Self.cbbLogin.ItemIndex = stpass.Count))  then
   edtPassword.Text := stpass.Strings[Self.cbbLogin.ItemIndex]
  else
   edtPassword.Text := '';
end;

function TfmAuth.ConnectEstablished: Boolean;
begin
result := self.ModalResult = mrOk;
end;

procedure TfmAuth.FormShow(Sender: TObject);
var ss1,ss2, _aFilename : string;
begin
 // if FileExists(ExtractFilePath(Application.ExeName)+'names.txt') then
 //AssignFile(NameF, ExtractFilePath(Application.ExeName)+'names.txt');

  _aFilename := TPath.Combine(ACFG.HomePath,cFileNAMESIni);
  AssignFile(NameF, _aFilename);
  if not FileExists(_aFilename) then   // �������
    begin
      Rewrite(NameF);
      writeln(NameF, cTempUserName+' '+cTempUserPass);
      CloseFile(NameF);
      cbbLogin.Items.Add(cTempUserName);
      stpass := TStringList.Create;
      stpass.Add({cTempUserName+' '+}cTempUserPass);
    end
  else
   begin
      Reset(NameF);

      stpass := TStringList.Create;

      while not Eof(nameF) do
      begin
        Readln(NameF, ss1);
        ss2 := Copy(ss1,1,Pos(' ',ss1)-1);
        cbbLogin.Items.Add(ss2);
        ss2 := Copy(ss1,Pos(' ',ss1)+1, Length(ss1));
        stpass.Add(ss2);
       // Next;
      end;
      CloseFile(nameF);
   end;

   if stpass.Count <> 0 then
   begin
     cbbLogin.ItemIndex := 0;
     edtPassword.Text :=  stpass.Strings[0];
   end
   else
   begin
    cbbLogin.Text := cTempUserName;
    edtPassword.Text := cTempUserPass;
   end;

end;

//function one(i:integer; bb : string):String;
//var aa: string;
//begin
//     case i of
//     0: begin
//        if bb = 'k' then aa := '0' ;
//        if bb = 'b' then aa := '1' ;
//        if bb = 'c' then aa := '2' ;
//        if bb = 'w' then aa := '3' ;
//        if bb = '*' then aa := '4' ;
//        if bb = 'p' then aa := '5' ;
//        if bb = 'f' then aa := '6' ;
//        if bb = '{' then aa := '7' ;
//        if bb = '?' then aa := '8' ;
//        if bb = 's' then aa := '9' ;
//       end;
//     1: begin
//        if bb = 'a' then aa := '0' ;
//        if bb = 'u' then aa := '1' ;
//        if bb = 'm' then aa := '2' ;
//        if bb = '^' then aa := '3' ;
//        if bb = 'e' then aa := '4' ;
//        if bb = 'q' then aa := '5' ;
//        if bb = '(' then aa := '6' ;
//        if bb = 'j' then aa := '7' ;
//        if bb = 'd' then aa := '8' ;
//        if bb = 'l' then aa := '9' ;
//       end;
//     2: begin
//        if bb = '[' then aa := '0' ;
//        if bb = 't' then aa := '1' ;
//        if bb = 'n' then aa := '2' ;
//        if bb = 'g' then aa := '3' ;
//        if bb = 'h' then aa := '4' ;
//        if bb = '#' then aa := '5' ;
//        if bb = 'v' then aa := '6' ;
//        if bb = '&' then aa := '7' ;
//        if bb = 'r' then aa := '8' ;
//        if bb = 'i' then aa := '9' ;
//       end;
//     3: begin
//        if bb = 'o' then aa := '0' ;
//        if bb = 'x' then aa := '1' ;
//        if bb = 'y' then aa := '2' ;
//        if bb = '@' then aa := '3' ;
//        if bb = 'z' then aa := '4' ;
//        if bb = '%' then aa := '5' ;
//        if bb = ')' then aa := '6' ;
//        if bb = '}' then aa := '7' ;
//        if bb = '!' then aa := '8' ;
//        if bb = '/' then aa := '9' ;
//       end;
//  end;  {case}
//  one := aa;
//end;


end.
