unit UI.uGeneral;

interface
uses
 System.Sysutils;

 type
  //���������� � �������� "�����"
  tNamedProc = record
    title: string; //�������� (��������� ��������)
    go: TProc; //����� ��������
    class function Construct(pTitle: string; pGoProc: TProc):tNamedProc; static;
    class function ConstructEmpty: tNamedProc; static;
  end;

  //��������� ����������������� ����������
  //*Next - � ����� ������� �.�. ������� ������ ����� ���� ��������������.
  tUISettings = record
    Title: string; //��������� ����
    Back: tNamedProc;
    NextTitle: TArray<string>; //�������� ��������
    Next: TArray<TProc>; //������� ������
    function NextByTitle(pTitle:string): TProc; //����� ��������� �� ��������
    procedure AppendNext(pTitle: string; pProc: TProc); //�������� ����� ��������
    class function Construct(pTitle:string; pBTitle:string; pBack: TProc; pNTitle:string; pNext: TProc): tUISettings; static;
  end;



implementation

{ tUISettings }

procedure tUISettings.AppendNext(pTitle: string; pProc: TProc);
begin
 self.NextTitle:= self.NextTitle+[pTitle];
 SetLength(self.Next,length(self.Next)+1);
 Self.Next[high(self.Next)]:= pProc;
end;

class function tUISettings.Construct(pTitle, pBTitle: string; pBack: TProc; pNTitle: string; pNext: TProc): tUISettings;
begin
 Result.Title:= pTitle;
 Result.Back:= tNamedProc.Construct(pBTitle,pBack);
 Result.NextTitle:= [pNTitle];
 Result.Next:= [];
 Result.Next[0]:= pNext;
end;

function tUISettings.NextByTitle(pTitle: string): TProc;
var
 _i: integer;
begin
 Result:= nil;
 for _i:= 0 to high( self.NextTitle) do
  if self.NextTitle[_i].Equals(pTitle) then
   EXIT(self.Next[_i]);
end;

{ tUIBack }

class function tNamedProc.Construct(pTitle: string; pGoProc: TProc): tNamedProc;
begin
 Result.title:= pTitle;
 Result.go:= pGoProc;
end;


class function tNamedProc.ConstructEmpty: tNamedProc;
begin
 Result:= tNamedProc.Construct('',nil);
end;


end.
