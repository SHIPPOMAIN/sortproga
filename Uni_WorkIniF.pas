unit Uni_WorkIniF;

interface
 uses INIFILES, Dialogs;
type

TWorkIniF =class
   public
   FUser,FPassword,FRole,FServer:string;
   Pstgserver,PSTGUSER,PSTGPASS,PSTGNAME:string;
   SERVER_PROXY:string;
   SERVERPROXYPORT,PSTGPORT:Integer;
   constructor Create();
   destructor Destroy; override;
   procedure WriteSettings(fPath:string);
   procedure ReadSettings(fPath:string);
   procedure Clear;
 end;

const
  _cSEctionF = 'ConnectF';
  _cSEctionPSTG  = 'ConnectPSQL';
  _cSEctionPROXY = 'PROXY';
implementation

{ TWorkIniF }

procedure TWorkIniF.Clear;
begin
   FUser      :='';
   FPassword  :='';
   FRole      :='';
   FServer    :='';
   Pstgserver :='';
   PSTGUSER   :='';
   PSTGPASS   :='';
   PSTGNAME   :='';
   SERVER_PROXY :='';
   SERVERPROXYPORT :=0;
   PSTGPORT        :=0;
end;

constructor TWorkIniF.Create;
begin

end;

destructor TWorkIniF.Destroy;
begin

  inherited;
end;

procedure TWorkIniF.ReadSettings(fPath:string);
var
   IniF:TIniFile;
begin
    IniF := TiniFile.Create(fPath);
  try
  Self.FServer         := IniF.ReadString(_cSEctionF,'Server','');
  Self.FUser           := IniF.ReadString(_cSEctionF,'User','');
  Self.FPassword       := IniF.ReadString(_cSEctionF,'Password','');
  Self.FRole           := IniF.ReadString(_cSEctionF,'Role','');

  Self.Pstgserver      := IniF.ReadString(_cSEctionPSTG,'DB_Postgress_server','');
  Self.PSTGPORT        := IniF.ReadInteger(_cSEctionPSTG,'DB_POSTGRESS_PORT',0);
  Self.PSTGUSER        := IniF.ReadString(_cSEctionPSTG,'DB_USER','');
  Self.PSTGPASS        := IniF.ReadString(_cSEctionPSTG,'DB_PASS','');
  Self.PSTGNAME        := IniF.ReadString(_cSEctionPSTG,'DB_NAME_POSTGRESS','');

  Self.SERVER_PROXY    := IniF.ReadString(_cSEctionPROXY,'SERVER_PROXY','');
  Self.SERVERPROXYPORT := IniF.ReadInteger(_cSEctionPROXY,'SERVER_PROXY_PORT',0);

    IniF.Free;
  except
    IniF.Free;
    Self.Clear;
    ShowMessage('������ ������ �����!');
  end;
end;

procedure TWorkIniF.WriteSettings(fPath:string);
 var
   IniF:TIniFile;
begin
  if fPath <> ''
  then IniF := TiniFile.Create(fPath)
  else Exit;

  try

  IniF.WRITEString(_cSEctionF,'Server',Self.FServer);
  IniF.WRITEString(_cSEctionF,'User',Self.FUser);
  IniF.WRITEString(_cSEctionF,'Password',Self.FPassword);
  IniF.WRITEString(_cSEctionF,'Role',Self.FRole);

  IniF.WRITEString(_cSEctionPSTG,'DB_Postgress_server',Self.Pstgserver);
  IniF.WriteInteger(_cSEctionPSTG,'DB_POSTGRESS_PORT',Self.PSTGPORT);
  IniF.WRITEString(_cSEctionPSTG,'DB_USER',Self.PSTGUSER);
  IniF.WRITEString(_cSEctionPSTG,'DB_PASS',Self.PSTGPASS);
  IniF.WRITEString(_cSEctionPSTG,'DB_NAME_POSTGRESS',Self.PSTGNAME);

  IniF.WRITEString(_cSEctionPROXY,'SERVER_PROXY',Self.SERVER_PROXY);
  IniF.WriteInteger(_cSEctionPROXY,'SERVER_PROXY_PORT',Self.SERVERPROXYPORT);

    IniF.Free;
  except
    IniF.Free;
    ShowMessage('������ ������ �����!');
  end;
end;
end.
