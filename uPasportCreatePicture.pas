unit uPasportCreatePicture;

interface

uses System.SysUtils, System.Classes, JSON, system.generics.collections,System.Math,  Vcl.Imaging.jpeg,
     IdIOHandler, IdIOHandlerSocket, IdIOHandlerStack, IdSSL, IdSSLOpenSSL,
     IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient, IdHTTP,
     Vcl.Forms;

type
    tOBJ_PIX = record        // ��� ������
      const
        _H_rep_MM =136;//144 ;
        _W_rep_MM =180;                //������ �������� � ����������
      var
      _duim_in_metr, _duim_in_bbox : Double;         //������ � �����
      _H_obj_pix, _W_obj_pix : Double;
      _H_rep_pix_str, _W_rep_pix_str : string;
      procedure Cliar;
      procedure calculate;
    end;

    tREP_PIX_STR = record    // ��� ������������� ���������.
      _H_rep_MM,_W_rep_MM  :integer;                // ����� ������������
      _duim_in_metr, _duim_in_bbox : Double;         //������ � �����
      _H_obj_pix, _W_obj_pix : Double;
      _H_rep_pix_str, _W_rep_pix_str : string;
      _deltaH,_deltaW : Integer;
      procedure Cliar;
      procedure calculate{(_H,_W:double)};
    end;


   TPictureReport = class
   private
     _pkn : string;
     _img : TJPEGImage ;
     _mashtab : string;
   public
    constructor Create();
    property ReportImage : TJPEGImage read _img;// write SetIMAGEJPEG;
    property ReportMashtab : string read _mashtab ;//write SetMastab;
    procedure GETPIcture(pKN:string; _H,_W : integer; _per:Integer);                //1
    procedure GetMashtab;
    destructor Destroy; override;
   end;

   TCreatePicteru = record
   private
    // class function GetIMAGEJPEG(comm : string):TJPEGImage;  static;
     class procedure REadJSONCoord(pKN:string); static;
     class function ReadMashtabBYBBOxCoord(var op:tOBJ_PIX):integer;  overload; static;
     class function ReadMashtabBYBBOxCoord(var rps:tREP_PIX_STR):integer; overload;  static;
     class procedure CreateNewBBOxCoord(_per:integer); static;
     class procedure CalculateNewBBOxCoord(delta:integer;var op:tOBJ_PIX);  overload;static;
     class procedure CalculateNewBBOxCoord(delta:integer;var rps:tREP_PIX_STR);  overload;static;
     class function USerMashtabCreate(_mash:integer):integer; static;
   public
     class procedure PrepareCoordsforPicture(pKn:string; _per:integer); static;       //
     class function CreatePicture(pKN:string; _per:Integer):TJPEGImage; static;       //2
     class function CreatePictureUSERMASHAB(Delta:integer; _h,_W:integer; _H_pix,_w_pix:integer):TJPEGImage; static;       //
    // class function SendCoordForPicture:string; static; overload;                 //2
     class function SendCoordForPicture(_h,_W:string):string; static;// overload;
   end;

  { TReadBBox = class
     _mX, _mY : TArray<double>;
     Mashtab:Integer;
     userMashtab : Integer;
    _x1new, _y1new, _x2new, _y2new : Double;
    _PicFormat : string;
    _H_rep_pix_str, _W_rep_pix_str : string;
    private
     procedure REadJSONCoord(pKN:string);
     procedure CreateNewBBOxCoord();
     procedure USerMashtabCreate();
    public
     constructor Create();
     destructor Destroy; override;
     function SendCoordForPicture:string;
   end;                                                }

    tMashtab = record
      Mashtab:Integer;
      userMashtab : Integer;
      procedure Cliar;
    end;



   TREADMGISAREA = class
     _MGISAREA : Double;
   public
     constructor Create();
     destructor Destroy; override;
     procedure ReadMGISAREA(pKN: string);
   end;

   tCOORDS = record
     _mX, _mY : TArray<double>;
    _x1new, _y1new, _x2new, _y2new : Double;
    procedure Clear;
   end;

const   //for report
  _H_rep_MM  =144;
  _W_rep_MM  =180;                //������ �������� � ����������
  _DPI  = 96;                                //DPI
  _PicFormat : string = 'jpeg';

implementation
 uses
  uDMFDAC,
  uDMReportPicture,
  Units.Paths;

var  JSON: TJSONObject; // rBB : TReadBBox;  //PR1:TPictureReport;
     COORD:tCOORDS;
     MM :tMashtab;
     OP : tOBJ_PIX;
     RPS : tREP_PIX_STR;

procedure tCOORDS.Clear;
begin
  _x1new := 0;
  _y1new := 0;
  _x2new := 0;
  _y2new := 0;
  SetLength(_mX,0);
  SetLength(_mY,0);
end;


constructor TPictureReport.Create;
begin
  Self._img := TJPEGImage.Create;
  Self._mashtab := '';
  MM.Cliar;
  OP.Cliar;
  COORD.Clear;
end;

destructor TPictureReport.Destroy;
begin
  FreeAndNil(Self._img);
//  FreeandNil(rBB);
  inherited;
end;


procedure TPictureReport.GetMashtab;
begin
  self._mashtab := MM.userMashtab.ToString;
end;

procedure TPictureReport.GETPIcture(pKN:string; _H,_W : integer; _per:Integer);
begin
 if _per = 1 then
    begin
      RPS._H_rep_MM := _H;
      RPS._W_rep_MM := _W;
      RPS.calculate;
    end;

 self._img :=  TCreatePicteru.CreatePicture(Pkn, _per);
 Self.GetMashtab;
end;

{class function TCreatePicteru.GetIMAGEJPEG(comm: string): TJPEGImage;
 var
  _mstr:TMemoryStream;
begin
  _mstr:=TMemoryStream.Create;
  dmRP.idhtpHttp.Get(comm, _mstr);
  _mstr.Seek(0, TSeekOrigin.soBeginning);
  Result:=TJPEGImage.Create;
  Result.LoadFromStream(_mstr);
 _mstr.Free;
end;                                                     }

class procedure TCreatePicteru.PrepareCoordsforPicture(pKn: string; _per:integer);
begin
  REadJSONCoord(pKn);   // ��������� ����������.
  CreateNewBBOxCoord(_per);   // ����������� �� ����� �������
end;

class procedure TCreatePicteru.CreateNewBBOxCoord(_per:integer);
var
    _max_obj_pix : Double; //������ ������� � �������� , ������ ������� ����
    _H_obj_metr_mash, _W_obj_metr_mash : Double;   //������ ������� ��. ����� (������)- ����� ��������������
    _coordXcenter, _coordYcenter : Double;         //���������� ������
begin
   if _per = 1 then mm.Mashtab := ReadMashtabBYBBOxCoord(RPS)
               else mm.Mashtab := ReadMashtabBYBBOxCoord(OP);
   MM.userMashtab := USerMashtabCreate(mm.Mashtab);

   if _per = 1 then CalculateNewBBOxCoord(0, rps)
               else CalculateNewBBOxCoord(0, op);
end;

class procedure TCreatePicteru.CalculateNewBBOxCoord(delta:integer; var op:tOBJ_PIX);
var
    _H_obj_metr_mash, _W_obj_metr_mash : Double;   //������ ������� ��. ����� (������)- ����� ��������������
    _coordXcenter, _coordYcenter : Double;         //���������� ������
    _MashX, _MashY : Double;
begin
    MM.userMashtab := MM.userMashtab+delta;
   _H_obj_metr_mash := op._H_obj_pix*mm.userMashtab;
   _W_obj_metr_mash := op._W_obj_pix*mm.userMashtab;

   _coordXcenter := (COORD._mx[0]+COORD._mx[4])/2;
   _coordYcenter := (COORD._my[0]+COORD._my[4])/2;

   COORD._x1new := _coordXcenter-_H_obj_metr_mash/2;
   COORD._y1new := _coordYcenter-_W_obj_metr_mash/2;
   COORD._x2new := _coordXcenter+_H_obj_metr_mash/2;
   COORD._y2new := _coordYcenter+_W_obj_metr_mash/2;
end;

class procedure TCreatePicteru.CalculateNewBBOxCoord(delta:integer;var RPS:tREP_PIX_STR);
var
    _H_obj_metr_mash, _W_obj_metr_mash : Double;   //������ ������� ��. ����� (������)- ����� ��������������
    _coordXcenter, _coordYcenter : Double;         //���������� ������
    _MashX, _MashY : Double;
begin
    MM.userMashtab := MM.userMashtab+delta;
   _H_obj_metr_mash := RPS._H_obj_pix*mm.userMashtab;
   _W_obj_metr_mash := RPS._W_obj_pix*mm.userMashtab;

   _coordXcenter := (COORD._mx[0]+COORD._mx[4])/2;
   _coordYcenter := (COORD._my[0]+COORD._my[4])/2;

   COORD._x1new := _coordXcenter-_H_obj_metr_mash/2;
   COORD._y1new := _coordYcenter-_W_obj_metr_mash/2;
   COORD._x2new := _coordXcenter+_H_obj_metr_mash/2;
   COORD._y2new := _coordYcenter+_W_obj_metr_mash/2;
end;

class function TCreatePicteru.CreatePicture(pKN:string; _per:integer):TJPEGImage;
begin
  PrepareCoordsforPicture(pKN, _per);
  if _per=1 then
    Result := dmRP.GetIMAGEJPEG(SendCoordForPicture(RPS._H_rep_pix_str, RPS._W_rep_pix_str))
  else
    Result := dmRP.GetIMAGEJPEG(SendCoordForPicture(OP._H_rep_pix_str, OP._W_rep_pix_str));
end;


class function TCreatePicteru.CreatePictureUSERMASHAB(Delta:integer; _h,_W:Integer; _H_pix,_w_pix:integer): TJPEGImage;
begin
  //RPS._deltaH := round(Abs(RPS._H_rep_MM-_h)/2);
  //RPS._deltaW := round(Abs(RPS._W_rep_MM-_W)/2);
  RPS._H_rep_MM := _h;
  RPS._W_rep_MM := _W;

  //RPS.calculate;
  RPS._H_rep_pix_str := _H_pix.ToString;
  RPS._W_rep_pix_str := _W_pix.ToString;

  CalculateNewBBOxCoord(delta, RPS);
  Result := dmRP.GetIMAGEJPEG(SendCoordForPicture(RPS._H_rep_pix_str, RPS._W_rep_pix_str));
end;



class procedure TCreatePicteru.REadJSONCoord(pKN:string);
var jvalue : TJSONValue;
    jarray, jcoords, jXY : TJSONArray;
    i,j:Integer;
begin
  with dmFD do
  begin
    if fdqry1.Active then fdqry1.Close;
    fdqry1.Params[0].Value := pKN; //pCadNUm;
    fdqry1.Active:= true;


    SetLength(COORD._mX,5);     SetLength(COORD._mY,5);
     try
          jvalue := TJSONObject.ParseJSONValue(fdqry1.Fields[0].AsString);
           if jvalue <> nil then
            begin
             JSON := jvalue  as TJSONObject;
             if JSON.Values['coordinates'] <> nil then jarray := JSON.Values['coordinates'] as TJSONArray;
             for i := 0 to jarray.Count-1 do
               begin
                // ss := jarray.Items[i].tojson;
                jcoords := jarray.items[i] as TJSONArray;

                for J := 0 to jcoords.Count-1 do
                  begin
                   jXY := jcoords.Items[j] as TJSONArray;
                   COORD._mX[j] := jXY.Items[0].value.ToDouble;
                   COORD._mY[j] := jXY.Items[1].value.ToDouble;
                  end;
               end;
            end;
            TArray.Sort<double>(COORD._mX);
            TArray.Sort<double>(COORD._mY);
         // Application.MessageBox(PWideChar(JSON.ToString),'REad',1)
     finally
          FreeAndNil(JSON);
          if fdqry1.Active then fdqry1.Close;
     end;

  end;
end;


class function TCreatePicteru.ReadMashtabBYBBOxCoord(var op:Tobj_pix): integer;
var
    //_H_rep_pix, _W_rep_pix : Double; //������ �������� � �������� , ������ �������� ����
    _H_obj_metr, _W_obj_metr : Double;             //������ ������� ��. ����� (������)
    _MashX, _MashY : Double;
begin
    op.calculate;

    op._W_obj_pix  :=  (op._H_rep_MM/1000)*(op._duim_in_metr/op._duim_in_bbox);   //�������� ���������� (������� � �� ����� (��� �� - �����))
    op._H_obj_pix  :=  (op._W_rep_MM/1000)*(op._duim_in_metr/op._duim_in_bbox);
    _H_obj_metr :=  Abs(COORD._mx[0]-COORD._mx[4]);
    _W_obj_metr :=  Abs(COORD._my[0]-COORD._my[4]);
    _MashX :=  _H_obj_metr/op._H_obj_pix  ;
    _MashY :=  _W_obj_metr/op._W_obj_pix  ;

   Result := Round(MAX(_MashX,_MashY));
end;

class function TCreatePicteru.ReadMashtabBYBBOxCoord(var RPS:tREP_PIX_STR): integer;
var
    //_H_rep_pix, _W_rep_pix : Double; //������ �������� � �������� , ������ �������� ����
    _H_obj_metr, _W_obj_metr : Double;             //������ ������� ��. ����� (������)
    _MashX, _MashY : Double;
begin
    //rps.calculate;

    rps._W_obj_pix  :=  (rps._H_rep_MM/1000)*(rps._duim_in_metr/rps._duim_in_bbox);   //�������� ���������� (������� � �� ����� (��� �� - �����))
    rps._H_obj_pix  :=  (rps._W_rep_MM/1000)*(rps._duim_in_metr/rps._duim_in_bbox);
    _H_obj_metr :=  Abs(COORD._mx[0]-COORD._mx[4]);
    _W_obj_metr :=  Abs(COORD._my[0]-COORD._my[4]);
    _MashX :=  _H_obj_metr/rps._H_obj_pix  ;
    _MashY :=  _W_obj_metr/rps._W_obj_pix  ;


    Result := Round(MAX(_MashX,_MashY));
end;

class function TCreatePicteru.SendCoordForPicture(_h, _W: string): string;
var
 ss,ss2,
 ssTemp:string;
begin
 //ss := StringReplace(COORD._x1new.tostring,',','.',[rfReplaceAll, rfIgnoreCase])+','+StringReplace(COORD._y1new.tostring,',','.',[rfReplaceAll, rfIgnoreCase])+','+StringReplace(COORD._x2new.tostring,',','.',[rfReplaceAll, rfIgnoreCase])+','+StringReplace(COORD._y2new.tostring,',','.',[rfReplaceAll, rfIgnoreCase]);//
//ss := COORD._x1new.tostring+','+COORD._y1new.tostring+','+COORD._x2new.tostring+','+COORD._y2new.tostring;
 ss := Format('%f,%f,%f,%f',[COORD._x1new,COORD._y1new,COORD._x2new,COORD._y2new]);
 //ss2 := 'http://195.98.73.242:7152/umz/service?&SERVICE=WMS&REQUEST=GetMap&VERSION=1.3.0&LAYERS=SH,Settlement,Les&STYLES=&FORMAT=image%2F'+_PicFormat+'&TRANSPARENT=TRUE&HEIGHT='+_H+'&WIDTH='+_W+'&WMSID=umz&DETECTRETINA=true&CRS=EPSG%3A3857&BBOX=';
 //ss2 := 'http://192.168.2.252:7152/umz/service?&SERVICE=WMS&REQUEST=GetMap&VERSION=1.3.0&LAYERS=SH,Settlement,Les&STYLES=&FORMAT=image%2F'+_PicFormat+'&TRANSPARENT=TRUE&HEIGHT='+_H+'&WIDTH='+_W+'&WMSID=umz&DETECTRETINA=true&CRS=EPSG%3A3857&BBOX=';
 ssTemp:= StringReplace(AnPAths.Server_address, '%HEIGHT%', _h,[rfReplaceAll, rfIgnoreCase]);
 ssTemp:= StringReplace(ssTemp, '%WIDTH%', _W,[rfReplaceAll, rfIgnoreCase]);
 ssTemp:= StringReplace(ssTemp, '%BBOX%',ss,[rfReplaceAll, rfIgnoreCase]);
 ssTemp:= StringReplace(ssTemp, '%FORMAT%',_PicFormat,[rfReplaceAll, rfIgnoreCase]);

 Result := ssTemp//ss2+ss;
end;

{class function TCreatePicteru.SendCoordForPicture:string;
var  ss,ss2{,ss3}{:string;
begin
 ss := COORD._x1new.tostring+','+COORD._y1new.tostring+','+COORD._x2new.tostring+','+COORD._y2new.tostring;//
 ss2 := 'http://195.98.73.242:7152/umz/service?&SERVICE=WMS&REQUEST=GetMap&VERSION=1.3.0&LAYERS=SH,Settlement,Les&STYLES=&FORMAT=image%2F'+_PicFormat+'&TRANSPARENT=TRUE&HEIGHT='+op._H_rep_pix_str+'&WIDTH='+op._W_rep_pix_str+'&WMSID=umz&DETECTRETINA=true&CRS=EPSG%3A3857&BBOX=';
 Result := ss2+ss;
end;}


class function TCreatePicteru.USerMashtabCreate(_mash:integer):integer;
var  _firstDIgit,step : integer;           //����������� �������� � ������ �����
    _ff:Integer;//Double;
    _ss:string;

function One(_fD:integer):Integer;
begin
 if (_fD div 5) = 0
   then Result := 5
   else
    if (_fD div 5) > 0
      then Result := 10
end;

begin
  if Length(_mash.ToString)<5 then
    begin
      _firstDIgit := One(StrToInt(copy(_mash.ToString,1,1)));
    end
  else
   begin
     _ss := copy(_mash.ToString,1,2);
     _ff := One(StrToInt(copy(_ss,2,1)));
     if _ff = 5 then _firstDIgit := StrToInt(copy(_ss,1,1))*10+_ff
                else _firstDIgit := (StrToInt(copy(_ss,1,1))+1)*10;
   end;
   step := 10;
    while (_mash/step)>1 do
    begin
      step := step*10;
    end;
    if Length(_mash.ToString)<5 then step := step div 10
                                       else step := step div 100 ;
   Result := _firstDIgit*step;  //(_firstDIgit+1)*step;
end;


constructor TREADMGISAREA.Create;
begin
 Self._MGISAREA := 0;
end;

destructor TREADMGISAREA.Destroy;
begin

  inherited;
end;

procedure TREADMGISAREA.ReadMGISAREA(pKN: string);
begin
  with dmFD do
  begin
    if fdqry2.Active then fdqry2.Close;
    fdqry2.Params[0].Value := pKN; //pCadNUm;
    fdqry2.Open;

    if fdqry2.IsEmpty then Self._MGISAREA := 0
                      else Self._MGISAREA := fdqry2.Fields[0].Value ;
    fdqry2.Close;
  end;
end;



{ tMashtab }

procedure tMashtab.Cliar;
begin
  MM.Mashtab := 0;
  MM.userMashtab := 0;
end;

{ tOBJ_PIX }

procedure tOBJ_PIX.calculate;   // ���������� ����� ���������
var _H_rep_pix, _W_rep_pix : Double; //������ �������� � �������� , ������ �������� ����
begin
   self._duim_in_metr := 100/2.54;
   self._duim_in_bbox := _duim_in_metr*2;
   _H_rep_pix := self._H_rep_MM/1000*self._duim_in_metr*_DPI;    self._H_rep_pix_str := Round(_H_rep_pix).ToString;
   _W_rep_pix := self._W_rep_MM/1000*self._duim_in_metr*_DPI;    self._W_rep_pix_str := Round(_W_rep_pix).ToString;
end;

procedure tOBJ_PIX.Cliar;
begin
  _H_obj_pix :=0;
  _W_obj_pix :=0;
  _duim_in_metr :=0;
  _duim_in_bbox :=0;
  _H_rep_pix_str := '';
  _W_rep_pix_str := '';
end;

{ tREP_PIX_STR }

procedure tREP_PIX_STR.calculate{(_H,_W:double)};
var _H_rep_pix, _W_rep_pix : Double; //������ �������� � �������� , ������ �������� ����
begin
  self._duim_in_metr := 100/2.54;
  self._duim_in_bbox := _duim_in_metr*2;
  _H_rep_pix := self._H_rep_MM/1000*self._duim_in_metr*_DPI;
  _W_rep_pix := self._W_rep_MM/1000*self._duim_in_metr*_DPI;
  self._H_rep_pix_str := Round(_H_rep_pix).ToString;
  self._W_rep_pix_str := Round(_W_rep_pix).ToString;

end;

procedure tREP_PIX_STR.Cliar;
begin
  _H_rep_MM := 0;
  _W_rep_MM := 0;
  _H_obj_pix :=0;
  _W_obj_pix :=0;
  _H_rep_pix_str := '';
  _W_rep_pix_str := '';
  _deltaH :=0;
  _deltaW :=0;
end;

end.
