object Form1: TForm1
  Left = 0
  Top = 0
  AutoSize = True
  Caption = 'Form1'
  ClientHeight = 120
  ClientWidth = 219
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object pnl3: TPanel
    Left = 0
    Top = 80
    Width = 219
    Height = 40
    Align = alBottom
    TabOrder = 0
    ExplicitWidth = 214
    object btnOk: TBitBtn
      Left = 1
      Top = 1
      Width = 110
      Height = 38
      Align = alRight
      Anchors = [akRight, akBottom]
      Caption = #1042#1099#1073#1088#1072#1090#1100
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      Kind = bkOK
      NumGlyphs = 2
      ParentFont = False
      TabOrder = 0
      OnClick = btnOkClick
      ExplicitLeft = -4
    end
    object btnCancel: TBitBtn
      Left = 111
      Top = 1
      Width = 107
      Height = 38
      Align = alRight
      Anchors = [akRight, akBottom]
      Caption = #1054#1090#1084#1077#1085#1072
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      Kind = bkCancel
      NumGlyphs = 2
      ParentFont = False
      TabOrder = 1
      OnClick = btnCancelClick
      ExplicitLeft = 106
    end
  end
  object rg1: TRadioGroup
    Left = 0
    Top = 0
    Width = 219
    Height = 80
    Align = alClient
    Caption = 'rg1'
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Calibri'
    Font.Style = []
    Items.Strings = (
      #1055#1088#1086#1096#1077#1076#1096#1080#1077' '
      #1053#1077' '#1087#1088#1086#1096#1077#1076#1096#1080#1077)
    ParentFont = False
    TabOrder = 1
    ExplicitWidth = 214
  end
end
