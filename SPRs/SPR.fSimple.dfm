object Form3: TForm3
  Left = 0
  Top = 0
  AutoSize = True
  Caption = 'Form3'
  ClientHeight = 95
  ClientWidth = 628
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object pnl3: TPanel
    Left = 0
    Top = 55
    Width = 628
    Height = 40
    Align = alBottom
    TabOrder = 0
    ExplicitTop = 79
    object btnOk: TBitBtn
      Left = 410
      Top = 1
      Width = 110
      Height = 38
      Align = alRight
      Anchors = [akRight, akBottom]
      Caption = #1042#1099#1073#1088#1072#1090#1100
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      Kind = bkOK
      NumGlyphs = 2
      ParentFont = False
      TabOrder = 0
      OnClick = btnOkClick
    end
    object btnCancel: TBitBtn
      Left = 520
      Top = 1
      Width = 107
      Height = 38
      Align = alRight
      Anchors = [akRight, akBottom]
      Caption = #1054#1090#1084#1077#1085#1072
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      Kind = bkCancel
      NumGlyphs = 2
      ParentFont = False
      TabOrder = 1
      OnClick = btnCancelClick
    end
  end
  object pnl1: TPanel
    Left = 0
    Top = 0
    Width = 628
    Height = 55
    Align = alClient
    TabOrder = 1
    ExplicitHeight = 57
    object lbl1: TLabel
      Left = 19
      Top = 16
      Width = 93
      Height = 18
      Caption = #1042#1074#1077#1076#1080#1090#1077' '#1084#1072#1089#1082#1091
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Calibri'
      Font.Style = []
      ParentFont = False
    end
    object edt1: TEdit
      Left = 130
      Top = 13
      Width = 482
      Height = 26
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Calibri'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
    end
  end
end
