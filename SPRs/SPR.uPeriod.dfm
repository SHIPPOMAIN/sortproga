object Form2: TForm2
  Left = 0
  Top = 0
  AutoSize = True
  Caption = #1042#1074#1077#1076#1080#1090#1077' '#1087#1077#1088#1080#1086#1076' '#1076#1077#1081#1089#1090#1074#1080#1103' '#1072#1088#1077#1085#1076#1099
  ClientHeight = 125
  ClientWidth = 377
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object pnl3: TPanel
    Left = 0
    Top = 85
    Width = 377
    Height = 40
    Align = alBottom
    TabOrder = 0
    ExplicitTop = 113
    object btnOk: TBitBtn
      Left = 159
      Top = 1
      Width = 110
      Height = 38
      Align = alRight
      Anchors = [akRight, akBottom]
      Caption = #1042#1099#1073#1088#1072#1090#1100
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      Kind = bkOK
      NumGlyphs = 2
      ParentFont = False
      TabOrder = 0
      OnClick = btnOkClick
    end
    object btnCancel: TBitBtn
      Left = 269
      Top = 1
      Width = 107
      Height = 38
      Align = alRight
      Anchors = [akRight, akBottom]
      Caption = #1054#1090#1084#1077#1085#1072
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      Kind = bkCancel
      NumGlyphs = 2
      ParentFont = False
      TabOrder = 1
      OnClick = btnCancelClick
    end
  end
  object pnl1: TPanel
    Left = 0
    Top = 0
    Width = 377
    Height = 85
    Align = alClient
    TabOrder = 1
    ExplicitLeft = 8
    ExplicitWidth = 361
    ExplicitHeight = 81
    object lbl3: TLabel
      Left = 34
      Top = 48
      Width = 97
      Height = 18
      Caption = #1055#1077#1088#1080#1086#1076' '#1074' '#1075#1086#1076#1072#1093
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Calibri'
      Font.Style = []
      ParentFont = False
    end
    object lbl2: TLabel
      Left = 202
      Top = 11
      Width = 16
      Height = 18
      Caption = #1087#1086
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Calibri'
      Font.Style = []
      ParentFont = False
    end
    object lbl1: TLabel
      Left = 34
      Top = 11
      Width = 6
      Height = 18
      Caption = 'c'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Calibri'
      Font.Style = []
      ParentFont = False
    end
    object edt3: TEdit
      Left = 137
      Top = 45
      Width = 202
      Height = 26
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Calibri'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
    end
    object medt2: TMaskEdit
      Left = 224
      Top = 8
      Width = 117
      Height = 26
      EditMask = '!99/99/0000;1;_'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Calibri'
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      TabOrder = 1
      Text = '  .  .    '
    end
    object medt1: TMaskEdit
      Left = 46
      Top = 8
      Width = 114
      Height = 26
      EditMask = '!99/99/0000;1;_'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Calibri'
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      TabOrder = 2
      Text = '  .  .    '
    end
  end
end
