object Form4: TForm4
  Left = 0
  Top = 0
  AutoSize = True
  Caption = 'Form4'
  ClientHeight = 79
  ClientWidth = 350
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object pnl3: TPanel
    Left = 0
    Top = 44
    Width = 350
    Height = 35
    Align = alBottom
    TabOrder = 0
    object btnOk: TBitBtn
      Left = 132
      Top = 1
      Width = 110
      Height = 33
      Align = alRight
      Anchors = [akRight, akBottom]
      Caption = #1042#1099#1073#1088#1072#1090#1100
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      Kind = bkOK
      NumGlyphs = 2
      ParentFont = False
      TabOrder = 0
      OnClick = btnOkClick
      ExplicitLeft = 133
    end
    object btnCancel: TBitBtn
      Left = 242
      Top = 1
      Width = 107
      Height = 33
      Align = alRight
      Anchors = [akRight, akBottom]
      Caption = #1054#1090#1084#1077#1085#1072
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      Kind = bkCancel
      NumGlyphs = 2
      ParentFont = False
      TabOrder = 1
      OnClick = btnCancelClick
      ExplicitLeft = 243
    end
  end
  object pnl1: TPanel
    Left = 0
    Top = 0
    Width = 350
    Height = 44
    Align = alClient
    TabOrder = 1
    ExplicitWidth = 342
    ExplicitHeight = 39
    object lbl2: TLabel
      Left = 194
      Top = 15
      Width = 12
      Height = 13
      Caption = #1087#1086
    end
    object lbl1: TLabel
      Left = 26
      Top = 15
      Width = 5
      Height = 13
      Caption = 'c'
    end
    object edt2: TEdit
      Left = 212
      Top = 12
      Width = 121
      Height = 21
      TabOrder = 0
    end
    object edt1: TEdit
      Left = 37
      Top = 12
      Width = 121
      Height = 21
      TabOrder = 1
    end
  end
end
