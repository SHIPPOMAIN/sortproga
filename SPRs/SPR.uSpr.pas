unit spr.uSpr;

interface

uses
  Winapi.Windows, Winapi.Messages, System.Generics.Collections, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.Grids, Vcl.DBGrids,
  Vcl.StdCtrls, Vcl.Buttons, data.db, System.StrUtils, Vcl.Menus,
  IBX.IBQuery,
  IBX.IBCustomDataSet;

type
  TfSpr = class(TForm)
    pnl1: TPanel;
    pnl2: TPanel;
    pnl3: TPanel;
    dbgrd1: TDBGrid;
    lbl1: TLabel;
    edtMask: TEdit;
    btnFind: TBitBtn;
    btnOk: TBitBtn;
    btnCancel: TBitBtn;
    pm1: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    ibdtstFind: TIBDataSet;
    procedure btnOkClick(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure dbgrd1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure N1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure dbgrd1CellClick(Column: TColumn);
    procedure N2Click(Sender: TObject);
    procedure btnFindClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    function getIDs: string;
    procedure setIDs(aValues:string );
    function getCaptions(pSTR : string=''): string;

  end;

var
  fSpr: TfSpr;
  masId : TArray<double>;
  _FF : Boolean;
   tempDs : TIBQuery;
  function Execute(var pSTR : string; pDs : TDataSource; pfCaption, pTableName, pFilterId : string; IS_Child : boolean) : string;

implementation
uses uDM;
{$R *.dfm}

function CountPos(const subtext: string; Text: string): Integer;
begin
  if (Length(subtext) = 0) or (Length(Text) = 0) or (Pos(subtext, Text) = 0) then
    Result := 0
  else
    Result := (Length(Text) - Length(StringReplace(Text, subtext, '', [rfReplaceAll]))) div
      Length(subtext);
end;

function Execute(var pSTR : string; pDs : TDataSource; pfCaption, pTableName, pFilterId : string; IS_Child : boolean) : string;
var i  : integer; myCursor : TCursor;
begin
  with fSpr do
  begin
   // myCursor := Cursor;
    Caption := pfCaption;
    dbgrd1.DataSource := nil;
    dbgrd1.DataSource := pDs;
    if dbgrd1.DataSource.DataSet.Active then dbgrd1.DataSource.DataSet.Close;
    if (pFilterId <> '') then pFilterId := Copy(pFilterId, 2, Length(pFilterId)-2);


    if (pTableName = 'T_SETTLE') and (pFilterId <> '') and (Pos(',', pFilterId)=0) then begin (dbgrd1.DataSource.DataSet as TIBQuery).ParamByName('p1').Value := 1; (dbgrd1.DataSource.DataSet as TIBQuery).ParamByName('p2').Value :=pFilterId ; end
      else
        if ((pTableName = 'T_SETTLE') and (pFilterId = '')) or ((pTableName = 'T_SETTLE') and (pFilterId <>'') and (Pos(',', pFilterId)<>0)) then begin (dbgrd1.DataSource.DataSet as TIBQuery).ParamByName('p1').Value := 0; (dbgrd1.DataSource.DataSet as TIBQuery).ParamByName('p2').Value :='-1' ; end;
    if (pTableName = 'T_KOLKHOZ') and (pFilterId <> '') and (Pos(',', pFilterId)=0) then begin (dbgrd1.DataSource.DataSet as TIBQuery).ParamByName('p1').Value := 1; (dbgrd1.DataSource.DataSet as TIBQuery).ParamByName('p2').Value :=pFilterId ; end
      else
        if ((pTableName = 'T_KOLKHOZ') and (pFilterId = '')) or ((pTableName = 'T_KOLKHOZ') and (pFilterId <>'') and (Pos(',', pFilterId)<>0)) then begin (dbgrd1.DataSource.DataSet as TIBQuery).ParamByName('p1').Value := 0; (dbgrd1.DataSource.DataSet as TIBQuery).ParamByName('p2').Value :='-1' ; end;

    dbgrd1.DataSource.DataSet.Open;

    {if IS_Child then
    begin
      btnOk.Visible := false;
    end;  }
    Cursor := crDefault;// myCursor;
    fSpr.setIDs(pSTR);

    if pTableName = 'T_CATEGORY' then pnl1.Enabled := False;
//    if pTableName = 'OWNER' then  begin dbgrd1.Columns[0].Width := 200; dbgrd1.Columns[1].Width := 20; dbgrd1.Columns[2].Width := 200; end;


    i := fSpr.ShowModal;

    if i = mrOK then
      begin
        if dbgrd1.SelectedRows.Count > 0 then
        begin
          pSTR := fSpr.getIDs;
          Result := fSpr.getCaptions; //getIDs;
        end
        else
          Result := fSpr.getCaptions(pSTR);
      end
    else
     begin
        Result := '';
     end;

   if (btnFind.Caption = '��������') then
     begin
      dbgrd1.DataSource.DataSet:=tempDs;
      if ibdtstFind.Active then ibdtstFind.Close;
      btnFind.Caption := '�����'
     end;

    dbgrd1.DataSource.DataSet.Close;
    if ibdtstFind.Active then ibdtstFind.Close;
    if Assigned(tempDs) then dbgrd1.DataSource:=pDs;
    _FF := false;
    tempDs := nil;
    edtMask.Text := '';
    btnFind.Caption := '�����';
    if pnl1.Enabled = False then pnl1.Enabled := true;

  end;
end;

procedure TfSpr.btnCancelClick(Sender: TObject);
begin
 Close;
end;

procedure TfSpr.btnFindClick(Sender: TObject);
var _s, _s1 : string;
begin
 if (Trim(edtMask.Text) <>'') and (Trim(edtMask.Text) <>' ') and (btnFind.Caption = '�����') then
  begin
   _s:=''''+'%'+ Trim(edtMask.Text)+'%'+'''';

   if  dbgrd1.DataSource.DataSet is TIBQuery then
   begin
      _s1 := TIBQuery(dbgrd1.DataSource.DataSet).sql.Text ;
     if dbgrd1.DataSource.DataSet.Name = 'ibqrySP' then
      begin
        if Pos('order',_s1) <>0 then _s1 := Copy(_s1, 1, Pos('order',_s1)-1);
         _s1 := _s1 + ' where upper(se.name) like upper('+ _s+')';
      end;
     if dbgrd1.DataSource.DataSet.Name ='ibqryRaion' then
      begin
        if Pos('order',_s1) <>0 then _s1 := Copy(_s1, 1, Pos('order',_s1)-1);
         _s1 := _s1 + ' where upper(lp.name) like upper('+ _s+')';
      end;
      if dbgrd1.DataSource.DataSet.Name ='ibqryKolhoz' then
      begin
        if Pos('order',_s1) <>0 then _s1 := Copy(_s1, 1, Pos('order',_s1)-1);
         _s1 := _s1 + ' where upper(kk.name) like upper('+ _s+')';
      end;
      if dbgrd1.DataSource.DataSet.Name ='ibqryUSE' then
      begin
        if Pos('order',_s1) <>0 then _s1 := Copy(_s1, 1, Pos('order',_s1)-1);
         _s1 := _s1 + ' where upper(put.name) like upper('+ _s+')';
      end;
      if dbgrd1.DataSource.DataSet.Name ='ibqryRights' then
      begin
        if Pos('order',_s1) <>0 then _s1 := Copy(_s1, 1, Pos('order',_s1)-1);
         _s1 := _s1 + ' where upper(rt.name) like upper('+ _s+')';
      end;
      if dbgrd1.DataSource.DataSet.Name ='ibqryOwners' then
      begin
         _s1 := Copy(_s1, 1, Pos('union',_s1)-1)+ ' where upper(pp.family) like upper('+ _s+')' +' union ' + Copy(_s1, Pos('union',_s1)+5, Length(_s1))+ ' where upper(ee.full_name) like upper('+ _s+')';
      end;

        if ibdtstFind.Active then ibdtstFind.Close;
        ibdtstFind.SelectSQL.Clear;
        ibdtstFind.SelectSQL.Add(_s1);
        ibdtstFind.Open;
        if not ibdtstFind.IsEmpty then
        begin
        if Pos('t_name_local_prefix', _s1)<> 0 then
          begin
            ibdtstFind.Fields[0].Visible := False;
            ibdtstFind.Fields[2].Visible := False;
            ibdtstFind.Fields[1].DisplayLabel :='������������� ������';
          end;
        if (Pos('t_settle', _s1)<> 0) then
          begin
            ibdtstFind.Fields[0].Visible := False;
            ibdtstFind.Fields[2].Visible := true;
            ibdtstFind.Fields[1].DisplayLabel := '�������� ���������';
            ibdtstFind.Fields[2].DisplayLabel := '������������� �����';
          end;
        if (Pos('t_kolkhoz', _s1)<> 0)  then
          begin
            ibdtstFind.Fields[0].Visible := False;
            ibdtstFind.Fields[2].Visible := true;
            ibdtstFind.Fields[1].DisplayLabel := '�������';
            ibdtstFind.Fields[2].DisplayLabel := '�������� ���������';
            ibdtstFind.Fields[3].DisplayLabel := '������������� ������';
          end;
        if (Pos('t_permitted_use_type', _s1)<> 0)  then
          begin
            ibdtstFind.Fields[0].Visible := False;
            ibdtstFind.Fields[2].Visible := true;
            ibdtstFind.Fields[1].DisplayLabel := '���';
            ibdtstFind.Fields[2].DisplayLabel := '��� �������������';
          end;
        if (Pos('t_right_type', _s1)<> 0)  then
          begin
            ibdtstFind.Fields[0].Visible := False;
            ibdtstFind.Fields[1].Visible := false;
            ibdtstFind.Fields[2].Visible := true;
            ibdtstFind.Fields[1].DisplayLabel := '���� �����';
          end;
        if (Pos('t_person ', _s1)<> 0)  then
          begin
            ibdtstFind.Fields[0].Visible := False;
            ibdtstFind.Fields[1].Visible := true;
            ibdtstFind.Fields[2].Visible := false;
            ibdtstFind.Fields[3].Visible := true;
            ibdtstFind.Fields[4].Visible := true;
            ibdtstFind.Fields[1].DisplayLabel := '�����������';
            ibdtstFind.Fields[2].DisplayLabel := '��� ������������';
            ibdtstFind.Fields[3].DisplayLabel := '������������� �����';
          end;

        tempDs := TIBQuery(dbgrd1.DataSource.DataSet);
        dbgrd1.DataSource.DataSet := ibdtstFind;
        dbgrd1.Columns[0].Width := 250;
        if dbgrd1.Columns.Count >1 then dbgrd1.Columns[1].Width := 200;
        if dbgrd1.Columns.Count >2 then dbgrd1.Columns[2].Width := 200;
        if (Pos('t_person ', _s1)<> 0)  then
        begin
          dbgrd1.Columns[0].Width := 350;
          dbgrd1.Columns[1].Width := 150;
        end;
        if (Pos('t_permitted_use_type', _s1)<> 0)  then
        begin
          dbgrd1.Columns[0].Width := 100;
          dbgrd1.Columns[1].Width := 550;
        end;
        btnFind.Caption := '��������';
        end
        else
          ShowMessage('����������� � ����� ��������� ���!');
   end;
  end
  else
  if (btnFind.Caption = '��������') then
  begin
     dbgrd1.DataSource.DataSet:=tempDs;
     if ibdtstFind.Active then ibdtstFind.Close;
     btnFind.Caption := '�����'end;
end;

procedure TfSpr.btnOkClick(Sender: TObject);
begin
 ModalResult := mrOK;
end;

procedure TfSpr.dbgrd1CellClick(Column: TColumn);
begin
 _FF := True;
end;

procedure TfSpr.dbgrd1DrawColumnCell(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumn; State: TGridDrawState);
var ff : Boolean; i, oldcolor, FoundIndex : Integer;
begin
 if not _ff then
 begin
  //ff := False;
  if masId <> nil then
  begin
    i := Low(masId);
    FoundIndex := -1;
    
    if TArray.BinarySearch<Double>(masId, self.dbgrd1.DataSource.DataSet.FieldByName('ID').Value, FoundIndex) then
    begin
      self.dbgrd1.Canvas.Brush.Color := clmoneygreen;//$FFFF;
      self.dbgrd1.DefaultDrawColumnCell(Rect, DataCol, Column, State);
    end;
  end;
 end;
end;


procedure TfSpr.FormCreate(Sender: TObject);
begin
  _ff := False;
end;

procedure TfSpr.FormShow(Sender: TObject);
begin
 if dbgrd1.DataSource.DataSet.Name ='ibqryOwners' then
 begin
 dbgrd1.Columns[0].Width := 350;
 dbgrd1.Columns[1].Width := 150;
 dbgrd1.Columns[2].Width := 220;
 end;
end;

function TfSpr.getCaptions(pSTR : string=''): string;
var i ,j, kk: Integer;   bm:TBookmark; _s,_s1: string;
begin
  if dbgrd1.SelectedRows.Count <> 0 then
  begin
    _s := '';
    for I := 0 to dbgrd1.SelectedRows.Count-1 do
    begin
     bm := dbgrd1.SelectedRows.Items[i];
     dbgrd1.DataSource.DataSet.Bookmark := tbookmark(bm);
     _s := _s + dbgrd1.DataSource.DataSet.FieldByName('Name').Value+',';
    end;
    Result := Copy(_s,1,length(_s)-1);
  end
  else
  begin
   if pSTR <>'' then
    BEGIN
     _s := '';
     _s1 :='';
     kk := CountPos(',',pSTR)+1;
     j := Pos(',',pSTR) ;
     if j<>0 then
     begin
     _s1 := Copy(pSTR, 2,j-2);
     if dbgrd1.DataSource.DataSet.Locate('ID', _s1.ToDouble,[]) then  _s := _s + dbgrd1.DataSource.DataSet.FieldByName('Name').Value+',';
     for I := 1 to kk-2 do
      begin
        _s1 := Copy(pSTR, j+1, PosEx(',',pSTR, j+1)-j-1);
        if dbgrd1.DataSource.DataSet.Locate('ID', _s1.ToDouble,[]) then  _s := _s + dbgrd1.DataSource.DataSet.FieldByName('Name').Value+',';
        j := PosEx(',',pSTR, j+1);
      end;
     _s1 := copy(pSTR, j+1, Pos(')',pSTR)-j-1);
     if dbgrd1.DataSource.DataSet.Locate('ID', _s1.ToDouble,[]) then  _s := _s + dbgrd1.DataSource.DataSet.FieldByName('Name').Value+',';
     Result := Copy(_s,1,length(_s)-1);
     end
     else
      begin
        _s1 :=  Copy(pSTR, 2,Length(pSTR)-2);
        if dbgrd1.DataSource.DataSet.Locate('ID', _s1.ToDouble,[])
        then
         Result := dbgrd1.DataSource.DataSet.FieldByName('Name').Value
        else
         Result := '';
      end;
    END
   else Result := '';
  end;
end;

function TfSpr.getIDs: string;
var i : Integer;   bm:TBookmark; _s, _sf, _su: string;
begin
 { if dbgrd1.SelectedRows.Count = 0 then  // ���� ������ �� ������� �� ��������� ���������� ��������
  begin

  end
  else  }

  //if (dbgrd1.DataSource.DataSet.Name <>'ibqryOwners') or ((dbgrd1.DataSource.DataSet.Name = 'ibdtstFind') and (dbgrd1.DataSource.DataSet.Fields.Count <>5)) //(Pos('���������� ����', TIBQuery(dbgrd1.DataSource.DataSet).sql.Text)=0))
  if (dbgrd1.DataSource.DataSet.Name ='ibqryOwners') or ((dbgrd1.DataSource.DataSet.Name = 'ibdtstFind') and (dbgrd1.DataSource.DataSet.Fields.Count =5))
  then
    begin
      _su := '';
     _sf := '';
    // _s  := '';
     for I := 0 to dbgrd1.SelectedRows.Count-1 do
      begin
       bm := dbgrd1.SelectedRows.Items[i];
       dbgrd1.DataSource.DataSet.Bookmark := tbookmark(bm);
       if dbgrd1.DataSource.DataSet.Fields[2].AsString = '1'
       then _sf := _sf + dbgrd1.DataSource.DataSet.Fields[0].AsString+',';
       if dbgrd1.DataSource.DataSet.Fields[2].AsString = '2'
       then _su := _su + dbgrd1.DataSource.DataSet.Fields[0].AsString+',';
      end;
      if _su <> '' then
       begin
         _su[Length(_su)] :=')';
         _su := '�('+_su;
       end;
      if _sf <> '' then
       begin
        _sf[Length(_sf)] :=')';
        _sf := '�('+_sf;
       end;

      Result :=_su+_sf;
    end
  else
   begin
      _s := '';
      for I := 0 to dbgrd1.SelectedRows.Count-1 do
      begin
       bm := dbgrd1.SelectedRows.Items[i];
       dbgrd1.DataSource.DataSet.Bookmark := tbookmark(bm);
       _s := _s + dbgrd1.DataSource.DataSet.Fields[0].AsString+',';
      end;
      _s[Length(_s)] :=')';
      Result :='('+_s;
   end;
end;

procedure TfSpr.N1Click(Sender: TObject);
var i : Integer; bm : TBookmark;
begin
  bm := dbgrd1.DataSource.DataSet.GetBookmark;
  dbgrd1.SelectedRows.Clear;
  dbgrd1.DataSource.DataSet.First;
  while not dbgrd1.DataSource.DataSet.Eof do
  begin
   dbgrd1.SelectedRows.CurrentRowSelected := True;
   dbgrd1.DataSource.DataSet.Next;
  end;
  dbgrd1.DataSource.DataSet.Bookmark := TBookmark(bm);
end;

procedure TfSpr.N2Click(Sender: TObject);
begin
 dbgrd1.SelectedRows.Clear;
end;

procedure TfSpr.setIDs(aValues: string);
var i,j,k : Integer; st : string;  stu,stf : string;
begin
 masId := nil;
 if Trim(aValues)<>'' then
 begin

 // ������� � �������������� ����������� ������������� ����� �����. ��� �� �����.
 // ��� ��� ��������� ���� ��� ��� ���������� ��� ���� ��������� ��������� � ������� �� �� � ��� ����.
 // ��� ��� ���� � ������ ���� ������ - �� ����������

{ if (pos('�',aValues) <> 0) and (Pos('�', aValues)<>0) then
  begin
   {stu := Copy(aValues, 3, Pos('�', aValues)-2);
   stf := Copy(aValues, Pos('�', aValues)+2, Length(aValues)-length(stu)-3);
   i := CountPos(',',stu); }

{  end
 else
 if (pos('�',aValues) <> 0) or (Pos('�', aValues)<>0) then
  begin
   i := CountPos(',',aValues);
   if (i=0) and (Length(aValues)>2) then
   begin
    SetLength(masId, 1);
    if (aValues[1] = '�') or (aValues[1] = '�')
     then st := Copy(aValues,3,Length(aValues)-2)
     else st := Copy(aValues,2,Length(aValues)-2);
    masId[0] := st.todouble; //strtofloat(Copy(aValues,2,Length(aValues)-2));
   end
   else
   if i<> 0 then
   begin
     SetLength(masId, i+1);
     k := pos(',',aValues);
     if (aValues[1] = '�') or (aValues[1] = '�')
     then st := Copy(aValues,3,Length(aValues)-2)
     else st := Copy(aValues,2,k-2);
     masId[0] := st.todouble;

     for j := 1 to i-1 do
      begin
       st := Copy(aValues, k+1, posEx(',',aValues,k+1)-k-1);
       masId[j] := st.ToDouble;
       k :=posEx(',',aValues,k+1);
      end;
     st := Copy(aValues,k+1, Pos(')',aValues)-k-1);
     masId[i] := st.ToDouble;
 end;
  end
 else
 begin     }
 if (pos('�',aValues) = 0) and (Pos('�', aValues)=0) then
 begin
 i := CountPos(',',aValues);
 if (i=0) and (Length(aValues)>2) then
 begin
  SetLength(masId, 1);
  st := Copy(aValues,2,Length(aValues)-2);
  masId[0] := st.todouble; //strtofloat(Copy(aValues,2,Length(aValues)-2));
 end
 else
 if i<> 0 then
 begin
   SetLength(masId, i+1);
   k := pos(',',aValues);
   st := Copy(aValues,2,k-2);
   masId[0] := st.todouble;

   for j := 1 to i-1 do
    begin
     st := Copy(aValues, k+1, posEx(',',aValues,k+1)-k-1);
     masId[j] := st.ToDouble;
     k :=posEx(',',aValues,k+1);
    end;
   st := Copy(aValues,k+1, Pos(')',aValues)-k-1);
   masId[i] := st.ToDouble;

 end;
 end;
 end;
// end;
end;



end.
