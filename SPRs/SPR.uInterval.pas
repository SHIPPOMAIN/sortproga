unit spr.uInterval;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Buttons, Vcl.ExtCtrls;

type
  TForm4 = class(TForm)
    pnl3: TPanel;
    btnOk: TBitBtn;
    btnCancel: TBitBtn;
    pnl1: TPanel;
    edt2: TEdit;
    lbl2: TLabel;
    edt1: TEdit;
    lbl1: TLabel;
    procedure btnOkClick(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form4: TForm4;
  function Execute(pFormName : string) : string;
implementation

{$R *.dfm}
function Execute(pFormName : string) : string;
var i : Integer; s1,s2 : string;
begin
  form4.Caption := pFormName;
  form4.edt1.Text:='';
  form4.edt2.Text:='';
  i:= form4.ShowModal;
  if i =mrOK then
  begin
   if (Trim(form4.edt1.Text) <> '') or (Trim(form4.edt2.Text)<>'')
   then
     begin
      if (Trim(form4.edt1.Text) <> '') then s1 :=Trim(form4.edt1.Text)
                                       else s1 :='0';
      if (Trim(form4.edt2.Text) <> '') then s2 :=Trim(form4.edt2.Text)
                                       else s2 :='+';
      Result := s1+','+s2;
     end
   else
     Result := '';
  end
  else
    Result := '';

end;
procedure TForm4.btnCancelClick(Sender: TObject);
begin
  Close;
end;

procedure TForm4.btnOkClick(Sender: TObject);
begin
  ModalResult := mrOK;
end;

end.
