unit spr.uPeriod;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Buttons, Vcl.ExtCtrls,
  Vcl.Mask,System.DateUtils;

type
  TForm2 = class(TForm)
    pnl3: TPanel;
    btnOk: TBitBtn;
    btnCancel: TBitBtn;
    pnl1: TPanel;
    edt3: TEdit;
    medt2: TMaskEdit;
    medt1: TMaskEdit;
    lbl3: TLabel;
    lbl2: TLabel;
    lbl1: TLabel;
    procedure btnOkClick(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form2: TForm2;
  function Execute() : string;
implementation

{$R *.dfm}
function Execute() : string;
var i,_cc : Integer;  _s1,_s2 : string; _d1,_d2: TDateTime;
begin
  form2.medt1.Text := '';
  Form2.medt2.Text := '';
  Form2.edt3.Text := '';
  i:= form2.ShowModal;
  if i =mrOK then
  begin
   if (form2.medt1.Text <> '  .  .    ') and (form2.medt2.Text <> '  .  .    ') and (Trim(form2.medt1.Text) <> '')  and (Trim(form2.medt2.Text) <> '')
   then
     begin
       _s1 :=Trim(form2.medt1.Text);
       _s2 :=Trim(form2.medt2.Text);
       _d1 := StrToDate(_s1);
       _d2 := StrToDate(_s2);
       _cc := DaysBetween(_d2,_d1);
         Result :=IntToStr(_cc div 365);  //Trim(form3.edt1.Text)
     end
   else
     if (Trim(form2.edt3.Text) <> '')
     then
       Result := Trim(form2.edt3.Text)
     else
       Result := '';
  end
  else
    Result := '';

end;

procedure TForm2.btnCancelClick(Sender: TObject);
begin
 Close;
end;

procedure TForm2.btnOkClick(Sender: TObject);
begin
 ModalResult := mrOK;
end;

end.
