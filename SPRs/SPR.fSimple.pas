unit spr.fSimple;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Buttons, Vcl.ExtCtrls;

type
  TForm3 = class(TForm)
    pnl3: TPanel;
    btnOk: TBitBtn;
    btnCancel: TBitBtn;
    pnl1: TPanel;
    edt1: TEdit;
    lbl1: TLabel;
    procedure btnOkClick(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form3: TForm3;
  function Execute(pFormName : string) : string;
implementation

{$R *.dfm}
function Execute(pFormName : string) : string;
var i : Integer;
begin
  form3.Caption := pFormName;
  form3.edt1.Text := '';
  i:= form3.ShowModal;
  if i =mrOK then
  begin
   if Trim(form3.edt1.Text) <> ''
   then
     Result := Trim(form3.edt1.Text)
   else
     Result := '';
  end
  else
    Result := '';

end;
procedure TForm3.btnCancelClick(Sender: TObject);
begin
 Close;
end;

procedure TForm3.btnOkClick(Sender: TObject);
begin
 ModalResult := mrOK;
end;

end.
