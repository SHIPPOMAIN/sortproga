object fSpr: TfSpr
  Left = 0
  Top = 0
  Cursor = crHourGlass
  Caption = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082
  ClientHeight = 515
  ClientWidth = 761
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnl1: TPanel
    Left = 0
    Top = 0
    Width = 761
    Height = 57
    Align = alTop
    TabOrder = 0
    object lbl1: TLabel
      Left = 25
      Top = 21
      Width = 81
      Height = 13
      Caption = #1055#1086#1080#1089#1082' '#1087#1086' '#1084#1072#1089#1082#1077':'
    end
    object edtMask: TEdit
      Left = 112
      Top = 17
      Width = 434
      Height = 22
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
    end
    object btnFind: TBitBtn
      Left = 653
      Top = 1
      Width = 107
      Height = 55
      Align = alRight
      Caption = #1055#1086#1080#1089#1082
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      OnClick = btnFindClick
    end
  end
  object pnl2: TPanel
    Left = 0
    Top = 57
    Width = 761
    Height = 397
    Align = alClient
    TabOrder = 1
    object dbgrd1: TDBGrid
      Left = 1
      Top = 1
      Width = 759
      Height = 395
      Align = alClient
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgMultiSelect, dgTitleClick]
      ParentFont = False
      PopupMenu = pm1
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
      OnCellClick = dbgrd1CellClick
      OnDrawColumnCell = dbgrd1DrawColumnCell
    end
  end
  object pnl3: TPanel
    Left = 0
    Top = 454
    Width = 761
    Height = 61
    Align = alBottom
    TabOrder = 2
    object btnOk: TBitBtn
      Left = 543
      Top = 1
      Width = 110
      Height = 59
      Align = alRight
      Anchors = [akRight, akBottom]
      Caption = #1042#1099#1073#1088#1072#1090#1100
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      Kind = bkOK
      NumGlyphs = 2
      ParentFont = False
      TabOrder = 0
      OnClick = btnOkClick
    end
    object btnCancel: TBitBtn
      Left = 653
      Top = 1
      Width = 107
      Height = 59
      Align = alRight
      Anchors = [akRight, akBottom]
      Caption = #1054#1090#1084#1077#1085#1072
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      Kind = bkCancel
      NumGlyphs = 2
      ParentFont = False
      TabOrder = 1
      OnClick = btnCancelClick
    end
  end
  object pm1: TPopupMenu
    Left = 376
    Top = 409
    object N1: TMenuItem
      Caption = #1042#1099#1076#1077#1083#1080#1090#1100' '#1074#1089#1077
      OnClick = N1Click
    end
    object N2: TMenuItem
      Caption = #1057#1085#1103#1090#1100' '#1074#1099#1076#1077#1083#1077#1085#1080#1077
      OnClick = N2Click
    end
  end
  object ibdtstFind: TIBDataSet
    Database = dm.ibdtbs1
    Transaction = dm.ibtrnsctn1
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    UniDirectional = False
    Left = 320
    Top = 408
  end
end
