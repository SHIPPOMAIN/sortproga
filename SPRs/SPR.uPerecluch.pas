unit spr.uPerecluch;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Buttons, Vcl.ExtCtrls;

type
  TForm1 = class(TForm)
    pnl3: TPanel;
    btnOk: TBitBtn;
    btnCancel: TBitBtn;
    rg1: TRadioGroup;
    procedure btnOkClick(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  function Execute(pFormName, rgNmae : string) : string;
implementation

{$R *.dfm}

function Execute(pFormName, rgNmae : string) : string;
var i : Integer; s1,s2 : string;
begin
  form1.Caption := pFormName;
  form1.rg1.Caption := rgNmae;
  form1.rg1.ItemIndex := -1;
  i:= form1.ShowModal;
  if i =mrOK then
  begin
    if Form1.rg1.ItemIndex = 0 then Result := 'true'
    else
    if Form1.rg1.ItemIndex = 1 then Result := 'false'
    else
      Result :=''
  end
  else
    Result := '';

end;

procedure TForm1.btnCancelClick(Sender: TObject);
begin
 Close;
end;

procedure TForm1.btnOkClick(Sender: TObject);
begin
 ModalResult := mrOK;
end;

end.
