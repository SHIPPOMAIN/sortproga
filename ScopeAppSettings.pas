unit ScopeAppSettings;

interface
uses

VCL.Forms,
System.IOUtils;

type
 // �������� ������ � "������" ����������
 ScopePath = record
  private
   const cLogo = 'logo.splash';
   class var
    fUser: string;
    fSplashImage: string;
  public
   class procedure Init(const pApp: TApplication); static;
   class property User: string read fUser;
   class property SplashImage: string read fSplashImage;
   class function ExistSplashImage: boolean; static;
 end;

implementation

{ ScopePath }

class function ScopePath.ExistSplashImage: boolean;
begin
 Result:= TFile.Exists(ScopePath.fSplashImage);
end;

class procedure ScopePath.Init(const pApp: TApplication);
begin
 ScopePath.fUser:=  TPath.GetDirectoryName(Application.ExeName)+TPath.DirectorySeparatorChar;
 ScopePath.fSplashImage:= ScopePath.fUser+ScopePath.cLogo;
end;

end.
