unit uDMReportPicture;

interface

uses
  System.SysUtils, System.Classes,
  frxClass, frxDBSet,frxExportRTF, frxExportPDF,
  IdIOHandler, IdIOHandlerSocket, IdIOHandlerStack, IdSSL, IdSSLOpenSSL,
  IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient, IdHTTP,
  Vcl.Imaging.jpeg,
  Vcl.Dialogs,
  Vcl.Forms;

type
  TdmRP = class(TDataModule)
    frxpdfxprt1: TfrxPDFExport;
    frxdbdtst2: TfrxDBDataset;
    frxrtfxprt1: TfrxRTFExport;
    frxdbdtst5: TfrxDBDataset;
    frxdbdtst1: TfrxDBDataset;
    frxrprt1: TfrxReport;
    idslhndlrscktpnsl1: TIdSSLIOHandlerSocketOpenSSL;
    idhtpHttp: TIdHTTP;
    frxdbdtst3: TfrxDBDataset;
    frxdbdtst4: TfrxDBDataset;
  private
    { Private declarations }
  public
    { Public declarations }
    procedure OpenReport(pCadNUm:string);
    function GetIMAGEJPEG(comm: string): TJPEGImage;
    procedure LoadShablon;
    procedure INItProxy;
  end;

var
  dmRP: TdmRP;

implementation
   uses uPasportCreatePicture,
        uDM,
        Units.Paths,
        uText,
        uThreadPicture ;

   var  MyThread: TMyThread;
{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TdmRP }

procedure TdmRP.INItProxy;
begin
  idhtpHttp.ProxyParams.ProxyServer := AnPAths.Server_proxy;
  idhtpHttp.ProxyParams.ProxyPort := AnPAths.Server_proxy_port;
end;

procedure TdmRP.LoadShablon;
begin
 if not frxrprt1.LoadFromFile(AnPAths.Report_File_shablon)
   then MessageDlg(rsErrorLoadPasportShablon,mtError,[mbOK],-1);

 if AnPAths.Reports_path <> '' then
  begin
   //frxrtfxprt1.DefaultPath := AnPAths.Reports_path;
   //frxpdfxprt1.DefaultPath := AnPAths.Reports_path;
  end;
end;

procedure TdmRP.OpenReport(pCadNUm: string);
var
    pic1 : TfrxPictureView;
    Mashtab_report : TfrxMemoView;
    MGISAREA : TfrxSysMemoView;
    pictureCreator2 : TPictureReport;
    rarea : TREADMGISAREA;
begin
  if pCadNUm <> '' then
    begin
      DM.OPenReport(pCadNUm);

      pictureCreator2 := TPictureReport.Create;

      pic1 := frxrprt1.FindObject('Picture1')as TfrxPictureView;
      //pic1.Picture.Graphic:= pictureCreator.GetIMAGE(pictureCreator.SendCoordForPicture);
      pictureCreator2.GETPIcture(pCadNUm,0,0,2);
      pic1.Picture.Graphic:= pictureCreator2.reportImage;

      Mashtab_report := frxrprt1.FindObject('MemoMashtab')as TfrxMemoView;
      pictureCreator2.GetMashtab;
      Mashtab_report.Text := '1:'+pictureCreator2.ReportMashtab;

      MGISAREA := frxrprt1.FindObject('SysMemo1')as TfrxSysMemoView;
      rarea := TREADMGISAREA.Create;
      rarea.readMGISAREA(pCadNUm);
      MGISAREA.Text := rarea._MGISAREA.ToString+' ��.�.';
      FreeAndNil(rarea);

      frxrprt1.ShowReport(true);

      FreeAndNil(pictureCreator2);
      dm.CloseReport;
    end;
end;


function TdmRP.GetIMAGEJPEG(comm: string): TJPEGImage;
 var
  _mstr:TMemoryStream;    ss:string;
begin
 try
  //Application.MessageBox(PWideChar(comm), 'comm',1);
  //ss := InputBox('Test program', '��', comm);
  _mstr:=TMemoryStream.Create;
  //ShowMessage('1');
  idhtpHttp.Get(comm, _mstr);
  //ShowMessage('2');
  _mstr.Seek(0, TSeekOrigin.soBeginning);
  // ShowMessage('3');
  Result:=TJPEGImage.Create;
  //  ShowMessage('4');
  Result.LoadFromStream(_mstr);
  //  ShowMessage('5');
 _mstr.Free;
 except on E : Exception do
 begin
 // ShowMessage(E.Message);
  MessageDlg(rsErrorCreatePictere,mtError,[mbOK],-1);
 end;
 end;
end;
end.
