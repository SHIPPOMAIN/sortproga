unit Views.tfmStat;

interface

uses
  Winapi.Windows, Winapi.Messages, System.Generics.Collections  ,System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ComCtrls, Vcl.ExtCtrls,
  Vcl.Grids, Vcl.DBGrids, Vcl.Menus, Vcl.Buttons,PngImage,Views.tfmTable;

type

  TfmStat = class(TForm)
    pnl1: TPanel;
    btn1: TSpeedButton;
    btn11: TSpeedButton;
    btn2: TSpeedButton;
    btn13: TButton;
    btn10: TSpeedButton;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btn10Click(Sender: TObject);
    procedure btn1Click(Sender: TObject);
    procedure btn11Click(Sender: TObject);
    procedure btn13Click(Sender: TObject);
  private
    { Private declarations }

    procedure CMVisibleChanged(var Message: TMessage); message CM_VISIBLECHANGED;
  public
    { Public declarations }
    fMasterVisible: boolean;
  end;



var
  fmStat: TfmStat;

implementation
uses UCreateSql, uDM, ScopeAppSettings, uText, uWorkShablon,  uRedactorSHb;
{$R *.dfm}

procedure TfmStat.btn10Click(Sender: TObject);
begin
 Form5.Show;
end;


procedure TfmStat.btn11Click(Sender: TObject);  //?????????????????
begin
 if fmTable.dbgrdResult.DataSource.DataSet.Active then  fmTable.Show
 else
 ShowMessage(rsBeforeInQuery);
end;



procedure TfmStat.btn13Click(Sender: TObject);
var _strVal : string;
begin
 _strVal := InputBox('������� ������', '������=', '���,����,��,��1,��1,��2,��3,��4,��5,��6,��7 ��� ���=(10);����=36:10:000000;��=0,1000;��1=true,1;��1=true,0;��2=true,0;��3=true,0;��4=true,0;��5=true,0;��6=true,0;��7=true,0;����=�(100000,200)�(33,456);') ;
 fRedactShbl.SetCurrentQuery(_strVal);
// test;

end;

procedure TfmStat.btn1Click(Sender: TObject);
begin
 fRedactShbl.Show;
end;


procedure TfmStat.CMVisibleChanged(var Message: TMessage);
begin
 if not self.fMasterVisible then self.Visible:= false
 else
  inherited;
end;


procedure TfmStat.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  //fConnect.Close;
end;


end.
