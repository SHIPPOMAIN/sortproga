unit Units.Utils;

interface
uses
 System.Sysutils,
 uParser;

 type
  requestscope = record
   class function decode(pAtt: tAttributs; pValue:string; _per:integer):string; static;
   class procedure CreateAndRunSql(_sql:string); static;
  end;

implementation
uses uDM, UCreateSql;

{ requestscope }

class procedure requestscope.CreateAndRunSql(_sql: string);
begin
 dm.inquiry(CreateSqlbyString(_sql));
end;

class function requestscope.decode(pAtt: tAttributs; pValue: string;
  _per: integer): string;
var ss:TArray<string>;  dim : string;
begin
 //������������ ��������, ������ ��� �� ���������� �����:
 //"�������� �� �������..."

 // _per - ��������� ������ ������� ��������� - 1 - ���� ������� �� ��������� - ��� ���������� ����� ��������;
 //                                             2 - ����� ���� �� SetCurrentQuery, � ��������� �������������� ������ ��� ����������� �����

  dim := ',' ;
  if pAtt = (aCadNum) then result:= '�������� � ������ '+pValue;
  if pAtt = (aArea) then
     begin
       if pvalue <>'' then
       begin
       ss := pvalue.split(dim,ExcludeEmpty);
       result:= '����� ������� ������ '+ ss[0]+ ',�� � ������ ���� ����� '+ss[1]+',��';
       end
       else
        result:= '����� ������� ������ 0,��' ;
     end;
  if pAtt = (aRaion) then if _per=1 then result:= '����������� � ����� �� ��������� ������������� ������� :'+pValue else result:= '����������� � ����� �� ��������� ������������� ������� :'+dm.GetIDNAMES(pValue, pAtt);
  if pAtt = (aSP) then if _per=1 then result:= '����������� � ����� �� ��������� �������� ��������� :'+pValue else result:= '����������� � ����� �� ��������� �������� ��������� :'+dm.GetIDNAMES(pValue, pAtt);
  if pAtt = (aKLH) then if _per=1 then result:= '����������� � ����� �� ��������� �������� :'+pValue else result:= '����������� � ����� �� ��������� �������� :'+dm.GetIDNAMES(pValue, pAtt);
  if pAtt = (aISP) then if _per=1 then result:= '�������� ���� �� ����� �������������: '+pValue else result:= '�������� ���� �� ����� �������������: '+dm.GetIDNAMES(pValue, pAtt);
  if pAtt = (aFISP) then result:= '�������� ���� �� ����������� ����� �������������: '+pValue;
  if pAtt = (aKTG) then if _per=1 then  result:= '������ � ��������� ��������� :'+pValue else result:= '������ � ��������� ��������� :'+dm.GetIDNAMES(pValue, pAtt);
  if pAtt = (aCADCOST) then
     begin
     if pvalue <>'' then
       begin
       ss := pvalue.split(dim,ExcludeEmpty);
       result:=  '����� ����������� ��������� ������ '+ ss[0]+ ' � ������ ���� ����� '+ss[1];
       end
     else
       result:=  '����� ����������� ��������� ������ 0 ���.���';
     end;
  if pAtt in [aGR1,AGR2,aGR3,aGR4,aGR5,aGR6,aGR7] then
    begin
      //if pValue <>'' then ss := pvalue.split(dim,ExcludeEmpty);
      if pValue = '' then
         result:= '�������� � ������� '+cCaptions[pAtt]
      else
         result:= '����� � ������� '+cCaptions[pAtt];
    end;
  if pAtt in [aUG1,aUG2,aUG3,aUG4,aUG5,aUG6,aUG7] then
    begin
    if pValue = '' then
        result:= '�������� � ������� '+cCaptions[pAtt]
    else
        result:= '����� � ������� '+cCaptions[pAtt];
    end;
  if pAtt = (aRight) then if _per=1 then result:= '�������� ���� �� ���� :'+pValue else result:= '�������� ���� �� ���� :'+dm.GetIDNAMES(pValue, pAtt);
  if pAtt = (aSobst) then if _per=1 then result:= '���������������� �� ��������� ������������� :'+pValue else result:= '���������������� �� ��������� ������������� :'+dm.GetIDNAMES(pValue, pAtt);
  if pAtt = (aKUCH) then begin if pValue = 'true' then result:= '������ ����������� ����' else result:= '�� ������ ����������� ����'; end;
  if pAtt = (aUFRS) then begin if pValue = 'true' then result:= '������ ���� ����' else result:= '�� ������ ���� ����'; end;
  if pAtt = (aArenda) then result:= '�������� ������';
  if pAtt = (aPeriod) then result:= '������� ���������� �� ������ '+pValue+ ' ���';
  if pAtt = (aASOBST) then if _per=1 then result:= '���������������� �� ��������� �����������:'+pValue else result:= '���������������� �� ��������� �����������:'+dm.GetIDNAMES(pValue, pAtt);

end;

end.
