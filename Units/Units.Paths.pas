unit Units.Paths;

interface
 uses
   {$IFDEF MSWINDOWS}
   WinAPI.SHFolder,
   WinAPI.Windows,
   {$ENDIF}
   Vcl.Dialogs,
   System.IniFiles,
   JSON,
   system.IOUtils,
   System.Classes,
   System.Types,
   System.SysUtils,
   VCL.Forms,
   FMX.Styles;
type
  tAnaliticaPaths = record
    public
    //lands
    DB_USER_ID: double;
    db_Lands_path : string;
    // postgress
    DB_Postgress_server: string;
    DB_POSTGRESS_PORT:string;
    DB_NAME_POSTGRESS: string;
    DB_USER:string;
    DB_PASS:string;
    //�����
    Server_address : string;
    Server_proxy : string;
    Server_proxy_port:integer;
    //������
    Report_File_shablon :string;
  //  Names_path : string;
    Reports_path : string;

    procedure ClearPath;
    class procedure READINIFILE(); static;
    class procedure WRITEINIFILE(); static;
    class procedure Init(); static;
  end;


  tAnalyticCfg = class    //������ �� �����������
    FModel: TJSONObject;
    fPortable: boolean;
    fFile: string;
    function getItem(aName: string): TJSONObject;
   public
    constructor Create(const aFile: string = '');
    destructor Destroy; override;
    procedure Save;
    procedure SaveAs(const aFile: string);


    function SysPath: string;
    function HomePath: string;
    function HomePathGeneral: string;
    class function DefHomePath:string; static;
    class function DefHomePathGeneral:string; static;
    property Item[Name: string]: TJSONObject read getItem;
    property Portable: boolean read fPortable;
    property CfgFile: string read fFile write fFile;
  end;

const

  cFileIniGEN = 'AISUMZ.ini';//'Ini.ini';
  cFileIni = 'Ini.ini';
  cFileIni2 = 'IniKT.ini';
  cFileNAMESIni  = 'names.txt';
  cReport_File_shablon = 'PasportZU.fr3';

  cDirGroup   = '������';
  cDirProject = 'Analytics';
  cDirReports = 'AnaliticaReports';
  cProjectVersion   = '1.0';
 {{$IFDEF DEBUG}
    //�� ���������
 {   cTEmp_db_Lands_path ='localhost:d:\WORK\AGRILAND\base 22-06-2016\lands22062016.fdb';
    // postgress
    cTEmp_DB_Postgress_server = '192.168.2.252';
    cTEmp_DB_DB_POSTGRESS_PORT = '5432';
    cTEmp_DB_DB_NAME_POSTGRESS = 'ums_test';
    cTEmp_DB_DB_USER='user';//'postgres';
    cTEmp_DB_DB_PASS='user';//'root';
    //�����
//    cTEmp_DB_Server_address ='http://195.98.73.242:7152/umz/service?&SERVICE=WMS&REQUEST=GetMap&VERSION=1.3.0&LAYERS=SH,Settlement,Les&STYLES=&FORMAT=image%2F%Format%&TRANSPARENT=TRUE&HEIGHT=%HEIGHT%&WIDTH=%WIDTH%&WMSID=umz&DETECTRETINA=true&CRS=EPSG%3A3857&BBOX=%BBOX%';
    ctemp_SERVER_PROXY = '192.2.168.28';
    cTEmp_SERVER_PROXY_PORT = '8080';
    cTEmp_DB_Server_address ='http://192.168.2.252:7152/umz/service?&SERVICE=WMS&REQUEST=GetMap&VERSION=1.3.0&LAYERS=SH,Settlement,Les&STYLES=&FORMAT=image%2F%Format%&TRANSPARENT=TRUE&HEIGHT=%HEIGHT%&WIDTH=%WIDTH%&WMSID=umz&DETECTRETINA=true&CRS=EPSG%3A3857&BBOX=%BBOX%';
  {$ELSE}
 {   cTEmp_db_Lands_path ='10.130.1.231:D:\isumz\db\lands.fdb';
    // postgress
    cTEmp_DB_Postgress_server = '195.98.73.242';
    cTEmp_DB_DB_POSTGRESS_PORT = '5432';
    cTEmp_DB_DB_NAME_POSTGRESS = 'ums_test';
    cTEmp_DB_DB_USER='user';//'postgres';
    cTEmp_DB_DB_PASS='user';//'root';
    //�����
    cTEmp_DB_Server_address ='http://195.98.73.242:7152/umz/service?&SERVICE=WMS&REQUEST=GetMap&VERSION=1.3.0&LAYERS=SH,Settlement,Les&STYLES=&FORMAT=image%2F%Format%&TRANSPARENT=TRUE&HEIGHT=%HEIGHT%&WIDTH=%WIDTH%&WMSID=umz&DETECTRETINA=true&CRS=EPSG%3A3857&BBOX=%BBOX%';
    ctemp_SERVER_PROXY = '';
    cTEmp_SERVER_PROXY_PORT = 0;
   {$ENDIF}
   // ���� �� ���������
    cTempUserName = '��������';
    cTempUserPass = 'user';
var
    //NameF : TextFile;
    ACFG : tAnalyticCfg;
    AnPAths : tAnaliticaPaths;
implementation
  uses Uni_WorkIniF;

var inif : TInifile;

{ tAnalyticCfg }

constructor tAnalyticCfg.Create(const aFile: string);
var
 _i:Integer;  _ss :string;
begin
  self.fPortable:= false;
  if aFile='' then begin

  for _i := 1 to ParamCount do  begin
    _ss := ParamStr(_i);
   if _ss ='-portable' then begin
     self.FPortable:= true;
     BREAK;
   end;
  end;

  self.fFile:= TPath.Combine(self.HomePath, cFileIni);
end;
  AnPAths.ClearPath;
end;

class function tAnalyticCfg.DefHomePath: string;
{$IFDEF RELEASE}
var
  _dest,_source,_version:string;
  {$IFDEF MSWINDOWS}
    LStr: array[0 .. MAX_PATH] of Char;
  {$ENDIF}
{$ENDIF}
begin
{$IFNDEF DEBUG}
  _version:=cProjectVersion;
  _dest:=_dest.Join(TPath.DirectorySeparatorChar,[TPath.GetHomePath, cDirGroup, cDirProject, _version]);
  {$IFDEF MSWINDOWS}
    // � ���������� �������� ����� ������������ ������ exe-����� � ����������� � ����
    // �� ���� ��� ���
    SetLastError(ERROR_SUCCESS);
    if SHGetFolderPath(0, CSIDL_COMMON_APPDATA, 0, 0, @LStr) = S_OK then begin
      _source:=LStr;
      _source := _source.Join(TPath.DirectorySeparatorChar,[_source, cDirGroup, cDirProject, _version]);
    end else
      _source:= _source.Join(TPath.DirectorySeparatorChar,[TPath.GetHomePath, cDirGroup, cDirProject, _version]);
  {$ELSE}
      _source:= _source.Join(TPath.DirectorySeparatorChar,[TPath.GetHomePath, cDirGroup, cDirProject, _version]);
  {$ENDIF}
  Result:=_dest;

  if NOT TDirectory.Exists(_dest) then
    if TFile.Exists(TPath.Combine(_source,cFileIni)) then
      TDirectory.Copy(_source, Result);

  if TFile.Exists(TPath.Combine(_dest,cFileIni)) then
    Result:=_dest
  else
{$ENDIF}
   Result:=TPath.GetDirectoryName(ParamStr(0));
end;

class function tAnalyticCfg.DefHomePathGeneral: string;
{$IFDEF RELEASE}
var
  _dest,_source,_version:string;
  {$IFDEF MSWINDOWS}
    LStr: array[0 .. MAX_PATH] of Char;
  {$ENDIF}
{$ENDIF}
begin
{$IFNDEF DEBUG}
//  _version:=cProjectVersion;
  _dest:=_dest.Join(TPath.DirectorySeparatorChar,[TPath.GetHomePath, cDirGroup]);
  {$IFDEF MSWINDOWS}
    // � ���������� �������� ����� ������������ ������ exe-����� � ����������� � ����
    // �� ���� ��� ���
    SetLastError(ERROR_SUCCESS);
    if SHGetFolderPath(0, CSIDL_COMMON_APPDATA, 0, 0, @LStr) = S_OK then begin
      _source:=LStr;
      _source := _source.Join(TPath.DirectorySeparatorChar,[_source, cDirGroup]);
    end else
      _source:= _source.Join(TPath.DirectorySeparatorChar,[TPath.GetHomePath, cDirGroup]);
  {$ELSE}
      _source:= _source.Join(TPath.DirectorySeparatorChar,[TPath.GetHomePath, cDirGroup]);
  {$ENDIF}
  Result:=_dest;

  {if NOT TDirectory.Exists(_dest) then
    if TFile.Exists(TPath.Combine(_source,cFileIniGEN)) then
      TDirectory.Copy(_source, Result);      }
  if NOT TFile.Exists(TPath.Combine(_dest,cFileIniGEN)) then
      TFile.Copy(TPath.Combine(_source,cFileIniGEN),TPath.Combine(Result,cFileIniGEN),True);

  if TFile.Exists(TPath.Combine(_dest,cFileIniGEN)) then
    Result:=_dest
  else
{$ENDIF}
   Result:=TPath.GetDirectoryName(ParamStr(0));

end;

destructor tAnalyticCfg.Destroy;
begin
  self.FModel.Free;
  inherited;
end;

function tAnalyticCfg.HomePath: string;
begin
 if self.fPortable then
  Result:=Self.SysPath
 else
  Result:= tAnalyticCfg.DefHomePath;
end;

function tAnalyticCfg.HomePathGeneral: string;
begin
 if self.fPortable then
  Result:=Self.SysPath
 else
  Result:= tAnalyticCfg.DefHomePathGeneral;
end;

class procedure tAnaliticaPaths.Init();
begin
  AnPAths.ClearPath;
  AnPAths.READINIFILE;
end;

class procedure tAnaliticaPaths.READINIFILE;
var ss,vPath : string;
   vSettings : TWorkIniF;
begin
{$IFDEF DEBUG}
  vPath := TPath.Combine(ACFG.HomePath, cFileIniGEN);
{$ELSE}
  vPath := TPath.Combine(ACFG.HomePathGeneral, cFileIniGEN);
 {$ENDIF}

  vSettings := TWorkIniF.Create;
  vSettings.ReadSettings(vPath);

  AnPAths.db_Lands_path      := vSettings.FServer;
  AnPAths.DB_Postgress_server:= vSettings.Pstgserver;
  AnPAths.DB_POSTGRESS_PORT  := IntToStr(vSettings.PSTGPORT);
  AnPAths.DB_USER            := vSettings.PSTGUSER;
  AnPAths.DB_PASS            := vSettings.PSTGPASS;
  AnPAths.DB_NAME_POSTGRESS  := vSettings.PSTGNAME;

  AnPAths.Server_proxy       := vSettings.SERVER_PROXY;
  AnPAths.Server_proxy_port  := vSettings.SERVERPROXYPORT;
  vSettings.Free;

  {$IFDEF DEBUG}
  vPath := TPath.Combine(ACFG.HomePath, cFileIni2);
  {$ELSE}
  vPath := TPath.Combine(ACFG.HomePath, cFileIni);
 {$ENDIF}
  IniF := TiniFile.Create(vPath);
  AnPAths.Server_address :=Inif.ReadString('SERVER','SERVER_ADDRESS','');

  ss := TPath.Combine(ACFG.HomePath, cReport_File_shablon);
  AnPAths.Report_File_shablon :=Inif.ReadString('Report','Report_File_shablon',ss);
  if AnPAths.Report_File_shablon ='' then AnPAths.Report_File_shablon := ss;

  ss := TPath.Combine(ACFG.HomePath, cDirReports);
  AnPAths.Reports_path :=Inif.ReadString('Report','Reports_path',ss);
  if AnPAths.Reports_path ='' then AnPAths.Reports_path := ss;
  Inif.Free;
 { AnPAths.db_Lands_path := Inif.ReadString('Lands','DB_LANDS_path','');
  AnPAths.DB_Postgress_server:=Inif.ReadString('Postgress','DB_Postgress_server','');
  AnPAths.DB_POSTGRESS_PORT:=Inif.ReadString('Postgress','DB_POSTGRESS_PORT','');
  AnPAths.DB_USER:=Inif.ReadString('Postgress','DB_USER','');
  AnPAths.DB_PASS:=Inif.ReadString('Postgress','DB_PASS','');
//  AnPAths.DB_USER:=cTEmp_DB_DB_USER;
//  AnPAths.DB_PASS:=cTEmp_DB_DB_PASS;
  AnPAths.DB_NAME_POSTGRESS:=Inif.ReadString('Postgress','DB_NAME_POSTGRESS','');

  AnPAths.Server_address :=Inif.ReadString('SERVER','SERVER_ADDRESS','');
  AnPAths.Server_proxy :=Inif.ReadString('SERVER','SERVER_PROXY','');
  AnPAths.Server_proxy_port :=Inif.ReadInteger('SERVER','SERVER_PROXY_PORT',0);  }


end;


class procedure tAnaliticaPaths.WRITEINIFILE;
var vPath : string;
   vSettings : TWorkIniF;
begin
 {$IFDEF DEBUG}
  vPath := TPath.Combine(ACFG.HomePath, cFileIniGEN);
 {$ELSE}
  vPath := TPath.Combine(ACFG.HomePathGeneral, cFileIniGEN);
 {$ENDIF}

{  vSettings := TWorkIniF.Create;
  vSettings.FServer    := AnPAths.db_Lands_path;
  vSettings.Pstgserver := AnPAths.DB_Postgress_server;
  vSettings.PSTGPORT   := StrToInt(AnPAths.DB_POSTGRESS_PORT);
  vSettings.PSTGUSER   := AnPAths.DB_USER;
  vSettings.PSTGPASS   := AnPAths.DB_PASS;
  vSettings.PSTGNAME   := AnPAths.DB_NAME_POSTGRESS;
  vSettings.SERVER_PROXY    := AnPAths.Server_proxy;
  vSettings.SERVERPROXYPORT := AnPAths.Server_proxy_port;

  vSettings.WriteSettings(vPath);
  vSettings.Free;                                          }

 {$IFDEF DEBUG}
  vPath := TPath.Combine(ACFG.HomePath, cFileIni2);
 {$ELSE}
  vPath := TPath.Combine(ACFG.HomePath, cFileIni);
 {$ENDIF}
  inif :=  TIniFile.Create(vPath);
  inif.WriteString('SERVER','SERVER_ADDRESS', AnPAths.Server_address);
  inif.WriteString('Report','Reports_path', AnPAths.Reports_path);
  Inif.WriteString('Report','Report_File_shablon',AnPAths.Report_File_shablon);
  Inif.Free;

end;

function tAnalyticCfg.getItem(aName: string): TJSONObject;
var
 _v: TJSONValue;
begin
  Result:= nil;
  if NOT Assigned(self.FModel) then EXIT;
  _v:= self.FModel.Get(aName).JsonValue;
  if not Assigned(_v) then
    self.FModel.AddPair(aName, TJSONObject.Create);
   Result := TJSONObject(self.FModel.Get(aName).JsonValue);
end;

procedure tAnalyticCfg.Save;
begin
 self.SaveAs(self.fFile);
end;

procedure tAnalyticCfg.SaveAs(const aFile: string);
var
  _strmw: TStreamWriter;
begin
  _strmw := TStreamWriter.Create(aFile, false, TEncoding.UTF8);
  _strmw.Write(self.FModel.ToString);
  _strmw.Flush;
  _strmw.Free;
end;


function tAnalyticCfg.SysPath: string;
begin
   Result:= TPath.GetDirectoryName(ParamStr(0));
end;

{ tPaths }

procedure tAnaliticaPaths.ClearPath;
begin
  DB_USER_ID := 0;
  db_Lands_path :='';
  DB_Postgress_server:='';
  DB_POSTGRESS_PORT:='';
  DB_NAME_POSTGRESS:='';
  DB_USER:='';
  DB_PASS:='';
  Server_address :='';
  Server_proxy :='';
  Server_proxy_port:=0;
//  Server_port:='';
  Report_File_shablon :='';
//  INI_path :='';
//  Names_path :='';
  Reports_path :='';
end;

end.
