unit uRedactorSHb;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.Menus,Vcl.Buttons,
  uFR1, {uFR2,}
  System.Generics.Collections,
  uParser,
  UI.Views.tfmTable,
  Vcl.Imaging.pngimage,
  UI.uGeneral,
  uDBFlatAPI;
type
  tFrameChecked =  record
    frami : TFrame3;
    select : Integer; //���� ���������
    where  : Integer; //���� ����� �� �����
    str_wh : string;  //(1,2,3)
    str_sel: string;  //���
    str_prefix : string;
  end;


  TfRedactShbl = class(TForm)
    ctgrypnlgrp1: TCategoryPanelGroup;
    ctgrypnl7: TCategoryPanel;
    chk11: TCheckBox;
    chk12: TCheckBox;
    chk13: TCheckBox;
    ctgrypnl6: TCategoryPanel;
    chk3: TCheckBox;
    chk8: TCheckBox;
    chk9: TCheckBox;
    chk10: TCheckBox;
    ctgrypnl5: TCategoryPanel;
    chk5: TCheckBox;
    chk7: TCheckBox;
    chk6: TCheckBox;
    chk4: TCheckBox;
    chkty3: TCheckBox;
    chkty2: TCheckBox;
    chkTY1: TCheckBox;
    ctgrypnl4: TCategoryPanel;
    chkGr7: TCheckBox;
    chkGR6: TCheckBox;
    chkGR5: TCheckBox;
    chkGR4: TCheckBox;
    chkGR3: TCheckBox;
    chkGR1: TCheckBox;
    chk2: TCheckBox;
    ctgrypnl3: TCategoryPanel;
    chkCadPrice: TCheckBox;
    chkCategory: TCheckBox;
    ctgrypnl2: TCategoryPanel;
    chk1: TCheckBox;
    chkIspZU: TCheckBox;
    ctgrypnl1: TCategoryPanel;
    chkSPos: TCheckBox;
    chkKolhoz: TCheckBox;
    chkSquare: TCheckBox;
    chkCadNUm: TCheckBox;
    chkRayon: TCheckBox;
    pnl3: TPanel;
    scrlbx1: TScrollBox;
    pmMain: TPopupMenu;
    N3: TMenuItem;
    N4: TMenuItem;
    N1: TMenuItem;
    N2: TMenuItem;
    pm1: TPopupMenu;
    MenuItem1: TMenuItem;
    MenuItem2: TMenuItem;
    N5: TMenuItem;
    N6: TMenuItem;
    N7: TMenuItem;
    pnlTools: TPanel;
    Label1: TLabel;
    btnBack: TButton;
    btnNext: TButton;
    procedure chkMain(Sender: TObject);
    procedure MenuItem2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FrameButtonClick(Sender: Tobject);
    procedure FrameCHKCkick(Sender : TOBject);
    procedure N1Click(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure N3Click(Sender: TObject);
    procedure N5Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure N7Click(Sender: TObject);
    procedure btnBackClick(Sender: TObject);
    procedure btnNextClick(Sender: TObject);
    procedure FrameButtonExitClick(Sender: Tobject);
  private
    fBack: tNamedProc;
    fNext: tNamedProc;
    { Private declarations }
    procedure ClearUI;
    procedure setQuery(const Value: string);  // ��������� ������� ������ � ���������� ��������������� �������� �� ����������
    function getQuery():string;
    procedure setUISettings(const Value: tUISettings);
    procedure setBackInfo(const Value: tNamedProc);
    procedure setNext(const Value: tNamedProc);  // �������� ������� ������ �� ������� �������
  public
    { Public declarations }
    _strSHName : string;
    _GR_ID : Double;
    property Query: string read getQuery write setQuery; //������
    property next: tNamedProc read fNext write setNext;
    property back: tNamedProc read fBack write setBackInfo;
  end;

var
  fRedactShbl: TfRedactShbl;
  str_Raionid ,str_SPid  : string;
  framenames : TList<tFrameChecked>;
  i : Integer;
  select, where  : string;
//  sssss : string;

implementation
  uses spr.uSpr, spr.fSimple,  spr.uInterval, spr.uPeriod, spr.uPerecluch, uText,  UCreateSql, uWorkShablon,
  uDM, Units.Utils, Units.DMImages;
{$R *.dfm}



function TfRedactShbl.getQuery: string;
var i:Integer;
begin
Select := '';
where := '';
 for I := 0 to 29 do
 begin
  if framenames[i].select = 1 then select := select+framenames[i].str_sel;
  if framenames[i].where = 1 then where := where+framenames[i].str_prefix+framenames[i].str_wh+';';
 end;

 if Trim(Select)='' then
   begin
      ShowMessage(rsNoneFields);
      Abort;
   end;
 if Trim(where)='' then
   begin
      ShowMessage(rsNoneIf);
      Abort;
   end;

 select := Copy(Select,1,Length(select)-1);
 Result :=  Select+ ' ��� '+ where;
end;


procedure TfRedactShbl.btnBackClick(Sender: TObject);
begin
 // ������� �������� �����
 if Self.Caption<> '��������� ���������� �������' then ClearUI;
 self.fBack.go();
end;

procedure TfRedactShbl.btnNextClick(Sender: TObject);
begin
 self.fNext.go();
end;

procedure TfRedactShbl.chkMain(Sender: TObject);
var _rc : tFrameChecked;  ii:Integer;  _ff : Boolean;
begin

   ii := (Sender as TCheckBox).tag;
   _rc := framenames[ii];

  if (Sender as TCheckBox).Checked then
  begin
   _rc.frami.Visible := True;

   case tAttributs(ii) of
    aRaion : begin str_Raionid := ''; end; // �������� ���������� ����� �������
    aSP    : begin str_SPid := ''; end; // �������� ���������� ����� �������� ���������
    aArenda: begin _rc.str_wh := 'true'; _rc.where := 1; end;
    aSobst : begin if chk12.Checked=False then chk12.Enabled := False; end;      // 7-7-16 ���� �� �� ��������� ���-�� ��� ������������ � ������������� � ����������
    aASOBST: begin if chk3.Checked=False then chk3.Enabled := False; end;        // �������� ������.
   end;
   framenames[ii] := _rc;
  end
  else
  begin
  case tAttributs(ii) of
    aSobst : begin if chk12.Enabled = False then chk12.Enabled := true; end;     // 7-7-16
    aASOBST: begin if chk3.Enabled = False then chk3.Enabled := true;  end;      // 7-7-16
  end;

   if _rc.frami.Checked then _rc.frami.Checked := False;
   _rc.frami.Visible := false;
   _rc.str_wh := '';
   _rc.where := 0;
   _rc.select := 0;
   _rc.frami.Content := requestscope.decode(tAttributs(ii),'',1);//cCaptions[tAttributs(ii)];
   _rc.frami.ChangeLblSize;

   framenames[ii] := _rc;
  end;
end;

procedure TfRedactShbl.ClearUI;
var _rc : tFrameChecked; _i: Integer; _att: tAttributs;
begin
     chkCadNUm.Checked := false;
     chkSquare.Checked := false ;
     chkRayon.Checked := false;
     chkSPos.Checked := false;
     chkKolhoz.Checked := false;
     chkIspZU.Checked := false;
     chk1.Checked := false;
     chkCategory.Checked := false;
     chkCadPrice.Checked := false;
     chkGR1.Checked := false;
     chk2.Checked := false;
     chkGR3.Checked := false;
     chkGR4.Checked := false;
     chkGR5.Checked := false;
     chkGR6.Checked := false;
     chkGr7.Checked := false;
     chk5.Checked := false;
     chk7.Checked := false;
     chk6.Checked := false;
     chk4.Checked := false;
     chkty3.Checked := false;
     chkty2.Checked := false;
     chkTY1.Checked := false;
     chk8.Checked := false;
     chk3.Checked := false;
     chk9.Checked := false;
     chk10.Checked := false;
     chk11.Checked := false;
     chk13.Checked := false;
     chk12.Checked := false;

   for _att :=Low(tAttributs)  to  High(tAttributs) do
   begin
   _rc := framenames[Integer(_att)];
   _rc.frami.Visible := false;
   _rc.str_wh := '';
   _rc.where := 0;
   _rc.select := 0;
   _rc.frami.Checked :=false;
   _rc.frami.Content := requestscope.decode(_att,'',1);//cCaptions[_att];
   _rc.frami.ChangeLblSize;
   framenames[Integer(_att)] := _rc;
   end;
end;

procedure TfRedactShbl.FormClose(Sender: TObject; var Action: TCloseAction);
begin
 Application.MainForm.Close;
end;

procedure TfRedactShbl.FormCreate(Sender: TObject);
var  ff : TFrame3;
     rec : tFrameChecked;
     _att:tAttributs;
begin
  framenames := TList<TFRameChecked>.Create;
 for _att :=Low(tAttributs)  to  High(tAttributs) do
  begin
    ff := TFrame3.Create(Self);
    ff.Name := 'fr'+inttostr(Integer(_att));
    ff.Parent := Self.scrlbx1;
    ff.Visible := False;
    ff.Tag := Integer(_att);
    ff.btn1.Tag := Integer(_att);
    if _att in [aGR1,AGR2,aGR3,aGR4,aGR5,aGR6,aGR7,aUG1,aUG2,aUG3,aUG4,aUG5,aUG6,aUG7] then
     ff.Content := cCaptions[_att]
    else
     ff.Content := requestscope.decode(_att,'',1);//cCaptions[_att];
    ff.PopupMenu := pmMain;
    ff.Align := alTop;
   // if _att in [aGR1,AGR2,aGR3,aGR4,aGR5,aGR6,aGR7,aUG1,aUG2,aUG3,aUG4,aUG5,aUG6,aUG7,aArenda] then
    if _att in [aArenda] then
      ff.btn1.Enabled := False
    else
      ff.btn1.OnClick :=  FrameButtonClick;

    ff.chk1.Tag := Integer(_att);
    ff.chk1.OnClick := FrameCHKCkick;

    ff.spb1.Tag := Integer(_att);
    ff.spb1.OnClick := FrameButtonExitClick;

    rec.frami := ff;
    rec.select := 0;
    rec.where := 0;
    rec.str_sel := cKey[_att]+',';
    rec.str_wh := '';
    rec.str_prefix := cKey[_att]+'=';
    framenames.Add(rec);
  end;
end;

procedure TfRedactShbl.FormShow(Sender: TObject);
begin
  select := '';
  where := '';
  ctgrypnl1.SetFocus;
end;

procedure TfRedactShbl.MenuItem2Click(Sender: TObject);
begin
  self.ClearUI;
end;

procedure TfRedactShbl.FrameButtonClick(Sender: Tobject);
var ss, ss2 : string; _rc : tFrameChecked; _ii: Integer;
begin
  ss :='';
  _rc := framenames[(Sender as TButton).tag];
  _ii := (Sender as TButton).tag;
  ss2 := _rc.str_wh;

  case tAttributs(_ii) of
    aCadNum : ss :=spr.fSimple.Execute('������� ����� ��� ������������ ������ ��');
    aArea   : ss :=spr.uInterval.Execute('������� �������� ��� ������� ��');
    aRaion  : begin
                ss := spr.uSpr.Execute(ss2,uDM.dm.dsRaion, '�������� �������� ������','T_NAME_LOCAL_PREFIX','',False);
                str_Raionid := ss2;
              end;
    aSP:      begin
               ss := spr.uSpr.Execute(ss2,uDM.dm.dsSP, '�������� �������� �������� ���������','T_SETTLE',str_Raionid,False);
               str_SPid := ss2;
              end;
    aKLH   :  ss :=  spr.uSpr.Execute(ss2,uDM.dm.dsKolhoz, '�������� �������','T_KOLKHOZ',str_SPid,False);
    aISP   :  ss :=  spr.uSpr.Execute(ss2,uDM.dm.dsUSE, '�������� ��� �������������','T_PERMITTED_USE','',False);
    aFISP  :  ss :=  spr.fSimple.Execute('������� ����� ��� ������������ �������������');
    aKTG   :  ss :=  spr.uSpr.Execute(ss2,uDM.dm.dsCategory, '�������� ��������� ������','T_CATEGORY','',False);
    aCADCOST: ss :=  spr.uInterval.Execute('������� �������� ��� ����������� ��������� ��');
    aRight :  ss :=  spr.uSpr.Execute(ss2,uDM.dm.dsRights, '�������� ��� �����','T_RIGHT_TYPE','',False);
    aSobst :  begin
               TButton(Sender).Cursor := crSQLWait;
               ss :=  spr.uSpr.Execute(ss2,uDM.dm.dsOwners, '�������� �������������','OWNER ','',False);
               TButton(Sender).Cursor := crArrow;
              end;
    aKUCH   : ss :=spr.uPerecluch.Execute('��������', '����������� ����');
    aUFRS   : ss :=spr.uPerecluch.Execute('��������', '���� ����');
    aPeriod : ss :=spr.uPeriod.Execute();
    aASOBST : begin
                TButton(Sender).Cursor := crSQLWait;
                ss :=  spr.uSpr.Execute(ss2,uDM.dm.dsOwners, '�������� �����������','OWNER ','',False);
                TButton(Sender).Cursor := crArrow;
              end;
    aGR1,AGR2,aGR3,aGR4,aGR5,aGR6,aGR7,aUG1,aUG2,aUG3,aUG4,aUG5,aUG6,aUG7:
              begin
                ss:= 'true,0';
              end;

  end;

  if ss<>'' then
  begin
     if tAttributs(_ii) in [aRaion,aSP,aKLH,aISP,aKTG,aRight,aSobst,aASOBST]
      then
       _rc.str_wh := ss2
      else
       _rc.str_wh := ss;
    _rc.frami.Content := requestscope.decode(tAttributs(_ii),ss,1);
    _rc.frami.ChangeLblSize;
    _rc.where := 1;
  end;
  framenames[(Sender as TButton).tag] := _rc;
end;

procedure TfRedactShbl.FrameButtonExitClick(Sender: Tobject);
var _rc : tFrameChecked; ii: Integer;
    _i,_j: integer;
    _pnl: TCategoryPanelSurface;
begin
   ii := (Sender as TSpeedButton).tag;
  _rc := framenames[ii];

  if _rc.frami.Checked then _rc.frami.Checked := False;
   _rc.frami.Visible := false;
   _rc.str_wh := '';
   _rc.where := 0;
   _rc.select := 0;
   _rc.frami.Content := requestscope.decode(tAttributs(ii),'',1);//cCaptions[tAttributs(ii)];
   _rc.frami.ChangeLblSize;

   framenames[ii] := _rc;

   // � ��������� ������ �������
   for _i:=0 to self.ctgrypnlgrp1.ControlCount-1 do
   if self.ctgrypnlgrp1.Controls[_i] is TCategoryPanel then begin
    _pnl:=  TCategoryPanelSurface(self.ctgrypnlgrp1.Controls[_i].Components[0]);
    for _j:= 0 to _pnl.ControlCount-1 do
     if _pnl.Controls[_j] is TCheckBox then
      if TCheckBox(_pnl.Controls[_j]).Tag = ii then
         TCheckBox(_pnl.Controls[_j]).Checked:= false;
   end;
end;

procedure TfRedactShbl.FrameCHKCkick(Sender: TOBject);
var  i,j : Integer;  _rc : tFrameChecked;
begin
 _rc := framenames[(Sender as TCheckBox).tag];
 if (Sender as TCheckBox).Checked then  _rc.select := 1
                                  else  _rc.select := 0;
 framenames[(Sender as TCheckBox).tag] := _rc;
end;

procedure TfRedactShbl.N1Click(Sender: TObject);
begin
  for I := 0 to framenames.Count-1 do
    begin
      if framenames[i].frami.Visible then framenames[i].frami.Checked:=true;
    end;
end;

procedure TfRedactShbl.N2Click(Sender: TObject);
begin
  for I := 0 to framenames.Count-1 do
    begin
      if framenames[i].frami.Visible then framenames[i].frami.Checked := false;
    end;
end;

procedure TfRedactShbl.N3Click(Sender: TObject);
var _rc : tFrameChecked;  ii:Integer;  _ff : Boolean;
    el : TComponent;
begin
    if pmmain.PopupComponent.ClassType = TFrame3 then
   begin
    el := self.pmMain.PopupComponent;
    ii := el.Tag;
   _rc := framenames[ii];

   //if _rc.frami.Checked then _rc.frami.Checked := False;
   if _rc.frami.chk1.Checked then _rc.frami.Checked := False;

   _rc.str_wh := '';
   _rc.where := 0;
   _rc.select := 0;
   _rc.frami.Content := requestscope.decode( tAttributs(ii),'',1);//cCaptions[tAttributs(ii)];
   _rc.frami.ChangeLblSize;

   framenames[ii] := _rc;
   end;
end;

procedure TfRedactShbl.N5Click(Sender: TObject);
var i:Integer;
begin
{ sssss := CreateSqlbyString(getQuery);
 try
  //fRedactShbl.Cursor := crSQLWait;
  dm.inquiry(sssss);
  //fRedactShbl.Cursor := crArrow;
  fmTable.GridSetting;
  fRedactShbl.Hide;
  fmTable.Show;
 except
  ShowMessage(rsErrorInQuery);
 end;                                }
end;

procedure TfRedactShbl.N7Click(Sender: TObject);
var i:Integer;
_strVal : string;
begin
 _strVal := getQuery;
 TDBRequestAPIScope.CreateNewRequest(Self._strSHName,Self._GR_ID,_strVal);
 Self.N7.Visible := false;
 self.fBack.go();
end;

procedure TfRedactShbl.setBackInfo(const Value: tNamedProc);
begin
 self.fBack:= Value;
 self.btnBack.Caption:= Self.fBack.title;
end;

procedure TfRedactShbl.setNext(const Value: tNamedProc);
begin
  fNext := Value;
  if Assigned(self.fNext.go) then begin
   self.btnNext.Caption:= self.fNext.title;
   self.btnNext.Visible:= True;
  end else
   self.btnNext.Visible:= False;
end;

procedure TfRedactShbl.setQuery(const Value: string);

procedure _setChecked(pAtt: tAttributs; pValue: Boolean);
var
 _i,_j: integer;
 _pnl: TCategoryPanelSurface;
begin
 for _i:=0 to self.ctgrypnlgrp1.ControlCount-1 do
   if self.ctgrypnlgrp1.Controls[_i] is TCategoryPanel then begin
    _pnl:=  TCategoryPanelSurface(self.ctgrypnlgrp1.Controls[_i].Components[0]);
    for _j:= 0 to _pnl.ControlCount-1 do
     if _pnl.Controls[_j] is TCheckBox then
      if TCheckBox(_pnl.Controls[_j]).Tag = integer(pAtt) then
         TCheckBox(_pnl.Controls[_j]).Checked:= pValue;
   end;
end;

var _ii : Integer;  _att: tAttributs; _rc : tFrameChecked ;
_parse: tSortParser;
idnames : string;
begin
 self.ClearUI;

 _parse:= tSortParser.Create;
 _parse.Parse(Value);

  for _att in _parse.Attr do begin
      _setChecked(_att,True);


      _ii := Ord(_att);
      _rc := framenames[_ii];
      _rc.select := 1;
      _rc.frami.Checked := True;
      framenames[_ii] := _rc;
    end;

    for _att in _parse.Where.Keys do begin
        _setChecked(_att,True);

         _ii := Ord(_att);
         _rc := framenames[_ii];
         _rc.where := 1;
         _rc.str_wh := _parse.Where.Items[_att];
         _rc.frami.Content := requestscope.decode(_att,_parse.Where.Items[_att],2);
         _rc.frami.ChangeLblSize;
         framenames[_ii] := _rc;
       end;
 _parse.Free;
end;


procedure TfRedactShbl.setUISettings(const Value: tUISettings);
begin
 
end;

end.
