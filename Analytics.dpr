program Analytics;

uses
  Vcl.Forms,
  UCreateSql in 'UCreateSql.pas',
  SplashForm in 'UI\SplashForm.pas',
  UI.Views.tfmStat in 'UI\UI.Views.tfmStat.pas' {fmStat},
  UI.Views.tfmSplashForm in 'UI\UI.Views.tfmSplashForm.pas' {fmSplashForm},
  UI.Views.tfmAuth in 'UI\UI.Views.tfmAuth.pas' {fmAuth},
  UI.Views.tfmWebMap in 'UI\UI.Views.tfmWebMap.pas' {fmWebMap},
  UI.Views.tfmTable in 'UI\UI.Views.tfmTable.pas' {fmTable},
  uFR1 in 'uFR1.pas' {Frame3: TFrame},
  ScopeAppSettings in 'ScopeAppSettings.pas',
  uText in 'uText.pas',
  uParser in 'uParser.pas',
  uWorkShablon in 'uWorkShablon.pas' {Form5},
  uRedactorSHb in 'uRedactorSHb.pas' {fRedactShbl},
  UI.iUIManager in 'UI\UI.iUIManager.pas',
  Units.Utils in 'Units\Units.Utils.pas',
  Vcl.Themes,
  Vcl.Styles,
  Units.DMImages in 'Units\Units.DMImages.pas' {fmImages: TDataModule},
  UI.uGeneral in 'UI\UI.uGeneral.pas',
  uPasportCreatePicture in 'uPasportCreatePicture.pas',
  UI.VIEWS.uTABLEShowKArta in 'UI\UI.VIEWS.uTABLEShowKArta.pas' {fTablePicture},
  SPR.fSimple in 'SPRs\SPR.fSimple.pas' {Form3},
  SPR.uInterval in 'SPRs\SPR.uInterval.pas' {Form4},
  SPR.uPerecluch in 'SPRs\SPR.uPerecluch.pas' {Form1},
  SPR.uPeriod in 'SPRs\SPR.uPeriod.pas' {Form2},
  SPR.uSpr in 'SPRs\SPR.uSpr.pas' {fSpr},
  uDBFlatAPI in 'DBWork\uDBFlatAPI.pas',
  uDM in 'DBWork\uDM.pas' {dm: TDataModule},
  uDMFDAC in 'DBWork\uDMFDAC.pas' {dmFD: TDataModule},
  uDMReportPicture in 'uDMReportPicture.pas' {dmRP: TDataModule},
  DockingUtils in 'Docking\DockingUtils.pas',
  uConjoinHost in 'Docking\uConjoinHost.pas' {ConjoinDockHost},
  uDockForm in 'Docking\uDockForm.pas' {DockableForm},
  uTabHost in 'Docking\uTabHost.pas' {TabDockHost},
  Units.Paths in 'Units\Units.Paths.pas',
  uThreadPicture in 'uThreadPicture.pas',
  System.SysUtils,
  Uni_WorkIniF in 'Uni_WorkIniF.pas';

{$R *.res}
var
 _splash: TSplash<TfmSplashForm>;
begin
  Application.Initialize;
  ACFG:=tAnalyticCfg.Create();
  tAnaliticaPaths.Init;
  //�������������� ������� ����
  ScopePath.Init(Application);
  TRY
  FormatSettings.DecimalSeparator:= '.';
   // TStyleManager.TrySetStyle('Metropolis UI Black');
    Application.CreateForm(TfmStat, fmStat);
  Application.CreateForm(TfmAuth, fmAuth);
  Application.CreateForm(TForm5, Form5);
  Application.CreateForm(TfmTable, fmTable);
  Application.CreateForm(TfRedactShbl, fRedactShbl);
  Application.CreateForm(TfmImages, fmImages);
  //Application.CreateForm(TfTablePicture, fTablePicture);
    Application.CreateForm(TForm3, Form3);
    Application.CreateForm(TForm4, Form4);
    Application.CreateForm(TForm1, Form1);
    Application.CreateForm(TForm2, Form2);
    Application.CreateForm(TfSpr, fSpr);
    Application.CreateForm(Tdm, dm);
    Application.CreateForm(TdmFD, dmFD);
    Application.CreateForm(TdmRP, dmRP);
    Application.CreateForm(TConjoinDockHost, ConjoinDockHost);
    Application.CreateForm(TTabDockHost, TabDockHost);
    {$ifdef debug}
    //������� �������
    fmAuth.cbbLogin.Text:= 'sysdba';
    fmAuth.edtPassword.Text:= 'superVlands';
    {$endif}
    fmAuth.ShowModal;
    if not fmAuth.ConnectEstablished then
     EXIT;

    fmStat.fMasterVisible:= false;
    udm.dm.ibqryStatistica.Open;  //05-07-16
   //���������� ��������
   {$ifndef debug}
   //ReportMemoryLeaksOnShutdown:= true;
   if ScopePath.ExistSplashImage then begin
     _splash:=   TSplash<tfmSplashForm>.Create(ScopePath.SplashImage);

     _splash.CbOnClose:= procedure begin
      fmStat.fMasterVisible:= True;
      fmStat.Visible:= true;
     end;
     _splash.Show(True);
    end
    else
    {$endif}
    fmStat.fMasterVisible:= True;
    initnavigate;
    Application.Run;

  FINALLY
   tAnaliticaPaths.WRITEINIFILE; //-- ������ �� �����. �������������� �������� � ��������� ���������� - ������ ���� �� ������ � ������ ���
   ACFG.Free;
  END;

end.
