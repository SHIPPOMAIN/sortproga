object fRedactShbl: TfRedactShbl
  Left = 0
  Top = 0
  Caption = #1056#1077#1076#1072#1082#1090#1086#1088
  ClientHeight = 582
  ClientWidth = 986
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object ctgrypnlgrp1: TCategoryPanelGroup
    Left = 0
    Top = 0
    Width = 396
    Height = 547
    HorzScrollBar.Visible = False
    VertScrollBar.Tracking = True
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWindowText
    HeaderFont.Height = -11
    HeaderFont.Name = 'Tahoma'
    HeaderFont.Style = []
    TabOrder = 0
    object ctgrypnl7: TCategoryPanel
      Top = 873
      Height = 117
      Caption = #1040#1088#1077#1085#1076#1072
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Calibri'
      Font.Style = []
      ParentFont = False
      PopupMenu = pm1
      TabOrder = 6
      object chk11: TCheckBox
        Tag = 27
        Left = 7
        Top = 16
        Width = 242
        Height = 17
        Caption = #1045#1089#1090#1100' '#1072#1088#1077#1085#1076#1072
        TabOrder = 0
        OnClick = chkMain
      end
      object chk12: TCheckBox
        Tag = 29
        Left = 7
        Top = 62
        Width = 242
        Height = 17
        Caption = #1040#1088#1077#1085#1076#1072#1090#1086#1088
        TabOrder = 1
        OnClick = chkMain
      end
      object chk13: TCheckBox
        Tag = 28
        Left = 7
        Top = 39
        Width = 242
        Height = 17
        Caption = #1055#1077#1088#1080#1086#1076' '#1087#1086#1083#1100#1079#1086#1074#1072#1085#1080#1103' ('#1074' '#1075#1086#1076#1072#1093')'
        TabOrder = 2
        OnClick = chkMain
      end
    end
    object ctgrypnl6: TCategoryPanel
      Top = 740
      Height = 133
      Caption = #1055#1088#1072#1074#1072
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Calibri'
      Font.Style = []
      ParentFont = False
      PopupMenu = pm1
      TabOrder = 0
      object chk3: TCheckBox
        Tag = 24
        Left = 7
        Top = 32
        Width = 97
        Height = 17
        Caption = #1057#1086#1073#1089#1090#1074#1077#1085#1085#1080#1082#1080
        TabOrder = 0
        OnClick = chkMain
      end
      object chk8: TCheckBox
        Tag = 23
        Left = 7
        Top = 9
        Width = 97
        Height = 17
        Caption = #1042#1080#1076' '#1087#1088#1072#1074#1072
        TabOrder = 1
        OnClick = chkMain
      end
      object chk9: TCheckBox
        Tag = 25
        Left = 7
        Top = 55
        Width = 242
        Height = 17
        Caption = #1055#1088#1086#1096#1077#1076#1096#1080#1077' '#1050#1072#1076#1072#1089#1090#1088#1086#1074#1099#1081' '#1091#1095#1077#1090
        TabOrder = 2
        OnClick = chkMain
      end
      object chk10: TCheckBox
        Tag = 26
        Left = 7
        Top = 78
        Width = 242
        Height = 17
        Caption = #1055#1088#1086#1096#1077#1076#1096#1080#1077' '#1059#1060#1056#1057
        TabOrder = 3
        OnClick = chkMain
      end
    end
    object ctgrypnl5: TCategoryPanel
      Top = 540
      Caption = #1042#1080#1076#1099' '#1091#1075#1086#1076#1080#1081
      PopupMenu = pm1
      TabOrder = 1
      object chk5: TCheckBox
        Tag = 16
        Left = 7
        Top = 3
        Width = 242
        Height = 17
        Caption = #1055#1072#1096#1085#1103
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Calibri'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        OnClick = chkMain
      end
      object chk7: TCheckBox
        Tag = 17
        Left = 7
        Top = 26
        Width = 193
        Height = 17
        Caption = #1047#1072#1083#1077#1078#1100
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Calibri'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        OnClick = chkMain
      end
      object chk6: TCheckBox
        Tag = 18
        Left = 7
        Top = 49
        Width = 193
        Height = 17
        Caption = #1057#1077#1085#1086#1082#1086#1089#1099
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Calibri'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
        OnClick = chkMain
      end
      object chk4: TCheckBox
        Tag = 19
        Left = 7
        Top = 72
        Width = 193
        Height = 17
        Caption = #1055#1072#1089#1090#1073#1080#1097#1072
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Calibri'
        Font.Style = []
        ParentFont = False
        TabOrder = 3
        OnClick = chkMain
      end
      object chkty3: TCheckBox
        Tag = 20
        Left = 7
        Top = 95
        Width = 193
        Height = 17
        Caption = #1052#1085#1086#1075#1086#1083#1077#1090#1085#1080#1077' '#1085#1072#1089#1072#1078#1076#1077#1085#1080#1103
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Calibri'
        Font.Style = []
        ParentFont = False
        TabOrder = 4
        OnClick = chkMain
      end
      object chkty2: TCheckBox
        Tag = 21
        Left = 7
        Top = 118
        Width = 193
        Height = 17
        Caption = #1053#1077#1091#1075#1086#1076#1100#1103
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Calibri'
        Font.Style = []
        ParentFont = False
        TabOrder = 5
        OnClick = chkMain
      end
      object chkTY1: TCheckBox
        Tag = 22
        Left = 7
        Top = 141
        Width = 267
        Height = 17
        Caption = #1055#1086#1083#1077#1079#1072#1097#1080#1090#1085#1099#1077' '#1083#1077#1089#1085#1099#1077' '#1085#1072#1089#1072#1078#1076#1077#1085#1080#1103
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Calibri'
        Font.Style = []
        ParentFont = False
        TabOrder = 6
        OnClick = chkMain
      end
    end
    object ctgrypnl4: TCategoryPanel
      Top = 340
      Caption = #1043#1088#1091#1087#1087#1099' '#1079#1077#1084#1077#1083#1100
      PopupMenu = pm1
      TabOrder = 2
      object chkGr7: TCheckBox
        Tag = 15
        Left = 7
        Top = 144
        Width = 169
        Height = 17
        Caption = #1055#1088#1086#1095#1080#1077' '#1079#1077#1084#1083#1080
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Calibri'
        Font.Style = []
        ParentFont = False
        TabOrder = 6
        OnClick = chkMain
      end
      object chkGR6: TCheckBox
        Tag = 14
        Left = 7
        Top = 121
        Width = 314
        Height = 17
        Caption = #1047#1077#1084#1083#1080', '#1087#1088#1080#1075#1086#1076#1085#1099#1077' '#1087#1086#1076' '#1086#1083#1077#1085#1100#1080' '#1087#1072#1089#1090#1073#1080#1097#1072
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Calibri'
        Font.Style = []
        ParentFont = False
        PopupMenu = pm1
        TabOrder = 5
        OnClick = chkMain
      end
      object chkGR5: TCheckBox
        Tag = 13
        Left = 7
        Top = 98
        Width = 442
        Height = 17
        Caption = #1047#1077#1084#1083#1080' '#1087#1086#1076' '#1083#1077#1089#1072#1084#1080', '#1085#1077' '#1087#1077#1088#1077#1074#1077#1076#1077#1085#1085#1099#1077'  '#1074' '#1089#1086#1089#1090#1072#1074' '#1079#1077#1084#1077#1083#1100' '#1083#1077#1089#1092#1086#1085#1076#1072
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Calibri'
        Font.Style = []
        ParentFont = False
        TabOrder = 3
        OnClick = chkMain
      end
      object chkGR4: TCheckBox
        Tag = 12
        Left = 7
        Top = 75
        Width = 442
        Height = 17
        Caption = #1047#1077#1084#1083#1080' '#1087#1086#1076' '#1076#1088#1077#1074#1077#1089#1085#1086'-'#1082#1091#1089#1090#1072#1088#1085#1080#1082#1086#1074#1086#1081' '#1088#1072#1089#1090#1080#1090#1077#1083#1100'-'#1102', '#1073#1086#1083#1086#1090#1072#1084#1080' '#1080' '#1090#1076
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Calibri'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
        OnClick = chkMain
      end
      object chkGR3: TCheckBox
        Tag = 11
        Left = 7
        Top = 52
        Width = 273
        Height = 17
        Caption = #1047#1077#1084#1083#1080' '#1087#1086#1076' '#1079#1072#1084#1082#1085#1091#1090#1099#1084#1080' '#1074#1086#1076#1086#1077#1084#1072#1084#1080
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Calibri'
        Font.Style = []
        ParentFont = False
        TabOrder = 4
        OnClick = chkMain
      end
      object chkGR1: TCheckBox
        Tag = 9
        Left = 7
        Top = 6
        Width = 228
        Height = 17
        Caption = #1057#1077#1083#1100#1089#1082#1086#1093#1086#1079#1103#1081#1089#1090#1074#1077#1085#1085#1099#1077' '#1091#1075#1086#1076#1100#1103
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Calibri'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        OnClick = chkMain
      end
      object chk2: TCheckBox
        Tag = 10
        Left = 7
        Top = 29
        Width = 354
        Height = 17
        Caption = #1047#1077#1084#1083#1080', '#1079#1072#1085#1103#1090#1099#1077' '#1074#1085#1091#1090#1088#1080#1093#1086#1079#1103#1081#1089#1090#1074#1077#1085#1085#1099#1084#1080' '#1076#1086#1088#1086#1075#1072#1084#1080
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Calibri'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        OnClick = chkMain
      end
    end
    object ctgrypnl3: TCategoryPanel
      Top = 248
      Height = 92
      Caption = #1061#1072#1088#1072#1082#1090#1077#1088#1080#1089#1090#1080#1082#1080' '#1047#1059
      PopupMenu = pm1
      TabOrder = 3
      object chkCadPrice: TCheckBox
        Tag = 8
        Left = 7
        Top = 32
        Width = 185
        Height = 17
        Caption = #1050#1072#1076#1072#1089#1090#1088#1086#1074#1072#1103' '#1089#1090#1086#1080#1084#1086#1089#1090#1100' '#1047#1059
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Calibri'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        OnClick = chkMain
      end
      object chkCategory: TCheckBox
        Tag = 7
        Left = 7
        Top = 9
        Width = 185
        Height = 17
        Caption = #1050#1072#1090#1077#1075#1086#1088#1080#1080' '#1047#1059
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Calibri'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        OnClick = chkMain
      end
    end
    object ctgrypnl2: TCategoryPanel
      Top = 161
      Height = 87
      Caption = #1048#1089#1087#1086#1083#1100#1079#1086#1074#1072#1085#1080#1077' '#1047#1059
      PopupMenu = pm1
      TabOrder = 4
      object chk1: TCheckBox
        Tag = 6
        Left = 7
        Top = 26
        Width = 209
        Height = 17
        Caption = #1060#1072#1082#1090#1080#1095#1077#1089#1082#1086#1077' '#1080#1089#1087#1086#1083#1100#1079#1086#1074#1072#1085#1080#1077
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Calibri'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        OnClick = chkMain
      end
      object chkIspZU: TCheckBox
        Tag = 5
        Left = 7
        Top = 3
        Width = 185
        Height = 17
        Caption = #1042#1080#1076' '#1080#1089#1087#1086#1083#1100#1079#1086#1074#1072#1085#1080#1103' '#1047#1059
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Calibri'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        OnClick = chkMain
      end
    end
    object ctgrypnl1: TCategoryPanel
      Top = 0
      Height = 161
      Caption = #1054#1073#1097#1080#1077
      PopupMenu = pm1
      TabOrder = 5
      object chkSPos: TCheckBox
        Tag = 3
        Left = 7
        Top = 80
        Width = 185
        Height = 17
        Caption = #1057#1077#1083#1100#1089#1082#1080#1077' '#1087#1086#1089#1077#1083#1077#1085#1080#1103
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Calibri'
        Font.Style = []
        ParentFont = False
        TabOrder = 3
        OnClick = chkMain
      end
      object chkKolhoz: TCheckBox
        Tag = 4
        Left = 7
        Top = 103
        Width = 185
        Height = 17
        Caption = #1050#1086#1083#1093#1086#1079
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Calibri'
        Font.Style = []
        ParentFont = False
        TabOrder = 4
        OnClick = chkMain
      end
      object chkSquare: TCheckBox
        Tag = 1
        Left = 7
        Top = 34
        Width = 185
        Height = 17
        Caption = #1055#1083#1086#1097#1072#1076#1100
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Calibri'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        OnClick = chkMain
      end
      object chkCadNUm: TCheckBox
        Left = 7
        Top = 11
        Width = 185
        Height = 17
        Caption = #1050#1072#1076#1072#1089#1090#1088#1086#1074#1099#1081' '#1085#1086#1084#1077#1088
        Color = clRed
        DoubleBuffered = True
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Calibri'
        Font.Style = []
        ParentColor = False
        ParentDoubleBuffered = False
        ParentFont = False
        TabOrder = 0
        OnClick = chkMain
      end
      object chkRayon: TCheckBox
        Tag = 2
        Left = 7
        Top = 57
        Width = 193
        Height = 17
        Caption = #1052#1091#1085#1080#1094#1080#1087#1072#1083#1100#1085#1099#1081' '#1088#1072#1081#1086#1085
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Calibri'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
        OnClick = chkMain
      end
    end
  end
  object pnl3: TPanel
    Left = 396
    Top = 0
    Width = 590
    Height = 547
    Align = alClient
    ParentBackground = False
    TabOrder = 1
    object Label1: TLabel
      Left = 1
      Top = 1
      Width = 588
      Height = 13
      Align = alTop
      Alignment = taCenter
      Caption = #1054#1090#1086#1073#1088#1072#1090#1100' '#1079#1077#1084#1077#1083#1100#1085#1099#1077' '#1091#1095#1072#1089#1090#1082#1080' ...'
      WordWrap = True
      ExplicitWidth = 167
    end
    object scrlbx1: TScrollBox
      Left = 1
      Top = 14
      Width = 588
      Height = 532
      HorzScrollBar.Visible = False
      Align = alClient
      PopupMenu = pmMain
      TabOrder = 0
    end
  end
  object pnlTools: TPanel
    Left = 0
    Top = 547
    Width = 986
    Height = 35
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    object btnBack: TButton
      Left = 0
      Top = 0
      Width = 155
      Height = 35
      Align = alLeft
      Caption = 'btnBack'
      ImageIndex = 0
      Images = fmImages.img32
      TabOrder = 0
      OnClick = btnBackClick
    end
    object btnNext: TButton
      Left = 831
      Top = 0
      Width = 155
      Height = 35
      Align = alRight
      Caption = 'btnNext'
      ImageAlignment = iaRight
      ImageIndex = 1
      Images = fmImages.img32
      TabOrder = 1
      OnClick = btnNextClick
    end
  end
  object pmMain: TPopupMenu
    Left = 1032
    Top = 593
    object N5: TMenuItem
      Caption = #1047#1072#1087#1091#1089#1090#1080#1090#1100' '#1096#1072#1073#1083#1086#1085
      Visible = False
      OnClick = N5Click
    end
    object N7: TMenuItem
      Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100' '#1096#1072#1073#1083#1086#1085' '#1074' '#1073#1080#1073#1083#1080#1086#1090#1077#1082#1091
      Visible = False
      OnClick = N7Click
    end
    object N6: TMenuItem
      Caption = '-'
    end
    object N3: TMenuItem
      Caption = #1054#1095#1080#1089#1090#1080#1090#1100' '#1091#1089#1083#1086#1074#1080#1077
      OnClick = N3Click
    end
    object N4: TMenuItem
      Caption = '-'
    end
    object N1: TMenuItem
      Caption = #1042#1099#1076#1077#1083#1080#1090#1100' '#1074#1089#1077
      OnClick = N1Click
    end
    object N2: TMenuItem
      Caption = #1057#1085#1103#1090#1100' '#1074#1099#1076#1077#1083#1077#1085#1080#1077
      OnClick = N2Click
    end
  end
  object pm1: TPopupMenu
    Left = 1032
    Top = 545
    object MenuItem1: TMenuItem
      Caption = #1042#1099#1076#1077#1083#1080#1090#1100' '#1074#1089#1077
      Enabled = False
      Visible = False
    end
    object MenuItem2: TMenuItem
      Caption = #1057#1085#1103#1090#1100' '#1074#1099#1076#1077#1083#1077#1085#1080#1077
      OnClick = MenuItem2Click
    end
  end
end
