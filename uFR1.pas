unit uFR1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls,
  Vcl.Imaging.pngimage, Vcl.Buttons;

type
  TFrame3 = class(TFrame)
    pnl1: TPanel;
    pnl2: TPanel;
    btn1: TButton;
    pnl3: TPanel;
    lbl1: TLabel;
    chk1: TCheckBox;
    spb1: TSpeedButton;
  private
    function getChecked: Boolean;
    function getContent: string;
    procedure setChecked(const Value: boolean);
    procedure setContent(const Value: string);
    { Private declarations }
  public
    { Public declarations }
    property Content: string read getContent write setContent;
    property Checked: boolean read getChecked write setChecked;
    procedure ChangeLblSize;
  end;

implementation

{$R *.dfm}

{ TFrame3 }

procedure TFrame3.ChangeLblSize;
var  i :Integer;
begin
  //i := Round(Self.lbl1.Width/self.pnl3.Width);
  i := Round(Length(Self.lbl1.Caption)/50);  //55
  if i > 1 then
  begin
   Self.lbl1.WordWrap := true;
   Self.Height := 25*i;
  end
  else
    begin
     Self.lbl1.WordWrap := false;
     Self.Height := 25;//34
    end;
end;

function TFrame3.getChecked: boolean;
begin
 self.chk1.Checked;
end;

function TFrame3.getContent: string;
begin
//  self.lbl1.Caption:= Value;
end;

procedure TFrame3.setChecked(const Value: boolean);
begin
 self.chk1.Checked:=Value;
end;

procedure TFrame3.setContent(const Value: string);
begin
  self.lbl1.Caption:= Value;
end;

end.
