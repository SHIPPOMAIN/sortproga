unit UMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.Generics.Collections  ,System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ComCtrls, Vcl.ExtCtrls,
  uFR1, uFR2, Vcl.Grids, Vcl.DBGrids, Vcl.Menus, Vcl.Buttons,PngImage,uParser;

type
  tOnSelParcel = procedure(const pCadNum: string) of object;

  tFrameChecked =  record
    frami : TFrame3;
    select : Integer;
    where  : Integer;
    str_wh : string;
    str_sel: string;
    str_prefix : string;
  end;

  TFMain = class(TForm)
    pnl1: TPanel;
    pnl2: TPanel;
    pgc1: TPageControl;
    tsNewSH: TTabSheet;
    tsRedactSh: TTabSheet;
    tsresult: TTabSheet;
    ctgrypnlgrp1: TCategoryPanelGroup;
    ctgrypnl1: TCategoryPanel;
    chkSPos: TCheckBox;
    chkKolhoz: TCheckBox;
    chkSquare: TCheckBox;
    chkCadNUm: TCheckBox;
    chkRayon: TCheckBox;
    ctgrypnl2: TCategoryPanel;
    chk1: TCheckBox;
    chkIspZU: TCheckBox;
    ctgrypnl3: TCategoryPanel;
    chkCadPrice: TCheckBox;
    chkCategory: TCheckBox;
    ctgrypnl4: TCategoryPanel;
    chkGr7: TCheckBox;
    chkGR6: TCheckBox;
    chkGR5: TCheckBox;
    chkGR4: TCheckBox;
    chkGR3: TCheckBox;
    chkGR1: TCheckBox;
    chk2: TCheckBox;
    ctgrypnl5: TCategoryPanel;
    chk5: TCheckBox;
    chk7: TCheckBox;
    chk6: TCheckBox;
    chk4: TCheckBox;
    chkty3: TCheckBox;
    chkty2: TCheckBox;
    chkTY1: TCheckBox;
    memo1: TMemo;
    btn8: TButton;
    Button1: TButton;
    btn7: TButton;
    btn9: TButton;
    mmo1: TMemo;
    btn3: TButton;
    btn4: TButton;
    btn5: TButton;
    btn6: TButton;
    pnl3: TPanel;
    ctgrypnl6: TCategoryPanel;
    chk3: TCheckBox;
    chk8: TCheckBox;
    chk9: TCheckBox;
    chk10: TCheckBox;
    ctgrypnl7: TCategoryPanel;
    chk11: TCheckBox;
    chk12: TCheckBox;
    chk13: TCheckBox;
    scrlbx1: TScrollBox;
    dbgrdResult: TDBGrid;
    pnl4: TPanel;
    pmMain: TPopupMenu;
    N2: TMenuItem;
    lbl2: TLabel;
    dlgSave1: TSaveDialog;
    btn1: TSpeedButton;
    btn11: TSpeedButton;
    btn12: TSpeedButton;
    btn10: TSpeedButton;
    btn2: TSpeedButton;
    lbl1: TLabel;
    btn13: TButton;
    pm1: TPopupMenu;
    MenuItem1: TMenuItem;
    MenuItem2: TMenuItem;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure chkMain(Sender: TObject);
    procedure FrameButtonClick(Sender: Tobject);
    procedure FrameCHKCkick(Sender : TOBject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure tsNewSHShow(Sender: TObject);
    procedure dbgrdResultDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure dbgrdResultCellClick(Column: TColumn);
    procedure btn10Click(Sender: TObject);
    procedure btn1Click(Sender: TObject);
    procedure btn11Click(Sender: TObject);
    procedure btn12Click(Sender: TObject);
    procedure btn2Click(Sender: TObject);
    procedure btn13Click(Sender: TObject);
    procedure SetCurrentQuery(const Value: string);
    procedure MenuItem1Click(Sender: TObject);
    procedure MenuItem2Click(Sender: TObject);
    procedure dbgrdResultTitleClick(Column: TColumn);
  //  procedure MemoCreate(Sender:TObject; mmname : string ; var _rc : tFrameChecked);
  private
    fOnSelParcel: tOnSelParcel;
    { Private declarations }
    procedure CMVisibleChanged(var Message: TMessage); message CM_VISIBLECHANGED;
  public
    { Public declarations }
    fMasterVisible: boolean;
    property OnSelParcel: tOnSelParcel read fOnSelParcel write fOnSelParcel;
  end;



var
  FMain: TFMain;
  i : Integer;
  _parse: tSortParser;

  select, where  : string;
  sssss : string;
  str_Raionid ,str_SPid  : string;

  ssssList : TStringList;
  framenames : TList<tFrameChecked>;
  labelnames, GridFieldNames, CHKNames : TStringList;

  mm1, mm2, mm3, mm4, mm5, mm6, mm7, mm8:TMemo;
implementation
uses UCreateSql, uDM, uConnect, uSpr, fSimple, uInterval, uPeriod, uPerecluch,
  ScopeAppSettings, uText, fmWebMap;
{$R *.dfm}

procedure TFMain.btn10Click(Sender: TObject);
var i:Integer;
begin
Select := '';
where := '';
 for I := 0 to 29 do
 begin
  if framenames[i].select = 1 then select := select+framenames[i].str_sel;
  if framenames[i].where = 1 then where := where+framenames[i].str_prefix+framenames[i].str_wh+';';
 end;
 if Trim(Select)='' then begin ShowMessage('�� �� ������� ���� ������� ������ ������!'); Abort; end;
 if Trim(where)='' then begin ShowMessage('�� �� ������� ������� ��� �������!'); Abort; end;
 select := Copy(Select,1,Length(select)-1);
 Mmo1.Lines.Add(Select+ ' ��� '+ where);
 sssss := CreateSqlbyString(Select+ ' ��� '+ where);
 Memo1.Lines.Add(sssss);


 if  dm.ibqryResult.Active then dm.ibqryResult.Close;
 dm.ibtrnsctn1.CommitRetaining;
 dm.ibqryResult.SQL.Clear;
 dm.ibqryResult.SQL.Add(sssss);
 try
  TButton(Sender).Cursor := crSQLWait;
  dm.ibqryResult.Open;
  TButton(Sender).Cursor := crArrow;

  for I := 0 to dm.ibqryResult.FieldCount-1 do
  begin
     if dm.ibqryResult.Fields[i].FieldName='CADNUM'  then begin dm.ibqryResult.Fields[i].DisplayLabel :=GridFieldNames[0]; dbgrdResult.Columns[i].Width := 150;  end
     else
     if dm.ibqryResult.Fields[i].FieldName='LAREA'  then begin dm.ibqryResult.Fields[i].DisplayLabel :=GridFieldNames[1];  dbgrdResult.Columns[i].Width := 80; end
     else
     if dm.ibqryResult.Fields[i].FieldName='RAION'  then begin dm.ibqryResult.Fields[i].DisplayLabel :=GridFieldNames[2];  dbgrdResult.Columns[i].Width := 250; end
     else
     if dm.ibqryResult.Fields[i].FieldName='SETTLE'  then begin dm.ibqryResult.Fields[i].DisplayLabel :=GridFieldNames[3]; dbgrdResult.Columns[i].Width := 250; end
     else
     if dm.ibqryResult.Fields[i].FieldName='KOLHOZ'  then begin dm.ibqryResult.Fields[i].DisplayLabel :=GridFieldNames[4]; dbgrdResult.Columns[i].Width := 250; end
     else
     if dm.ibqryResult.Fields[i].FieldName='LUSENAME'  then begin dm.ibqryResult.Fields[i].DisplayLabel :=GridFieldNames[5]; dbgrdResult.Columns[i].Width := 250; end
     else
     if dm.ibqryResult.Fields[i].FieldName='LFACTUAL_USE' then begin dm.ibqryResult.Fields[i].DisplayLabel :=GridFieldNames[6]; dbgrdResult.Columns[i].Width := 250; end
     else
     if dm.ibqryResult.Fields[i].FieldName='CATEGORY'  then begin dm.ibqryResult.Fields[i].DisplayLabel :=GridFieldNames[7]; dbgrdResult.Columns[i].Width := 200; end
     else
     if dm.ibqryResult.Fields[i].FieldName='CADPRICE'  then begin dm.ibqryResult.Fields[i].DisplayLabel :=GridFieldNames[8]; dbgrdResult.Columns[i].Width := 80; end
     else
     if dm.ibqryResult.Fields[i].FieldName='AGROUP'  then begin dm.ibqryResult.Fields[i].DisplayLabel :=GridFieldNames[9]; dbgrdResult.Columns[i].Width := 80; end
     else
     if dm.ibqryResult.Fields[i].FieldName='AGROUP2'  then begin dm.ibqryResult.Fields[i].DisplayLabel :=GridFieldNames[10];dbgrdResult.Columns[i].Width := 80; end
     else
     if dm.ibqryResult.Fields[i].FieldName='AGROUP3'  then begin dm.ibqryResult.Fields[i].DisplayLabel :=GridFieldNames[11]; dbgrdResult.Columns[i].Width := 80;end
     else
     if dm.ibqryResult.Fields[i].FieldName='AGROUP4'  then begin dm.ibqryResult.Fields[i].DisplayLabel :=GridFieldNames[12];dbgrdResult.Columns[i].Width := 80; end
     else
     if dm.ibqryResult.Fields[i].FieldName='AGROUP5'  then begin dm.ibqryResult.Fields[i].DisplayLabel :=GridFieldNames[13];dbgrdResult.Columns[i].Width := 80; end
     else
     if dm.ibqryResult.Fields[i].FieldName='AGROUP6'  then begin dm.ibqryResult.Fields[i].DisplayLabel :=GridFieldNames[14];dbgrdResult.Columns[i].Width := 80; end
     else
     if dm.ibqryResult.Fields[i].FieldName='AGROUP7'  then begin dm.ibqryResult.Fields[i].DisplayLabel :=GridFieldNames[15];dbgrdResult.Columns[i].Width := 80; end
     else
     if dm.ibqryResult.Fields[i].FieldName='ATYPE'  then begin dm.ibqryResult.Fields[i].DisplayLabel :=GridFieldNames[16];dbgrdResult.Columns[i].Width := 80; end
     else
     if dm.ibqryResult.Fields[i].FieldName='ATYPE2'  then begin dm.ibqryResult.Fields[i].DisplayLabel :=GridFieldNames[17];dbgrdResult.Columns[i].Width := 80; end
     else
     if dm.ibqryResult.Fields[i].FieldName='ATYPE3'  then begin dm.ibqryResult.Fields[i].DisplayLabel :=GridFieldNames[18];dbgrdResult.Columns[i].Width := 80; end
     else
     if dm.ibqryResult.Fields[i].FieldName='ATYPE4'  then begin dm.ibqryResult.Fields[i].DisplayLabel :=GridFieldNames[19];dbgrdResult.Columns[i].Width := 80; end
     else
     if dm.ibqryResult.Fields[i].FieldName='ATYPE5'  then begin dm.ibqryResult.Fields[i].DisplayLabel :=GridFieldNames[20];dbgrdResult.Columns[i].Width := 80; end
     else
     if dm.ibqryResult.Fields[i].FieldName='ATYPE6'  then begin dm.ibqryResult.Fields[i].DisplayLabel :=GridFieldNames[21];dbgrdResult.Columns[i].Width := 80; end
     else
     if dm.ibqryResult.Fields[i].FieldName='ATYPE7'  then begin dm.ibqryResult.Fields[i].DisplayLabel :=GridFieldNames[22];dbgrdResult.Columns[i].Width := 80; end
     else
     if dm.ibqryResult.Fields[i].FieldName='RIGHTNAME'  then begin dm.ibqryResult.Fields[i].DisplayLabel :=GridFieldNames[23];dbgrdResult.Columns[i].Width := 250; end
     else
     if dm.ibqryResult.Fields[i].FieldName='SOBNAME'  then begin dm.ibqryResult.Fields[i].DisplayLabel :=GridFieldNames[24];dbgrdResult.Columns[i].Width := 200; end
     else
     if dm.ibqryResult.Fields[i].FieldName='KUCH'  then begin dm.ibqryResult.Fields[i].DisplayLabel :=GridFieldNames[25];dbgrdResult.Columns[i].Width := 30; end
     else
     if dm.ibqryResult.Fields[i].FieldName='UFRS'  then begin dm.ibqryResult.Fields[i].DisplayLabel :=GridFieldNames[26];dbgrdResult.Columns[i].Width := 30; end
     else
     if dm.ibqryResult.Fields[i].FieldName='ARENDA'  then begin dm.ibqryResult.Fields[i].DisplayLabel :=GridFieldNames[27];dbgrdResult.Columns[i].Width := 30; end
     else
     if dm.ibqryResult.Fields[i].FieldName='PERIOD'  then begin dm.ibqryResult.Fields[i].DisplayLabel :=GridFieldNames[28];dbgrdResult.Columns[i].Width := 50; end
     else
     if dm.ibqryResult.Fields[i].FieldName='ASOBNAME'  then begin dm.ibqryResult.Fields[i].DisplayLabel :=GridFieldNames[29];dbgrdResult.Columns[i].Width := 200; end;
  end;
  pgc1.ActivePage:= tsresult;
 
 except
  ShowMessage('������! ������ �� ���������');
 end;

end;



procedure TFMain.btn11Click(Sender: TObject);
begin
 if dbgrdResult.DataSource.DataSet.Active then
 pgc1.ActivePage := tsresult
 else
 ShowMessage('�� ��� �� ��������� ������!');
end;

procedure TFMain.btn12Click(Sender: TObject);
var ff : TextFile;  _i:Integer;
begin
 if dm.ibqryResult.Active then
  begin
    if dlgSave1.Execute then
    begin
    try
    AssignFile(ff, dlgSave1.FileName{+FormatDatetime('dd.mm.yyyy hh.mm.ss', Now())+'.csv'});
    Rewrite(ff);
    dm.ibqryResult.DisableControls;
    dm.ibqryResult.First;
    for _i := 0 to  dm.ibqryResult.FieldCount-1 do
       Write(ff,dm.ibqryResult.Fields[_i].DisplayLabel+';');
    Writeln(ff);
    TSpeedButton(Sender).Cursor := crSQLWait;
    while not dm.ibqryResult.eof do
    begin
      for _i := 0 to  dm.ibqryResult.FieldCount-1 do
       Write(ff,dm.ibqryResult.Fields[_i].AsString+';');
      Writeln(ff);
      dm.ibqryResult.Next;
    end;
    TSpeedButton(Sender).Cursor := crArrow;
   dm.ibqryResult.First;
   dm.ibqryResult.EnableControls;
   CloseFile(FF);
   ShowMessage(rsEndLoad);
   except
   ShowMessage(rsErrorLoad);
   end;
   end;
  end;

end;

procedure TFMain.btn13Click(Sender: TObject);
var _strVal : string;
begin
 _strVal := InputBox('������� ������', '������=', '���,����,��,��1,��1,��2,��3,��4,��5,��6,��7 ��� ���=(361000);����=36:10:000000;��=0,1000;��1=true,1;��1=true,0;��2=true,0;��3=true,0;��4=true,0;��5=true,0;��6=true,0;��7=true,0;') ;
 Self.SetCurrentQuery(_strVal);
// test;

end;

procedure TFMain.btn1Click(Sender: TObject);
begin
 pgc1.ActivePage := tsNewSH;
end;



procedure TFMain.btn2Click(Sender: TObject);
begin
 if not Assigned(fWebMap) then
   Application.CreateForm(TfmWebMap, fWebMap);
 if fWebMap.Visible then
  fWebMap.Close
 else
  fWebMap.Show
end;



procedure TFMain.chkMain(Sender: TObject);
var _rc : tFrameChecked;  ii:Integer;  _ff : Boolean;
begin
   if lbl1.Visible then lbl1.Visible := False;
   ii := (Sender as TCheckBox).tag-1;
   _rc := framenames[ii];

  if (Sender as TCheckBox).Checked then
  begin
   _rc.frami.Visible := True;

   case ii  of
    2: begin str_Raionid := ''; end; // �������� ���������� ����� �������
    3: begin str_SPid := ''; end; // �������� ���������� ����� �������� ���������
    9: begin _rc.str_wh := 'true,0'; _rc.where := 1; end;
    10: begin _rc.str_wh := 'true,0'; _rc.where := 1; end;
    11: begin _rc.str_wh := 'true,0'; _rc.where := 1; end;
    12: begin _rc.str_wh := 'true,0'; _rc.where := 1; end;
    13: begin _rc.str_wh := 'true,0'; _rc.where := 1; end;
    14: begin _rc.str_wh := 'true,0'; _rc.where := 1; end;
    15: begin _rc.str_wh := 'true,0'; _rc.where := 1; end;

    16: begin _rc.str_wh := 'true,0'; _rc.where := 1; end;
    17: begin _rc.str_wh := 'true,0'; _rc.where := 1; end;
    18: begin _rc.str_wh := 'true,0'; _rc.where := 1; end;
    19: begin _rc.str_wh := 'true,0'; _rc.where := 1; end;
    20: begin _rc.str_wh := 'true,0'; _rc.where := 1; end;
    21: begin _rc.str_wh := 'true,0'; _rc.where := 1; end;
    22: begin _rc.str_wh := 'true,0'; _rc.where := 1; end;

   // 25: begin _rc.str_wh := 'true'; _rc.where := 1; end;
   // 26: begin _rc.str_wh := 'true'; _rc.where := 1; end;
    27: begin _rc.str_wh := 'true'; _rc.where := 1; end;
   end;
   framenames[ii] := _rc;
  end
  else
  begin
     if not _rc.frami.lbl1.Visible then
      begin
        case  ii+1 of
          3  :begin mm1.Lines.Clear; mm1.Visible := False; FreeAndNil(mm1); end;
          4  :begin mm2.Lines.Clear; mm2.Visible := False; FreeAndNil(mm2);end;
          5  :begin mm3.Lines.Clear; mm3.Visible := False; FreeAndNil(mm3);end;
          6  :begin mm4.Lines.Clear; mm4.Visible := False; FreeAndNil(mm4);end;
          8  :begin mm5.Lines.Clear; mm5.Visible := False; FreeAndNil(mm5);end;
          24 :begin mm6.Lines.Clear; mm6.Visible := False; FreeAndNil(mm6);end;
          25 :begin mm7.Lines.Clear; mm7.Visible := False; FreeAndNil(mm7);end;
          30 :begin mm8.Lines.Clear; mm8.Visible := False; FreeAndNil(mm8);end;
        end;  
       // if _rc.frami.mm1.Visible then begin _rc.frami.mm1.Visible := False; _rc.frami.mm1.Lines.Clear;  end;       
        _rc.frami.lbl1.Visible := true;
      end;
   if _rc.frami.chk1.Checked then _rc.frami.chk1.Checked := False;
   _rc.frami.Visible := false;
   _rc.str_wh := '';
   _rc.where := 0;
   _rc.select := 0;
   _rc.frami.chk1.Checked :=false;
   _rc.frami.Height := 43;
   _rc.frami.lbl1.Caption := labelnames[ii];

   framenames[ii] := _rc;
//   _FF := False;
//   ii := 0;
//   while (not _ff) and (ii<30) do
//   begin
//    _ff := (framenames[ii].frami.Visible = False);
//    ii := ii+1;
//   end;
//
//   if _ff then lbl1.Visible := True;
   

  end;
end;


procedure TFMain.CMVisibleChanged(var Message: TMessage);
begin
 if not self.fMasterVisible then self.Visible:= false
 else
  inherited;
end;

procedure TFMain.dbgrdResultCellClick(Column: TColumn);
begin
  if Assigned(self.fOnSelParcel)  then
   if dbgrdResult.DataSource.DataSet.Fields[0].FieldName = 'CADNUM' then self.fOnSelParcel(dbgrdResult.DataSource.DataSet.Fields[0].AsString);
end;

procedure TFMain.dbgrdResultDrawColumnCell(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
// ������ �������� ������
	IF TDBGrid(Sender).DataSource.DataSet.RecNo mod 2 = 1
	Then TDBGrid(Sender).Canvas.Brush.Color:=clGradientInactiveCaption; //RGB(188,184,190);

	// ��������������� ��������� ������� ������� �������
	IF  gdSelected   IN State
	Then Begin
		TDBGrid(Sender).Canvas.Brush.Color:= clHighLight;
		TDBGrid(Sender).Canvas.Font.Color := clHighLightText;
	End;
	// ������ GRID �������������� ������
	TDBGrid(Sender).DefaultDrawColumnCell(Rect,DataCol,Column,State);
end;

procedure TFMain.dbgrdResultTitleClick(Column: TColumn);
var _s1, _s2, _s3,_s4, _s5,_s6,_ss:string; ss,ss2, ss3 : TArray<string>;
dil1, dil2 : string;
begin
 with dm do
 begin
 dil1 := 'union';
 dil2 := 'order';
  ibqryResult.Close;
  ss:= ibqryResult.SQL.Text.Split(dil1,ExcludeEmpty);
  _s1 := Trim(ss[0]);
  _s2 := Trim(ss[1]);

  ss2 := _s1.Split(dil2, ExcludeEmpty);
  _s3 := Trim(ss2[0]);
  //_s4 := Trim(ss2[1]);

  if _s2 <> '' then
  begin
   ss3 := _s2.Split(dil2, ExcludeEmpty);
  _s5 := Trim(ss3[0]);
  //_s6 := Trim(ss3[1]);
  end;

  _ss:= _s3+' order '+ Column.FieldName;
  if _s2 <> '' then  _ss:=_ss+ _s5+' order '+ Column.FieldName;

  ibqryResult.SQL.Clear;
  ibqryResult.SQL.Add(_ss);
  ibqryResult.Open;
 end;
end;

procedure TFMain.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  fConnect.Close;
end;

function getselname(i:integer):string;
begin
  case i of
     1: Result:= '����,';
     2: Result:= '��,';
     3: Result:= '���,';
     4: Result:= '��,';
     5: Result:= '���,';
     8: Result:= '���,';
     9: Result:= '�����,';
     6: Result:= '���,';
     7: Result:= '����,';
    10: Result:= '��1,';
    11: Result:= '��2,';
    12: Result:= '��3,';
    13: Result:= '��4,';
    14: Result:= '��5,';
    15: Result:= '��6,';
    16: Result:= '��7,';
    17: Result:= '��1,';
    18: Result:= '��2,';
    19: Result:= '��3,';
    20: Result:= '��4,';
    21: Result:= '��5,';
    22: Result:= '��6,';
    23: Result:= '��7,';
    24: Result:= '�����,';
    25: Result:= '����,';
    26: Result:= '���,';
    27: Result:= '����,';
    28: Result:= '������,';
    29: Result:= '���,';
    30: Result:= '�����,';
 end;
end;

function getprefname(i:integer):string;
begin
  case i of
     1: Result:= '����=';
     2: Result:= '��=';
     3: Result:= '���=';
     4: Result:= '��=';
     5: Result:= '���=';
     8: Result:= '���=';
     9: Result:= '�����=';
     6: Result:= '���=';
     7: Result:= '����=';
    10: Result:= '��1=';
    11: Result:= '��2=';
    12: Result:= '��3=';
    13: Result:= '��4=';
    14: Result:= '��5=';
    15: Result:= '��6=';
    16: Result:= '��7=';
    17: Result:= '��1=';
    18: Result:= '��2=';
    19: Result:= '��3=';
    20: Result:= '��4=';
    21: Result:= '��5=';
    22: Result:= '��6=';
    23: Result:= '��7=';
    24: Result:= '�����=';
    25: Result:= '����=';
    26: Result:= '���=';
    27: Result:= '����=';
    28: Result:= '������=';
    29: Result:= '���=';
    30: Result:= '�����=';
 end;
end;

function getFieldname(i:integer):string;
begin
  case i of
     1: Result:= '����������� �����';
     2: Result:= '�������,��';
     3: Result:= '������������� �����';
     4: Result:= '�������� ���������';
     5: Result:= '������';
     8: Result:= '��������� ������';
     9: Result:= '����������� ���������';
     6: Result:= '��� ������������� ��';
     7: Result:= '����������� �������������';
    10: Result:= '�������������������� ������, ��';
    11: Result:= '�����, ������� ���������.��������,��';
    13: Result:= '����� ��� ��.-����.����-� � ��,��';
    14: Result:= '����� ��� ������, �� ������������, ��';
    12: Result:= '����� ��� ���������� ���������,��';
    15: Result:= '�����, ��������� ��� ��. ��������, ��';
    16: Result:= '������ �����, ��';
    17: Result:= '�����,��';
    18: Result:= '������, ��';
    19: Result:= '��������,��';
    20: Result:= '��������, ��';
    21: Result:= '����������� ����������,��';
    22: Result:= '��������, ��';
    23: Result:= '������������ ������ ����������, ��';
    24: Result:= '��� �����';
    25: Result:= '�����������';
    26: Result:= '���.����';
    27: Result:= '����';
    28: Result:= '������';
    29: Result:= '������ �������� ������';
    30: Result:= '���������';
 end;
end;

function getCHKname(i:integer):string;
begin
  case i of
     1: Result:= '����������� �����';
     2: Result:= '�������,��';
     3: Result:= '������������� �����';
     4: Result:= '�������� ���������';
     5: Result:= '������';
     8: Result:= '��������� ������';
     9: Result:= '����������� ���������';
     6: Result:= '��� ������������� ��';
     7: Result:= '����������� �������������';
    10: Result:= '�������������������� ������, ��';
    11: Result:= '�����, ������� ���������.��������,��';
    13: Result:= '����� ��� ��.-����.����-� � ��,��';
    14: Result:= '����� ��� ������, �� ������������, ��';
    12: Result:= '����� ��� ���������� ���������,��';
    15: Result:= '�����, ��������� ��� ��. ��������, ��';
    16: Result:= '������ �����, ��';
    17: Result:= '�����,��';
    18: Result:= '������, ��';
    19: Result:= '��������,��';
    20: Result:= '��������, ��';
    21: Result:= '����������� ����������,��';
    22: Result:= '��������, ��';
    23: Result:= '������������ ������ ����������, ��';
    24: Result:= '��� �����';
    25: Result:= '�����������';
    26: Result:= '���.����';
    27: Result:= '����';
    28: Result:= '������';
    29: Result:= '������ �������� ������';
    30: Result:= '���������';
 end;
end;

procedure TFMain.FormCreate(Sender: TObject);
var  ff : TFrame3;
     rec : tFrameChecked;
begin
  _parse:= tSortParser.Create;

  labelnames := TStringList.Create;
  labelnames.Add('����������� ����� ������� = ');
  labelnames.Add('������� = ');
  labelnames.Add('������������ ����� = ');
  labelnames.Add('�������� ��������� = ');
  labelnames.Add('������ = ');
  labelnames.Add('��� ������������� = ');
  labelnames.Add('����������� ������������� = ');
  labelnames.Add('��������� = ');
  labelnames.Add('����������� ��������� = ');
  labelnames.Add('�������� �������������������� ������');
  labelnames.Add('�������� �����, ������� �������������������� ��������');
  labelnames.Add('�������� ����� ��� ���������� ���������');
  labelnames.Add('�������� ����� ��� ������, �� ������������  � ������ ������ ��������');
  labelnames.Add('�������� ����� ��� ��������-������������� ���������-�, �������� � ��');
  labelnames.Add('�������� �����, ��������� ��� ������ ��������');
  labelnames.Add('�������� ������ �����');
  labelnames.Add('�������� �����');
  labelnames.Add('�������� ������');
  labelnames.Add('�������� ��������');
  labelnames.Add('�������� ��������');
  labelnames.Add('�������� ����������� ����������');
  labelnames.Add('�������� ��������');
  labelnames.Add('�������� ������������ ������ ����������');
  labelnames.Add('��� ����� = ');
  labelnames.Add('����������� = ');
  labelnames.Add('�������� ��������� ����������� ����');
  labelnames.Add('�������� ��������� ����');
  labelnames.Add('�������� ������� � �������');
  labelnames.Add('������ ������ = ');
  labelnames.Add('��������� = ');

  GridFieldNames := TStringList.Create;
  for I := 0 to 29 do
  begin
   GridFieldNames.Add(getFieldname(i+1));
  end;

  framenames := TList<TFRameChecked>.Create;
  for I := 0 to 29 do
  begin
    ff := TFrame3.Create(Self);
    ff.Name := 'fr'+inttostr(i+1);
    ff.Parent := Fmain.scrlbx1;
    ff.Visible := False;
    ff.Tag := i+1;
    ff.btn1.Tag := i+1;
    ff.lbl1.Caption := labelnames[i];
  
  //  ff.mm1.Visible := false;
  //  ff.mm1.ScrollBars := ssVertical;
  //  ff.mm1.Align := alClient;
    ff.Align := alTop;
    if ((i>8) and (i<23)) or (i=27) then
      ff.btn1.Enabled := False
    else
      ff.btn1.OnClick :=  FrameButtonClick;

    {if  (i=27)  then  ff.chk1.enabled := false;}

    ff.chk1.Tag := i+1;
    ff.chk1.OnClick := FrameCHKCkick;

    rec.frami := ff;
    rec.select := 0;
    rec.where := 0;
    rec.str_sel := getselname(i+1);
    rec.str_wh := '';
    rec.str_prefix := getprefname(i+1);
    framenames.Add(rec);
  end;
  pgc1.ActivePage := tsNewSH;
end;

procedure TFMain.FormShow(Sender: TObject);
begin
  select := '';
  where := '';
  tsRedactSh.TabVisible := False;
  pgc1.ActivePage := tsNewSH;
end;

{procedure TFMain.MemoCreate(Sender:TObject; mmname : string; var _rc : tFrameChecked);
var mm:TMemo;
begin
 mm := TMemo.Create(Self);
 mm.Name := mmname;
 mm.Font.Size := 11;
 mm.Font.Name := 'Calibri';
 mm.Parent := _rc.frami.pnl3;
 mm.Lines.Clear;
 mm.Lines.Add(_rc.frami.lbl1.Caption);
 _rc.frami.Height :=mm.Font.Size * mm.Lines.Count; //mm1.Height;
 mm.Align := alClient;
 mm.Enabled := false;
 mm.ScrollBars := ssVertical;
end;  }

procedure TFMain.FrameButtonClick(Sender: Tobject);
var ss : string; _rc : tFrameChecked; _ii,_jj, _j1: Integer;
begin
  ss :='';
  _rc := framenames[(Sender as TButton).tag-1];
  _ii := 43;
  case (Sender as TButton).tag of
    1 : begin
          _rc.str_wh :=fSimple.Execute('������� ����� ��� ������������ ������ ��');
          if _rc.str_wh <> '' then
             begin
              _rc.frami.lbl1.Caption := labelnames[(Sender as TButton).tag-1]+_rc.str_wh;
             _rc.where := 1;
             end;
        end;
    2 : begin
          _rc.str_wh :=uInterval.Execute('������� �������� ��� ������� ��');
          if _rc.str_wh <> '' then
            begin
              _rc.frami.lbl1.Caption := '������� �� = '+_rc.str_wh;
              _rc.where := 1;
            end;
        end;
    3 : begin
          ss := uSpr.Execute(_rc.str_wh,uDM.dm.dsRaion, '�������� �������� ������','T_NAME_LOCAL_PREFIX','',False);
          if ss <> '' then
          begin
           _rc.frami.lbl1.Caption := labelnames[(Sender as TButton).tag-1]+ss;
           str_Raionid := _rc.str_wh;
          // _ii := _rc.frami.pnl3.Height;
           if _rc.frami.lbl1.Width > _rc.frami.pnl3.Width then
           begin
            _rc.frami.lbl1.Visible :=false;
     {      _rc.frami.mm1.Visible := True;
            _rc.frami.mm1.Lines.Clear;
            _rc.frami.mm1.Lines.Add(_rc.frami.lbl1.Caption);
            _rc.frami.Height := _rc.frami.mm1.Font.Size *  _rc.frami.mm1.Lines.Count;
            _rc.frami.mm1.Align := alClient;
            if _rc.frami.Height < 43 then _rc.frami.Height := 43;
            _rc.frami.mm1.Enabled := false;         }
            
        //    _rc.frami.mm1.CaretPos.SetLocation(0,0);       
            mm1 := TMemo.Create(Self);
            mm1.Font.Size := 11;
            mm1.Font.Name := 'Calibri';
            mm1.Parent := _rc.frami.pnl3;
            mm1.Lines.Clear;
            mm1.Lines.Add(_rc.frami.lbl1.Caption);
            _rc.frami.Height :=mm1.Font.Size * mm1.Lines.Count; //mm1.Height;
            mm1.Align := alClient;
            mm1.Enabled := false;
            mm1.ScrollBars := ssVertical;


          {  if not Assigned(mm1) then  MemoCreate(Self, 'mm1',_rc)
            else
              begin
                mm1.Lines.Clear;
                mm1.Lines.Add(_rc.frami.lbl1.Caption);
               _rc.frami.Height :=mm1.Font.Size * mm1.Lines.Count;
              end;   }
           end
           else
            begin
             //if Assigned(mm1) then
            if mm1.Visible then
              begin
                mm1.Align := alNone;
                mm1.Height := _ii;
                mm1.Visible := false;
                mm1.Lines.Clear;
                mm1 := nil;
              end;

             { if  _rc.frami.mm1.Visible
              then
               begin 
                 _rc.frami.mm1.Visible :=False;
                 _rc.frami.mm1.Align := alNone;
                 _rc.frami.mm1.Height := _ii; 
                 _rc.frami.mm1.Lines.Clear;
               end;  }
             _rc.frami.lbl1.Visible :=true;
             _rc.frami.Height:= _ii;
            end;
          _rc.where := 1;
          end;
        end;
    4 : begin
          ss := uSpr.Execute(_rc.str_wh,uDM.dm.dsSP, '�������� �������� �������� ���������','T_SETTLE',str_Raionid,False);
          if ss <> '' then
          begin
           _rc.frami.lbl1.Caption := labelnames[(Sender as TButton).tag-1]+ss;
           str_SPid := _rc.str_wh;
      //     _ii := _rc.frami.lbl1.Height;
           if _rc.frami.lbl1.Width > _rc.frami.pnl3.Width then
           begin
            _rc.frami.lbl1.Visible :=false;            
      {      _rc.frami.mm1.Visible := True;
            _rc.frami.mm1.Align := alClient;
            _rc.frami.mm1.Lines.Clear;
            _rc.frami.mm1.Lines.Add(_rc.frami.lbl1.Caption);
            _rc.frami.Height := _rc.frami.mm1.Font.Size *  _rc.frami.mm1.Lines.Count;
            _rc.frami.mm1.Enabled := false;
            _rc.frami.mm1.ScrollBars := ssVertical;    }
            mm2 := TMemo.Create(Self);
            mm2.Font.Size := 11;
            mm2.Font.Name := 'Calibri';
            mm2.Parent := _rc.frami.pnl3;
            mm2.Lines.Clear;
            mm2.Lines.Add(_rc.frami.lbl1.Caption);
            _rc.frami.Height :=mm2.Font.Size * mm2.Lines.Count;
            mm2.Align := alClient;
            mm2.Enabled := false;
            mm2.ScrollBars := ssVertical;  
            
           end
           else
            begin
            if Assigned(mm2) then
              begin
               mm2.Visible := false;
               mm2.Lines.Clear;
               mm2:=nil;
              end;    
          //   if  _rc.frami.mm1.Visible then  _rc.frami.mm1.Visible :=False;   
             _rc.frami.lbl1.Visible :=true;
             _rc.frami.Height:= _ii;
            end;
          _rc.where := 1;
          end;
        end;
    5 : begin
          ss :=  uSpr.Execute(_rc.str_wh,uDM.dm.dsKolhoz, '�������� �������','T_KOLKHOZ',str_SPid,False);
          if ss <> '' then
          begin
           _rc.frami.lbl1.Caption := labelnames[(Sender as TButton).tag-1]+ss;
    //       _ii := _rc.frami.lbl1.Height;
           if _rc.frami.lbl1.Width > _rc.frami.pnl3.Width then
           begin
            _rc.frami.lbl1.Visible :=false;
         {   _rc.frami.mm1.Visible := True;
            _rc.frami.mm1.Align := alClient;
            _rc.frami.mm1.Lines.Clear;            
            _rc.frami.mm1.Lines.Add(_rc.frami.lbl1.Caption);
            _rc.frami.Height := _rc.frami.mm1.Font.Size *  _rc.frami.mm1.Lines.Count;
            _rc.frami.mm1.Enabled := false;
            _rc.frami.mm1.ScrollBars := ssVertical;   }
            mm3 := TMemo.Create(Self);
            mm3.Font.Size := 11;
            mm3.Font.Name := 'Calibri';
            mm3.Parent := _rc.frami.pnl3;
            mm3.Lines.Clear;
            mm3.Lines.Add(_rc.frami.lbl1.Caption);
            _rc.frami.Height :=mm3.Font.Size * mm3.Lines.Count;
            mm3.Align := alClient;
            mm3.Enabled := false;
            mm3.ScrollBars := ssVertical;    
           end
           else
            begin
            if Assigned(mm3) then
              begin
               mm3.Visible := false;
               mm3.Lines.Clear;
               mm3:=nil;
              end;   
         //   if  _rc.frami.mm1.Visible then  _rc.frami.mm1.Visible :=False;     
             _rc.frami.lbl1.Visible :=true;
             _rc.frami.Height:= _ii;
            end;
          _rc.where := 1;
          end;
        end;
    6 : begin
          ss :=  uSpr.Execute(_rc.str_wh,uDM.dm.dsUSE, '�������� ��� �������������','T_PERMITTED_USE','',False);
          if ss <> '' then
          begin
           _rc.frami.lbl1.Caption := labelnames[(Sender as TButton).tag-1]+ss;
     //      _ii := _rc.frami.lbl1.Height;
           if _rc.frami.lbl1.Width > _rc.frami.pnl3.Width then
           begin
            _rc.frami.lbl1.Visible :=false;
         {   _rc.frami.mm1.Visible := True;
            _rc.frami.mm1.Align := alClient;
            _rc.frami.mm1.Lines.Clear;            
            _rc.frami.mm1.Lines.Add(_rc.frami.lbl1.Caption);
            _rc.frami.Height := _rc.frami.mm1.Font.Size *  _rc.frami.mm1.Lines.Count;
            _rc.frami.mm1.Enabled := false;
            _rc.frami.mm1.ScrollBars := ssVertical;     }
            mm4 := TMemo.Create(Self);
            mm4.Font.Size := 11;
            mm4.Font.Name := 'Calibri';
            mm4.Parent := _rc.frami.pnl3;
            mm4.Lines.Clear;
            mm4.Lines.Add(_rc.frami.lbl1.Caption);
            _rc.frami.Height :=mm4.Font.Size * mm4.Lines.Count;
            mm4.Align := alClient;
            mm4.Enabled := false;
            mm4.ScrollBars := ssVertical;               
           end
           else
            begin
            if Assigned(mm4) then
              begin
               mm4.Visible := false;
               mm4.Lines.Clear;
               mm4:=nil;
              end; 
            // if  _rc.frami.mm1.Visible then  _rc.frami.mm1.Visible :=False;
             _rc.frami.lbl1.Visible :=true;
             _rc.frami.Height:= _ii;
            end;
           _rc.where := 1;
          end;
        end;
    7 : begin
          _rc.str_wh :=fSimple.Execute('������� ����� ��� ������������ �������������');
          if _rc.str_wh <> '' then
          begin
            _rc.frami.lbl1.Caption := labelnames[(Sender as TButton).tag-1]+_rc.str_wh;
            _rc.where := 1;
          end;
        end;
    8 : begin
          ss :=  uSpr.Execute(_rc.str_wh,uDM.dm.dsCategory, '�������� ��������� ������','T_CATEGORY','',False);
          if ss <> '' then
          begin
           _rc.frami.lbl1.Caption := labelnames[(Sender as TButton).tag-1]+ss;
    //       _ii := _rc.frami.lbl1.Height;
           if _rc.frami.lbl1.Width > _rc.frami.pnl3.Width then
           begin
            _rc.frami.lbl1.Visible :=false;
          {  _rc.frami.mm1.Visible := True;
            _rc.frami.mm1.Align := alClient;
            _rc.frami.mm1.Lines.Clear;            
            _rc.frami.mm1.Lines.Add(_rc.frami.lbl1.Caption);
            _rc.frami.Height := _rc.frami.mm1.Font.Size *  _rc.frami.mm1.Lines.Count;
            _rc.frami.mm1.Enabled := false;
            _rc.frami.mm1.ScrollBars := ssVertical;      }
            mm5 := TMemo.Create(Self);
            mm5.Font.Size := 11;
            mm5.Font.Name := 'Calibri';
            mm5.Parent := _rc.frami.pnl3;
            mm5.Lines.Clear;
            mm5.Lines.Add(_rc.frami.lbl1.Caption);
            _rc.frami.Height :=mm5.Font.Size * mm5.Lines.Count;
            mm5.Align := alClient;
            mm5.Enabled := false;
            mm5.ScrollBars := ssVertical;  
           end
           else
            begin
            if Assigned(mm5) then
              begin
               mm5.Visible := false;
               mm5.Lines.Clear;
               mm5:=nil;
              end; 
         //   if  _rc.frami.mm1.Visible then  _rc.frami.mm1.Visible :=False;
             _rc.frami.lbl1.Visible :=true;
            _rc.frami.Height:= _ii;
            end;
          _rc.where := 1;
          end;
        end;
    9 : begin
          _rc.str_wh :=uInterval.Execute('������� �������� ��� ����������� ��������� ��');
          if _rc.str_wh <> '' then begin
            _rc.frami.lbl1.Caption := labelnames[(Sender as TButton).tag-1]+_rc.str_wh;
            _rc.where := 1;
          end;
        end;
    24 : begin
         ss :=  uSpr.Execute(_rc.str_wh,uDM.dm.dsRights, '�������� ��� �����','T_RIGHT_TYPE','',False);
          if ss <> '' then
          begin
           _rc.frami.lbl1.Caption := labelnames[(Sender as TButton).tag-1]+ss;
     //      _ii := _rc.frami.lbl1.Height;
           if _rc.frami.lbl1.Width > _rc.frami.pnl3.Width then
           begin
            _rc.frami.lbl1.Visible :=false;
        {    _rc.frami.mm1.Visible := True;
            _rc.frami.mm1.Align := alClient;
            _rc.frami.mm1.Lines.Clear;            
            _rc.frami.mm1.Lines.Add(_rc.frami.lbl1.Caption);
            _rc.frami.Height := _rc.frami.mm1.Font.Size *  _rc.frami.mm1.Lines.Count;
            _rc.frami.mm1.Enabled := false;
            _rc.frami.mm1.ScrollBars := ssVertical;   }
            mm6 := TMemo.Create(Self);
            mm6.Font.Size := 11;
            mm6.Font.Name := 'Calibri';
            mm6.Parent := _rc.frami.pnl3;
            mm6.Lines.Clear;
            mm6.Lines.Add(_rc.frami.lbl1.Caption);
            _rc.frami.Height :=mm6.Font.Size * mm6.Lines.Count;
            mm6.Align := alClient;
            mm6.Enabled := false;
            mm6.ScrollBars := ssVertical; 
           end
           else
            begin
            if Assigned(mm6) then
              begin
               mm6.Visible := false;
               mm6.Lines.Clear;
               mm6 := nil;
              end; 
           //  if  _rc.frami.mm1.Visible then  _rc.frami.mm1.Visible :=False;
             _rc.frami.lbl1.Visible :=true;
             _rc.frami.Height:= _ii;
            end;
          _rc.where := 1;
          end;
        end;
    25 : begin
          TButton(Sender).Cursor := crSQLWait;
          ss :=  uSpr.Execute(_rc.str_wh,uDM.dm.dsOwners, '�������� �������������','OWNER ','',False);
          if ss <> '' then
          begin
           _rc.frami.lbl1.Caption := labelnames[(Sender as TButton).tag-1]+ss;
    //       _ii := _rc.frami.lbl1.Height;
           if _rc.frami.lbl1.Width > _rc.frami.pnl3.Width then
           begin
           _rc.frami.lbl1.Visible :=false;
         {  _rc.frami.mm1.Visible := True;
           _rc.frami.mm1.Align := alClient;
           _rc.frami.mm1.Lines.Clear;           
           _rc.frami.mm1.Lines.Add(_rc.frami.lbl1.Caption);
           _rc.frami.Height := _rc.frami.mm1.Font.Size *  _rc.frami.mm1.Lines.Count;
           _rc.frami.mm1.Enabled := false;
           _rc.frami.mm1.ScrollBars := ssVertical;  }
            mm7 := TMemo.Create(Self);
            mm7.Font.Size := 11;
            mm7.Font.Name := 'Calibri';
            mm7.Parent := _rc.frami.pnl3;
            mm7.Lines.Clear;
            mm7.Lines.Add(_rc.frami.lbl1.Caption);
            _rc.frami.Height :=mm7.Font.Size * mm7.Lines.Count;
            mm7.Align := alClient;
            mm7.Enabled := false;
            mm7.ScrollBars := ssVertical;       
           end
           else
            begin
            if Assigned(mm7) then
              begin
               mm7.Visible := false;
               mm7.Lines.Clear;
               mm7:=nil;
              end;     
           //   if  _rc.frami.mm1.Visible then  _rc.frami.mm1.Visible :=False;
             _rc.frami.lbl1.Visible :=true;
             _rc.frami.Height:= _ii;
            end;
          _rc.where := 1;
          end;
          TButton(Sender).Cursor := crArrow;
        end;
    26 :begin
          _rc.str_wh :=uPerecluch.Execute('��������', '����������� ����');
          if _rc.str_wh <> '' then
          begin
            if _rc.str_wh='true' then _rc.frami.lbl1.Caption := labelnames[(Sender as TButton).tag-1]+'������'
            else
            if _rc.str_wh='false' then  _rc.frami.lbl1.Caption := labelnames[(Sender as TButton).tag-1]+'�� ������';
           _rc.where := 1;
          end;
        end;
    27 :begin
         _rc.str_wh :=uPerecluch.Execute('��������', '���� ����');
          if _rc.str_wh <> '' then
          begin
            if _rc.str_wh='true' then _rc.frami.lbl1.Caption := labelnames[(Sender as TButton).tag-1]+'������'
                                 else _rc.frami.lbl1.Caption := labelnames[(Sender as TButton).tag-1]+'�� ������';
           _rc.where := 1;
          end;
        end;
    29 : begin
          _rc.str_wh :=uPeriod.Execute();
          if _rc.str_wh <> '' then
          begin
              _rc.frami.lbl1.Caption := labelnames[(Sender as TButton).tag-1]+_rc.str_wh;
              _rc.where := 1;
          end;
        end;
    30 : begin
          TButton(Sender).Cursor := crSQLWait;
          ss :=  uSpr.Execute(_rc.str_wh,uDM.dm.dsOwners, '�������� �����������','OWNER ','',False);
          if ss <> '' then
          begin
           _rc.frami.lbl1.Caption := labelnames[(Sender as TButton).tag-1]+ss;
    //       _ii := _rc.frami.lbl1.Height;
           if _rc.frami.lbl1.Width > _rc.frami.pnl3.Width then
           begin
            _rc.frami.lbl1.Visible :=false;
         {   _rc.frami.mm1.Visible := True;
            _rc.frami.mm1.Align := alClient;
            _rc.frami.mm1.Lines.Clear;            
            _rc.frami.mm1.Lines.Add(_rc.frami.lbl1.Caption);
            _rc.frami.Height := _rc.frami.mm1.Font.Size *  _rc.frami.mm1.Lines.Count;
            if _rc.frami.Height < 43 then _rc.frami.Height := 43;            
            _rc.frami.mm1.Enabled := false;
            _rc.frami.mm1.ScrollBars := ssVertical;    }
            mm8 := TMemo.Create(Self);
            mm8.Font.Size := 11;
            mm8.Font.Name := 'Calibri';
            mm8.Parent := _rc.frami.pnl3;
            mm8.Lines.Clear;
            mm8.Lines.Add(_rc.frami.lbl1.Caption);
            _rc.frami.Height :=mm8.Font.Size * mm8.Lines.Count;
            mm8.Align := alClient;
            mm8.Enabled := false;
            mm8.ScrollBars := ssVertical;   
           end
           else
            begin
            if Assigned(mm8) then
              begin
               mm8.Visible := false;
               mm8.Lines.Clear;
               mm8:=nil;
              end;    
          //   if  _rc.frami.mm1.Visible then  _rc.frami.mm1.Visible :=False;
             _rc.frami.lbl1.Visible :=true;
             _rc.frami.Height:= _ii;
            end;
          _rc.where := 1;
          end;
          TButton(Sender).Cursor := crArrow;
        end;
  end;
  framenames[(Sender as TButton).tag-1] := _rc;
 // memo1.Lines.Add(_rc.str_wh);
end;

procedure TFMain.FrameCHKCkick(Sender: TOBject);
var  i,j : Integer;  _rc : tFrameChecked;
begin
 _rc := framenames[(Sender as TCheckBox).tag-1];
 if (Sender as TCheckBox).Checked then  _rc.select := 1
                                  else  _rc.select := 0;
 framenames[(Sender as TCheckBox).tag-1] := _rc;
end;

procedure TFMain.MenuItem1Click(Sender: TObject);
begin
  {   chkCadNUm.Checked := True;
     chkSquare.Checked := True ;
     chkRayon.Checked := True;
     chkSPos.Checked := True;
     chkKolhoz.Checked := True;
     chkIspZU.Checked := True;
     chk1.Checked := True;
     chkCategory.Checked := True;
     chkCadPrice.Checked := True;
     chkGR1.Checked := True;
     chk2.Checked := True;
     chkGR3.Checked := True;
     chkGR4.Checked := True;
     chkGR5.Checked := True;
     chkGR6.Checked := True;
     chkGr7.Checked := True;
     chk5.Checked := True;
     chk7.Checked := True;
     chk6.Checked := True;
     chk4.Checked := True;
     chkty3.Checked := True;
     chkty2.Checked := True;
     chkTY1.Checked := True;
     chk8.Checked := True;
     chk3.Checked := True;
     chk9.Checked := True;
     chk10.Checked := True;
     chk11.Checked := True;
     chk13.Checked := True;
     chk12.Checked := True;      }
end;

procedure TFMain.MenuItem2Click(Sender: TObject);
var _rc : tFrameChecked; _i: Integer;
begin
     chkCadNUm.Checked := false;
     chkSquare.Checked := false ;
     chkRayon.Checked := false;
     chkSPos.Checked := false;
     chkKolhoz.Checked := false;
     chkIspZU.Checked := false;
     chk1.Checked := false;
     chkCategory.Checked := false;
     chkCadPrice.Checked := false;
     chkGR1.Checked := false;
     chk2.Checked := false;
     chkGR3.Checked := false;
     chkGR4.Checked := false;
     chkGR5.Checked := false;
     chkGR6.Checked := false;
     chkGr7.Checked := false;
     chk5.Checked := false;
     chk7.Checked := false;
     chk6.Checked := false;
     chk4.Checked := false;
     chkty3.Checked := false;
     chkty2.Checked := false;
     chkTY1.Checked := false;
     chk8.Checked := false;
     chk3.Checked := false;
     chk9.Checked := false;
     chk10.Checked := false;
     chk11.Checked := false;
     chk13.Checked := false;
     chk12.Checked := false;

   for _I := 0 to framenames.Count-1 do
   begin
   _rc := framenames[_i];

    if not _rc.frami.lbl1.Visible then
      begin
        case  _i+1 of
          3  : FreeAndNil(mm1);
          4  : FreeAndNil(mm2);
          5  : FreeAndNil(mm3);
          6  : FreeAndNil(mm4);
          8  : FreeAndNil(mm5);
          24 : FreeAndNil(mm6);
          25 : FreeAndNil(mm7);
          30 : FreeAndNil(mm8);
        end;
        _rc.frami.lbl1.Visible := true;
      end;
   _rc.frami.Visible := false;
   _rc.str_wh := '';
   _rc.where := 0;
   _rc.select := 0;
   _rc.frami.chk1.Checked :=false;
   _rc.frami.pnl1.Height := 43;
   _rc.frami.pnl2.Height := 43;
   _rc.frami.pnl3.Height := 43;
   _rc.frami.lbl1.Caption := labelnames[_i];

   framenames[_i] := _rc;
   end;
end;

procedure TFMain.N1Click(Sender: TObject);
begin
  for I := 0 to framenames.Count-1 do
    begin
      if framenames[i].frami.Visible then framenames[i].frami.chk1.Checked:=true;
    end;
end;

procedure TFMain.N2Click(Sender: TObject);
begin
  for I := 0 to framenames.Count-1 do
    begin
      if framenames[i].frami.Visible then framenames[i].frami.chk1.Checked := false;
    end;
end;

procedure TFMain.SetCurrentQuery(const Value: string);
var _ii : Integer;  _att: tAttributs; _rc : tFrameChecked ;
begin
 MenuItem2Click(nil);
 uParser.Execute(Value,_parse);

 //UI

  for _att :=Low(tAttributs)  to  High(tAttributs) do
    if _parse.Attr.Contains(_att) then
    begin
       case _att of
         aCadNum : chkCadNUm.Checked := True;
         aArea:    chkSquare.Checked := True ;
         aRaion:   chkRayon.Checked := True;
         aSP:      chkSPos.Checked := True;
         aKLH:     chkKolhoz.Checked := True;
         aKTG:     chkIspZU.Checked := True;
         aCADCOST: chk1.Checked := True;
         aISP:     chkCategory.Checked := True;
         aFISP:    chkCadPrice.Checked := True;
         aGR1:     chkGR1.Checked := True;
         AGR2:     chk2.Checked := True;
         aGR3:     chkGR3.Checked := True;
         aGR4:     chkGR4.Checked := True;
         aGR5:     chkGR5.Checked := True;
         aGR6:     chkGR6.Checked := True;
         aGR7:     chkGr7.Checked := True;
         aUG1:     chk5.Checked := True;
         aUG2:     chk7.Checked := True;
         aUG3:     chk6.Checked := True;
         aUG4:     chk4.Checked := True;
         aUG5:     chkty3.Checked := True;
         aUG6:     chkty2.Checked := True;
         aUG7:     chkTY1.Checked := True;
         aRight:   chk8.Checked := True;
         aSobst:   chk3.Checked := True;
         aKUCH:    chk9.Checked := True;
         aUFRS:    chk10.Checked := True;
         aArenda:  chk11.Checked := True;
         aPeriod:  chk13.Checked := True;
         aASOBST:  chk12.Checked := True;
       end;
      _ii := Ord(_att);
      _rc := framenames[_ii];
      _rc.select := 1;
      _rc.frami.chk1.Checked := True;
      framenames[_ii] := _rc;
    end;

    for _att :=Low(tAttributs)  to  High(tAttributs) do
     if _parse.Where.ContainsKey(_att) then
       begin
        case _att of
         aCadNum : if not chkCadNUm.Checked then chkCadNUm.Checked := True;
         aArea:    if not chkSquare.Checked then chkSquare.Checked := True ;
         aRaion:   if not chkRayon.Checked  then chkRayon.Checked := True;
         aSP:      if not chkSPos.Checked   then chkSPos.Checked := True;
         aKLH:     if not chkKolhoz.Checked then chkKolhoz.Checked := True;
         aKTG:     if not chkIspZU.Checked  then chkIspZU.Checked := True;
         aCADCOST: if not chk1.Checked then chk1.Checked := True;
         aISP:     if not chkCategory.Checked then chkCategory.Checked := True;
         aFISP:    if not chkCadPrice.Checked then chkCadPrice.Checked := True;
         aGR1:     if not chkGR1.Checked then chkGR1.Checked := True;
         AGR2:     if not chk2.Checked then chk2.Checked := True;
         aGR3:     if not chkGR3.Checked then chkGR3.Checked := True;
         aGR4:     if not chkGR4.Checked then chkGR4.Checked := True;
         aGR5:     if not chkGR5.Checked then chkGR5.Checked := True;
         aGR6:     if not chkGR6.Checked then chkGR6.Checked := True;
         aGR7:     if not chkGr7.Checked then chkGr7.Checked := True;
         aUG1:     if not chk5.Checked then chk5.Checked := True;
         aUG2:     if not chk7.Checked then chk7.Checked := True;
         aUG3:     if not chk6.Checked then chk6.Checked := True;
         aUG4:     if not chk4.Checked then chk4.Checked := True;
         aUG5:     if not chkty3.Checked then chkty3.Checked := True;
         aUG6:     if not chkty2.Checked then chkty2.Checked := True;
         aUG7:     if not chkTY1.Checked then chkTY1.Checked := True;
         aRight:   if not chk8.Checked then chk8.Checked := True;
         aSobst:   if not chk3.Checked then chk3.Checked := True;
         aKUCH:    if not chk9.Checked then chk9.Checked := True;
         aUFRS:    if not chk10.Checked then chk10.Checked := True;
         aArenda:  if not chk11.Checked then chk11.Checked := True;
         aPeriod:  if not chk13.Checked then chk13.Checked := True;
         aASOBST:  if not chk12.Checked then chk12.Checked := True;
       end;
         _ii := Ord(_att);
         _rc := framenames[_ii];
         _rc.where := 1;
         _rc.str_wh := _parse.Where.Items[_att];
         if ((_ii >8) and (_ii<23)) or (i=27) then
           _rc.frami.lbl1.Caption := labelnames[_ii]
         else
          _rc.frami.lbl1.Caption := labelnames[_ii] + _parse.Where.Items[_att];
         framenames[_ii] := _rc;
       end;


//  //SQL
//  if _parse.Attr.Contains(tAttributs.aCadNum) then
//   begin
//     //dsfsdfsdf
//   end;
end;

procedure TFMain.tsNewSHShow(Sender: TObject);
begin
  if btn10.Enabled = false then btn10.Enabled := True;
 // lbl2.Visible := false;
end;

end.
