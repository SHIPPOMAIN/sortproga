object Form5: TForm5
  Left = 0
  Top = 0
  Caption = #1041#1080#1073#1083#1080#1086#1090#1077#1082#1072' '#1079#1072#1087#1088#1086#1089#1086#1074
  ClientHeight = 445
  ClientWidth = 733
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object VirtTrView: TVirtualStringTree
    Left = 0
    Top = 0
    Width = 273
    Height = 410
    Align = alLeft
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Calibri'
    Font.Style = []
    Header.AutoSizeIndex = 0
    Header.Font.Charset = DEFAULT_CHARSET
    Header.Font.Color = clWindowText
    Header.Font.Height = -11
    Header.Font.Name = 'Tahoma'
    Header.Font.Style = []
    Header.MainColumn = -1
    ParentFont = False
    PopupMenu = pmShbl
    TabOrder = 0
    TabStop = False
    TreeOptions.MiscOptions = [toAcceptOLEDrop, toCheckSupport, toEditable, toFullRepaintOnResize, toInitOnSave, toToggleOnDblClick, toWheelPanning, toEditOnClick]
    OnEditing = VirtTrViewEditing
    OnFreeNode = VirtTrViewFreeNode
    OnGetText = VirtTrViewGetText
    OnPaintText = VirtTrViewPaintText
    OnNewText = VirtTrViewNewText
    OnNodeClick = VirtTrViewNodeClick
    ExplicitLeft = -6
    ExplicitTop = -6
    Columns = <>
  end
  object mmo1: TMemo
    Left = 273
    Top = 0
    Width = 460
    Height = 410
    Align = alClient
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Calibri'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
  end
  object pnlTools: TPanel
    Left = 0
    Top = 410
    Width = 733
    Height = 35
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    object btnBack: TButton
      Left = 0
      Top = 0
      Width = 155
      Height = 35
      Align = alLeft
      Caption = 'btnBack'
      ImageIndex = 0
      Images = fmImages.img32
      TabOrder = 0
      OnClick = btnBackClick
    end
    object btnNext: TButton
      Left = 578
      Top = 0
      Width = 155
      Height = 35
      Align = alRight
      Caption = 'btnNext'
      ImageAlignment = iaRight
      ImageIndex = 1
      Images = fmImages.img32
      TabOrder = 1
      OnClick = btnNextClick
    end
  end
  object pmShbl: TPopupMenu
    Left = 96
    Top = 128
    object N4: TMenuItem
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1075#1088#1091#1087#1087#1091
      OnClick = N4Click
    end
    object N6: TMenuItem
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1096#1072#1073#1083#1086#1085
      OnClick = N6Click
    end
    object N5: TMenuItem
      Caption = '-'
    end
    object N3: TMenuItem
      Caption = #1047#1072#1075#1088#1091#1079#1080#1090#1100' '#1096#1072#1073#1083#1086#1085' '#1074' '#1088#1077#1076#1072#1082#1090#1086#1088
      OnClick = N3Click
    end
    object N8: TMenuItem
      Caption = '-'
    end
    object N2: TMenuItem
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1075#1088#1091#1087#1087#1091
      OnClick = N2Click
    end
    object N7: TMenuItem
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1074#1089#1077' '#1096#1072#1073#1083#1086#1085#1099' '#1074' '#1075#1088#1091#1087#1087#1077
      OnClick = N7Click
    end
    object N9: TMenuItem
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1096#1072#1073#1083#1086#1085
      OnClick = N9Click
    end
  end
end
