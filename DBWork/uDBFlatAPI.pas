unit uDBFlatAPI;

interface
uses
 System.SysUtils,
 system.generics.collections,
 FMX.Dialogs ;
type

  tGroup = record
    ID : double;
    Name : string;
//    procedure DeleteGroup(ID:double);
  end;

  tRequest = record
    id : Double;
    groupid : Double;
    NameRequest : string;
    RequestVlaue : string;
  end;

  TDBGroupAPIScope = record
    public
    class function GetAllGroups:TArray<tGroup>; static;     // �������� ��� ������
    class function GetGroup(value:double):tGroup; static;   // �������� ���� ������
    class function RenameGroup(valueID: Double; valueName:string):tGroup; static; // ������������� ������
    class function DeleteGroup(value : Double; _per: boolean):Boolean; static;    // ������� ������
    class function CreateNewGroup(value:string):tGroup;static;   // ������� ����� ������
    class function DeleteAllRequestfromGroup(value : Double):Boolean; static;
  end;

  TDBRequestAPIScope = record
    public
    class function GetAllRequests(Gr_iD:double):TArray<tRequest>; static;     // �������� ��� ������� �� ������
    class function GetRequest(value:double):tRequest; static;   // �������� ���� ������ �� ����
    class function RenameRequest(valueID: Double; valueName:string):tRequest; static; // ������������� ������
    class function DeleteRequest(value : Double):Boolean; static;    // ������� ������
    class function CreateNewRequest(valueName: string; GrID:double; value:string):tRequest;static;   // ������� ����� ������
  end;

  resourcestring
   _str_SQLGetAllGroupforSYSDBA = 'Select ID,Name from t_Group';
   _str_SQLGetAllGroupforOTHER = 'Select ID,Name from t_Group where user_id=:p1';


implementation
 uses uDM, uText, Units.Paths;

//procedure tGroup.DeleteGroup(ID: double);
//begin
//
//end;




{ DBGroupAPIScope }

class function TDBGroupAPIScope.CreateNewGroup(value: string): tGroup;
var _ID:Double;
begin
 with dm do
 begin
  try
   if ibqryGROUPGenID.Active then ibqryGROUPGenID.Close;
   ibqryGROUPGenID.active := true;
   _ID :=   lrgntfldIBQGenIdGEN_ID.Value;
   ibqryGROUPGenID.active := false;

   if ibqryInsertGroup.Active then ibqryInsertGroup.Close;
   ibqryInsertGroup.Params[0].Value := _ID;
   ibqryInsertGroup.Params[1].Value := value;
   ibqryInsertGroup.Params[2].Value := AnPAths.DB_USER_ID;
   ibqryInsertGroup.ExecSQL;
   ibtrnsctn1.CommitRetaining;
    Result.ID:= _ID;
    Result.Name := value;
  except
    ShowMessage(erGROUPINSERT);
    ibtrnsctn1.RollbackRetaining;
    Result.ID := 0;
    Result.Name := '';
  end;
 end;
end;

class function TDBGroupAPIScope.DeleteAllRequestfromGroup(value: Double): Boolean;
begin
 with dm do
 begin
   try
    if ibqryDeleteRequests.Active then ibqryDeleteRequests.Close;
    ibqryDeleteRequests.Params[0].Value := value;
    ibqryDeleteRequests.ExecSQL;
    ibtrnsctn1.CommitRetaining;
    Result := True;
   except
     ShowMessage(erRequestDelete);
     ibtrnsctn1.RollbackRetaining;
     Result := false;
   end;
 end;
end;

class function TDBGroupAPIScope.DeleteGroup(value: Double; _per: boolean):boolean;
begin
// PEr -false - ������� ������ ������
// per - true - ������� ������ ������ � ��������� � ��� ���������
 with dm do
 begin
  if _per = False then
    try
     if ibqryDeleteGroup.Active then ibqryDeleteGroup.Close;
     ibqryDeleteGroup.Params[0].Value := value;
     ibqryDeleteGroup.ExecSQL;
     //ibqryDeleteGroup.close;
     ibtrnsctn1.CommitRetaining;
     Result := True;
    except
     ShowMessage(erGroupNotEmpty);
   //  ibqryDeleteGroup.close;
     ibtrnsctn1.RollbackRetaining;
     Result := false;
    end
  else
   try
    if ibqryDeleteRequests.Active then ibqryDeleteRequests.Close;
    ibqryDeleteRequests.Params[0].Value := value;
    ibqryDeleteRequests.ExecSQL;
  //  ibqryDeleteRequests.close;
    if ibqryDeleteGroup.Active then ibqryDeleteGroup.Close;
    ibqryDeleteGroup.Params[0].Value := value;
    ibqryDeleteGroup.ExecSQL;
  //  ibqryDeleteGroup.close;
    ibtrnsctn1.CommitRetaining;
    Result := True;
   except
     ShowMessage(erGroupNotEmpty);
   //  ibqryDeleteRequests.close;
   //  ibqryDeleteGroup.close;
     ibtrnsctn1.RollbackRetaining;
     Result := false;
   end;
 end;
end;

class function TDBGroupAPIScope.GetAllGroups: TArray<tGroup>;
var _AG : TArray<tGroup>; _i : Integer;
begin
 with dm do
 begin
  if ibqryGetAllGroup.Active then ibqryGetAllGroup.Close;                          //05-09-16
  if AnPAths.DB_USER_ID =1 then                           // enter by SYSDBA
    begin
      ibqryGetAllGroup.SQL.Clear;
      ibqryGetAllGroup.SQL.Add(_str_SQLGetAllGroupforSYSDBA);
    end
  else
    begin
      ibqryGetAllGroup.SQL.Clear;                         // enter somebody else
      ibqryGetAllGroup.SQL.Add(_str_SQLGetAllGroupforOTHER);
      ibqryGetAllGroup.Params[0].Value := AnPAths.DB_USER_ID;
    end;
  ibqryGetAllGroup.Open;
  //if not ibqryGetAllGroup.Active then ibqryGetAllGroup.Open;                    // end 05-09-16
  ibqryGetAllGroup.First;
  _i := 0;
  while not ibqryGetAllGroup.eof do
   begin
    _i:=_i+1;
    ibqryGetAllGroup.Next;
   end;
   ibqryGetAllGroup.First;
  // _AG :=  TArray<tGroup>.Create;
   SetLength(_AG, _i);
  _i:=0;
  while not ibqryGetAllGroup.eof do
  begin
    _AG[_i].ID   := ibqryGetAllGroup.FieldByName('ID').Value;
    _AG[_i].Name := ibqryGetAllGroup.FieldByName('NAME').Value;
    inc(_i);
    ibqryGetAllGroup.Next;
  end;
  ibqryGetAllGroup.Close;
  Result := _AG;
  SetLength(_AG, 0);
 end;
end;

class function TDBGroupAPIScope.GetGroup(value:double): tGroup;
var _AG : tGroup;
begin
 with dm do
 begin
   if ibqryGetGroup.Active then ibqryGetGroup.Close;
   ibqryGetGroup.ParamByName('p1').Value := value;
   ibqryGetGroup.Open;
   if not ibqryGetGroup.IsEmpty then
   begin
     _AG.ID := ibqryGetGroup.FieldByName('ID').Value;
     _AG.NAME := ibqryGetGroup.FieldByName('NAME').Value;
   end
   else
   begin
     _AG.ID := 0;
     _AG.NAME := '';
   end;
  ibqryGetGroup.Close;
  Result := _AG;
 end;
end;

class function TDBGroupAPIScope.RenameGroup(valueID: Double; valueName:string): tGroup;
begin
with dm do
 begin
   if ibqryREnameGroup.Active then ibqryREnameGroup.Close;
   ibqryREnameGroup.ParamByName('p1').Value := valueID;
   ibqryREnameGroup.ParamByName('NAME').Value := valueNAME;
   ibqryREnameGroup.ExecSQL;
   ibtrnsctn1.CommitRetaining;
   Result.ID   := valueID;
   Result.Name := valueName;
 end;
end;

{ TDBRequestAPIScope }

class function TDBRequestAPIScope.CreateNewRequest(valueName: string; GrID:double; value:string): tRequest;
var _ID:Double;
begin
 with dm do
 begin
  try
   if ibqryRequestGenID.Active then ibqryRequestGenID.Close;
   ibqryRequestGenID.active := true;
   _ID :=   lrgntfld1.Value;
   ibqryRequestGenID.active := false;

   if ibqryInsertRequest.Active then ibqryInsertRequest.Close;
   ibqryInsertRequest.Params[0].Value := _ID;
   ibqryInsertRequest.Params[1].Value := GrID;
   ibqryInsertRequest.Params[2].Value := valueName;
   ibqryInsertRequest.Params[3].Value := value;
   ibqryInsertRequest.ExecSQL;
   ibtrnsctn1.CommitRetaining;
    Result.ID:= _ID;
    Result.groupid :=GrID;
    Result.NameRequest := valueName;
    Result.RequestVlaue := value;
  except
    ShowMessage(erRequestINSERT);
    ibtrnsctn1.RollbackRetaining;
    Result.ID:= 0;
    Result.groupid :=0;
    Result.NameRequest := '';
    Result.RequestVlaue := '';
  end;
 end;
end;

class function TDBRequestAPIScope.DeleteRequest(value: Double): Boolean;
begin
 with dm do
 begin
   try
    if ibqryDeleteRequest.Active then ibqryDeleteRequest.Close;
    ibqryDeleteRequest.Params[0].Value := value;
    ibqryDeleteRequest.ExecSQL;
  //  ibqryDeleteRequest.close;
    ibtrnsctn1.CommitRetaining;
    Result := True;
   except
     ShowMessage(erRequestDelete);
   //  ibqryDeleteRequest.close;
     ibtrnsctn1.RollbackRetaining;
     Result := false;
   end;
end;
end;

class function TDBRequestAPIScope.GetAllRequests(Gr_iD:double): TArray<tRequest>;
var _AR : TArray<tRequest>; _i : Integer;
begin
 with dm do
 begin
 // if not ibqryGetAllReguests.Active then ibqryGetAllReguests.Open;
   if ibqryGetAllReguests.Active then ibqryGetAllReguests.close;
  ibqryGetAllReguests.Params[0].Value := Gr_iD;
  ibqryGetAllReguests.Open;
  ibqryGetAllReguests.First;
  _i := 0;
  while not ibqryGetAllReguests.eof do
   begin
    _i:=_i+1;
    ibqryGetAllReguests.Next;
   end;
   ibqryGetAllReguests.First;
  // _AG :=  TArray<tGroup>.Create;
   SetLength(_AR, _i);
  _i:=0;
  while not ibqryGetAllReguests.eof do
  begin
    _AR[_i].ID        := ibqryGetAllReguests.FieldByName('ID').Value;
    _AR[_i].groupid   := ibqryGetAllReguests.FieldByName('GROUP_ID').Value;
    _AR[_i].NameRequest  := ibqryGetAllReguests.FieldByName('NAME').Value;
    _AR[_i].RequestVlaue := ibqryGetAllReguests.FieldByName('RVALUE').Value;
    inc(_i);
    ibqryGetAllReguests.Next;
  end;
  ibqryGetAllReguests.Close;
  Result := _AR;
  SetLength(_AR, 0);
 end;

end;

class function TDBRequestAPIScope.GetRequest(value: double): tRequest;
var _AR : tRequest;
begin
 with dm do
 begin
   if ibqryGetRequest.Active then ibqryGetRequest.Close;
   ibqryGetRequest.ParamByName('p1').Value := value;
   ibqryGetRequest.Open;
   if not ibqryGetRequest.IsEmpty then
   begin
     _AR.ID           := ibqryGetRequest.FieldByName('ID').Value;
     _AR.groupid      := ibqryGetRequest.FieldByName('GROUP_ID').Value;
     _AR.NameRequest  := ibqryGetRequest.FieldByName('NAME').Value;
     _AR.RequestVlaue := ibqryGetRequest.FieldByName('RVALUE').Value;
   end
   else
   begin
     _AR.ID := 0;
     _AR.groupid := 0;
     _AR.NameRequest := '';
     _AR.RequestVlaue := '';
   end;
  ibqryGetRequest.Close;
  Result := _AR;
 end;

end;

class function TDBRequestAPIScope.RenameRequest(valueID: Double; valueName: string): tRequest;
begin
with dm do
 begin
   if ibqryREnameRequest.Active then ibqryREnameRequest.Close;
   ibqryREnameRequest.ParamByName('p1').Value := valueID;
   ibqryREnameRequest.ParamByName('NAME').Value := valueNAME;
   ibqryREnameRequest.ExecSQL;
   ibtrnsctn1.CommitRetaining;
   Result := GetRequest(valueID);
 end;
end;

end.
