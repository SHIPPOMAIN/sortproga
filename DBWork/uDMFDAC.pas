unit uDMFDAC;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.PG,
  FireDAC.Phys.PGDef, Data.DB, FireDAC.Comp.Client, FireDAC.Stan.Param,
  FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt, FireDAC.Comp.DataSet,
  FireDAC.VCLUI.Wait, FireDAC.Comp.UI,FMX.Dialogs;

type
  TdmFD = class(TDataModule)
    fdphyspgdrvrlnk1: TFDPhysPgDriverLink;
    fdqry1: TFDQuery;
    fdgxwtcrsr1: TFDGUIxWaitCursor;
    fdqry2: TFDQuery;
    fdqry3: TFDQuery;
    conPostGres: TFDConnection;
  private
    { Private declarations }
  public
    { Public declarations }
    function LogIn(): string;
  end;

var
  dmFD: TdmFD;

implementation
 uses uText, Units.Paths;
{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TdmFD }

function TdmFD.LogIn: string;
var ss:string; i:Integer;
begin
Result:= '';
TRY

  if conPostGres.Connected then conPostGres.Connected:=False;

  //conPostGres.Params.Clear;
  //conPostGres.DriverName := 'PG';

  i:=conPostGres.Params.IndexOfName('Server');
  conPostGres.Params.Delete(i);
  conPostGres.Params.Add('Server='+AnPAths.DB_Postgress_server);
  i:=conPostGres.Params.IndexOfName('User_Name');
  conPostGres.Params.Delete(i);
  conPostGres.Params.Add('User_Name='+AnPAths.DB_USER);
  i:=conPostGres.Params.IndexOfName('Password');
  conPostGres.Params.Delete(i);
  conPostGres.Params.Add('Password='+AnPAths.DB_PASS);


  {conPostGres.Params.Add('DatabaseName= '+AnPAths.DB_NAME_POSTGRESS);
  conPostGres.Params.Add('User_Name='+AnPAths.DB_USER);
  conPostGres.Params.Add('Password='+AnPAths.DB_PASS);
  conPostGres.Params.Add('Server='+AnPAths.DB_Postgress_server);
  conPostGres.Params.Add('Port='+AnPAths.DB_POSTGRESS_PORT);
  conPostGres.Params.Add('MetaDefShema=public');
  conPostGres.Params.Add('Pooled=false');
  conPostGres.Params.Add('ExtendedMetadata=false');
  conPostGres.Params.Add('LoginTimeout=0');
  conPostGres.Params.Add('CharacterSet=csNone');   }

  conPostGres.Connected:=True;

  fdqry3.Active := True;
  fdqry3.First;
  i:=0;
  while not fdqry3.eof do
  begin
    ss:= fdqry3.Fields[0].AsString;
    fdqry3.Next;
    inc(i);
  end;
  fdqry3.Active := false;
except on E : Exception do
  begin
  //ShowMessage(E.Message);
  Result:= rsErrorPOSTGRESSCONNECT;
  end;
end;
end;

end.
