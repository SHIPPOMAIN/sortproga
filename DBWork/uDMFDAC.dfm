object dmFD: TdmFD
  OldCreateOrder = False
  Height = 271
  Width = 323
  object fdphyspgdrvrlnk1: TFDPhysPgDriverLink
    Left = 152
    Top = 32
  end
  object fdqry1: TFDQuery
    Connection = conPostGres
    SQL.Strings = (
      
        'select ST_AsGeoJSON(ST_Transform(ST_Envelope(agric.geom),3857)) ' +
        'As bbox from agric where agric.number =:p1')
    Left = 56
    Top = 96
    ParamData = <
      item
        Name = 'P1'
        DataType = ftString
        ParamType = ptInput
        Value = Null
      end>
  end
  object fdgxwtcrsr1: TFDGUIxWaitCursor
    Provider = 'Forms'
    Left = 152
    Top = 97
  end
  object fdqry2: TFDQuery
    Connection = conPostGres
    SQL.Strings = (
      'select realarea from agric where number = :p1')
    Left = 56
    Top = 160
    ParamData = <
      item
        Name = 'P1'
        ParamType = ptInput
      end>
  end
  object fdqry3: TFDQuery
    Connection = conPostGres
    SQL.Strings = (
      'SELECT pg_class.relname,pg_namespace.nspname '
      
        'FROM pg_class INNER JOIN pg_namespace ON pg_class.relnamespace=p' +
        'g_namespace.oid '
      'WHERE pg_table_is_visible(pg_class.oid);')
    Left = 56
    Top = 216
  end
  object conPostGres: TFDConnection
    Params.Strings = (
      'Database=ums_test'
      'User_Name=postgres'
      'Password=root'
      'Server=195.98.73.242'
      'DriverID=PG')
    Left = 64
    Top = 32
  end
end
