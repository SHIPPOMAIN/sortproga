object dm: Tdm
  OldCreateOrder = False
  Height = 517
  Width = 881
  object ibdtbs1: TIBDatabase
    DatabaseName = 'D:\WORK\AGRILAND\base 22-06-2016\lands22062016.fdb'
    Params.Strings = (
      'user_name=SYSDBA'
      'lc_ctype=WIN1251'
      'password=superVlands')
    LoginPrompt = False
    DefaultTransaction = ibtrnsctn1
    ServerType = 'IBServer'
    Left = 80
    Top = 32
  end
  object ibtrnsctn1: TIBTransaction
    DefaultDatabase = ibdtbs1
    Left = 128
    Top = 32
  end
  object ibqry1: TIBQuery
    Database = ibdtbs1
    Transaction = ibtrnsctn1
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'Select db_user, db_password, id'
      'from t_user'
      'where upper(login)=upper(:p1)'
      'and upper(user_password) = upper(:p2)')
    Left = 192
    Top = 32
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'p1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'p2'
        ParamType = ptUnknown
      end>
  end
  object ibqry2: TIBQuery
    Database = ibdtbs1
    Transaction = ibtrnsctn1
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    Left = 256
    Top = 32
  end
  object dsRaion: TDataSource
    DataSet = ibqryRaion
    Left = 40
    Top = 160
  end
  object dsKolhoz: TDataSource
    DataSet = ibqryKolhoz
    Left = 120
    Top = 160
  end
  object dsSP: TDataSource
    DataSet = ibqrySP
    Left = 192
    Top = 160
  end
  object dsCategory: TDataSource
    DataSet = ibqryCategory
    Left = 256
    Top = 160
  end
  object dsUSE: TDataSource
    DataSet = ibqryUSE
    Left = 48
    Top = 280
  end
  object ibqryRaion: TIBQuery
    Database = ibdtbs1
    Transaction = ibtrnsctn1
    AfterOpen = ibqryRaionAfterOpen
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'select lp.ID, lp.name, lp.local_prefix'
      'from t_name_local_prefix lp')
    Left = 32
    Top = 112
    object lrgntfldRaionID: TLargeintField
      FieldName = 'ID'
      Origin = '"T_NAME_LOCAL_PREFIX"."ID"'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
      Visible = False
    end
    object ibstrngfldRaionNAME: TIBStringField
      DisplayLabel = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077' '#1084#1091#1085#1080#1094#1080#1087#1072#1083#1100#1085#1086#1075#1086' '#1088#1072#1081#1086#1085#1072
      DisplayWidth = 50
      FieldName = 'NAME'
      Origin = '"T_NAME_LOCAL_PREFIX"."NAME"'
      Size = 200
    end
    object intgrfldRaionLOCAL_PREFIX: TIntegerField
      FieldName = 'LOCAL_PREFIX'
      Origin = '"T_NAME_LOCAL_PREFIX"."LOCAL_PREFIX"'
      Visible = False
    end
  end
  object ibqryKolhoz: TIBQuery
    Database = ibdtbs1
    Transaction = ibtrnsctn1
    AfterOpen = ibqryKolhozAfterOpen
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      
        ' select kk.id, kk.name, se.name, nlm.name from t_kolkhoz kk left' +
        ' join t_settle se on (kk.settle_id = se.id) left join t_name_loc' +
        'al_prefix nlm on (se.name_local_prefix_id = nlm.id)'
      'where ((:p1 =0 ) or (kk.settle_id =:p2) ) '
      '  order by 4,3,2')
    Left = 112
    Top = 112
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'p1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'p2'
        ParamType = ptUnknown
      end>
    object lrgntfldKolhozID: TLargeintField
      FieldName = 'ID'
      Origin = '"T_KOLKHOZ"."ID"'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
      Visible = False
    end
    object ibstrngfldKolhozNAME: TIBStringField
      DisplayLabel = #1050#1086#1083#1093#1086#1079#1099
      DisplayWidth = 50
      FieldName = 'NAME'
      Origin = '"T_KOLKHOZ"."NAME"'
      Size = 200
    end
    object ibstrngfldKolhozNAME1: TIBStringField
      DisplayLabel = #1057#1077#1083#1100#1089#1082#1080#1077' '#1087#1086#1089#1077#1083#1077#1085#1080#1103
      DisplayWidth = 50
      FieldName = 'NAME1'
      Origin = '"T_SETTLE"."NAME"'
      Size = 200
    end
    object ibstrngfldKolhozNAME2: TIBStringField
      DisplayLabel = #1052#1091#1085#1080#1094#1080#1087#1072#1083#1100#1085#1099#1077' '#1088#1072#1081#1086#1085#1099
      DisplayWidth = 50
      FieldName = 'NAME2'
      Origin = '"T_NAME_LOCAL_PREFIX"."NAME"'
      Size = 200
    end
  end
  object ibqrySP: TIBQuery
    Database = ibdtbs1
    Transaction = ibtrnsctn1
    AfterOpen = ibqrySPAfterOpen
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'select se.id,  se.name, nlm.name '
      
        'from t_settle se left join t_name_local_prefix nlm on (se.name_l' +
        'ocal_prefix_id = nlm.id)'
      'where ((:p1 =0 ) or (se.name_local_prefix_id =:p2) )'
      ' order by 3,2')
    Left = 184
    Top = 112
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'p1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'p2'
        ParamType = ptUnknown
      end>
    object lrgntfldSPID: TLargeintField
      FieldName = 'ID'
      Origin = '"T_SETTLE"."ID"'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
      Visible = False
    end
    object ibstrngfldSPNAME: TIBStringField
      DisplayLabel = #1057#1077#1083#1100#1089#1082#1080#1077' '#1087#1086#1089#1077#1083#1077#1085#1080#1103
      DisplayWidth = 50
      FieldName = 'NAME'
      Origin = '"T_SETTLE"."NAME"'
      Size = 200
    end
    object ibstrngfldSPNAME1: TIBStringField
      DisplayLabel = #1052#1091#1085#1080#1094#1080#1087#1072#1083#1100#1085#1099#1077' '#1088#1072#1081#1086#1085#1099
      DisplayWidth = 50
      FieldName = 'NAME1'
      Origin = '"T_NAME_LOCAL_PREFIX"."NAME"'
      Size = 200
    end
  end
  object ibqryCategory: TIBQuery
    Database = ibdtbs1
    Transaction = ibtrnsctn1
    AfterOpen = ibqryCategoryAfterOpen
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      ' select cc.id, cc.name from t_land_category cc')
    Left = 256
    Top = 112
    object lrgntfldCategoryID: TLargeintField
      FieldName = 'ID'
      Origin = '"T_LAND_CATEGORY"."ID"'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
      Visible = False
    end
    object ibstrngfldCategoryNAME: TIBStringField
      DisplayLabel = #1050#1072#1090#1077#1075#1086#1088#1080#1080' '#1079#1077#1084#1077#1083#1100
      DisplayWidth = 100
      FieldName = 'NAME'
      Origin = '"T_LAND_CATEGORY"."NAME"'
      Size = 500
    end
  end
  object ibqryUSE: TIBQuery
    Database = ibdtbs1
    Transaction = ibtrnsctn1
    AfterOpen = ibqryUSEAfterOpen
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      ' select put.id, put.code ,put.name from t_permitted_use_type put')
    Left = 40
    Top = 224
    object lrgntfldUSEID1: TLargeintField
      FieldName = 'ID'
      Origin = '"T_PERMITTED_USE_TYPE"."ID"'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
      Visible = False
    end
    object lrgntfldUSECODE1: TLargeintField
      DisplayLabel = #1050#1086#1076
      FieldName = 'CODE'
      Origin = '"T_PERMITTED_USE_TYPE"."CODE"'
    end
    object ibstrngfldUSENAME1: TIBStringField
      DisplayLabel = #1042#1080#1076#1099' '#1080#1089#1087#1086#1083#1100#1079#1086#1074#1072#1085#1080#1103' '#1047#1059
      FieldName = 'NAME'
      Origin = '"T_PERMITTED_USE_TYPE"."NAME"'
      Size = 200
    end
  end
  object ibqryRights: TIBQuery
    Database = ibdtbs1
    Transaction = ibtrnsctn1
    AfterOpen = ibqryRightsAfterOpen
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'select rt.id, rt.parent_id, rt.name from t_right_type rt'
      '   order by 2,3')
    Left = 128
    Top = 224
    object lrgntfldRightsID: TLargeintField
      FieldName = 'ID'
      Origin = '"T_RIGHT_TYPE"."ID"'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
      Visible = False
    end
    object lrgntfldRightsPARENT_ID: TLargeintField
      FieldName = 'PARENT_ID'
      Origin = '"T_RIGHT_TYPE"."PARENT_ID"'
      Visible = False
    end
    object ibstrngfldRightsNAME: TIBStringField
      DisplayLabel = #1042#1080#1076' '#1087#1088#1072#1074#1072
      FieldName = 'NAME'
      Origin = '"T_RIGHT_TYPE"."NAME"'
      Size = 200
    end
  end
  object dsRights: TDataSource
    DataSet = ibqryRights
    Left = 120
    Top = 280
  end
  object ibqryOwners: TIBQuery
    Database = ibdtbs1
    Transaction = ibtrnsctn1
    AfterOpen = ibqryOwnersAfterOpen
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      
        'select pp.id, pp.family||'#39' '#39'||pp.name||'#39' '#39'||pp.last_name  Name, ' +
        '1, '#39#1060#1080#1079#1080#1095#1077#1089#1082#1086#1077' '#1083#1080#1094#1086#39', nlp.name from t_person pp  left join t_nam' +
        'e_local_prefix nlp on (pp.local_prefix = nlp.local_prefix)'
      ''
      '  union'
      ''
      
        '  select ee.id, ee.full_name Name, 2, '#39#1070#1088#1080#1076#1080#1095#1077#1089#1082#1086#1077' '#1083#1080#1094#1086#39',  nlp.n' +
        'ame from t_enterprise ee  left join t_name_local_prefix nlp on (' +
        'ee.local_prefix = nlp.local_prefix)')
    Left = 200
    Top = 224
    object lrgntfldOwnersID: TLargeintField
      FieldName = 'ID'
      Origin = '"T_PERSON"."ID"'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
      Visible = False
    end
    object ibstrngfldOwnersNAME: TIBStringField
      DisplayLabel = #1057#1086#1073#1089#1090#1074#1077#1085#1085#1080#1082
      DisplayWidth = 150
      FieldName = 'NAME'
      ProviderFlags = []
      Size = 602
    end
    object intgrfldOwnersCONSTANT: TIntegerField
      FieldName = 'CONSTANT'
      ProviderFlags = []
      ReadOnly = True
      Visible = False
    end
    object ibstrngfldOwnersF_1: TIBStringField
      DisplayLabel = #1058#1080#1087' '#1089#1086#1073#1089#1090#1074#1077#1085#1085#1080#1082#1072
      FieldName = 'F_1'
      ProviderFlags = []
      FixedChar = True
      Size = 16
    end
    object ibstrngfldOwnersNAME1: TIBStringField
      DisplayLabel = #1052#1091#1085#1080#1094#1080#1087#1072#1083#1100#1085#1099#1081' '#1088#1072#1081#1086#1085
      DisplayWidth = 50
      FieldName = 'NAME1'
      Origin = '"T_NAME_LOCAL_PREFIX"."NAME"'
      Size = 200
    end
  end
  object dsOwners: TDataSource
    DataSet = ibqryOwners
    Left = 200
    Top = 280
  end
  object dsResult: TDataSource
    DataSet = ibqryResult
    Left = 272
    Top = 280
  end
  object ibqryResult: TIBQuery
    Database = ibdtbs1
    Transaction = ibtrnsctn1
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    Left = 280
    Top = 224
  end
  object ibqryGetAllGroup: TIBQuery
    Database = ibdtbs1
    Transaction = ibtrnsctn1
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'Select ID,Name from t_Group')
    Left = 480
    Top = 112
  end
  object ibqryGETGroup: TIBQuery
    Database = ibdtbs1
    Transaction = ibtrnsctn1
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'Select ID,Name, user_id '
      'from t_Group'
      'where ID=:p1')
    Left = 616
    Top = 112
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'p1'
        ParamType = ptUnknown
      end>
  end
  object ibqryRenameGroup: TIBQuery
    Database = ibdtbs1
    Transaction = ibtrnsctn1
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'UPDATE T_GROUP'
      'SET NAME=:NAME'
      'where ID=:p1')
    Left = 672
    Top = 136
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'NAME'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'p1'
        ParamType = ptUnknown
      end>
  end
  object ibqryDeleteGroup: TIBQuery
    Database = ibdtbs1
    Transaction = ibtrnsctn1
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'Delete from t_GROUP'
      'where ID=:p1')
    Left = 552
    Top = 128
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'p1'
        ParamType = ptUnknown
      end>
  end
  object ibqryDeleteRequests: TIBQuery
    Database = ibdtbs1
    Transaction = ibtrnsctn1
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'Delete from T_REQUESTS'
      'where GROUP_ID=:p1')
    Left = 576
    Top = 224
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'p1'
        ParamType = ptUnknown
      end>
  end
  object ibqryGroupGenId: TIBQuery
    Database = ibdtbs1
    Transaction = ibtrnsctn1
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'select  GEN_ID(G_GROUP,1) from RDB$DATABASE;')
    Left = 808
    Top = 128
    object lrgntfldIBQGenIdGEN_ID: TLargeintField
      FieldName = 'GEN_ID'
      ProviderFlags = []
    end
  end
  object ibqryInsertGroup: TIBQuery
    Database = ibdtbs1
    Transaction = ibtrnsctn1
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'INSERT INTO T_GROUP(ID,NAME,USER_ID) VALUES(:p1,:p2,:p3)')
    Left = 744
    Top = 112
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'p1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'p2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'p3'
        ParamType = ptUnknown
      end>
  end
  object ibqryInsertRequest: TIBQuery
    Database = ibdtbs1
    Transaction = ibtrnsctn1
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      
        'INSERT INTO T_REQUESTS(ID,GROUP_ID,NAME,RVALUE) VALUES(:p1,:p2,:' +
        'p3,:p4)')
    Left = 504
    Top = 208
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'p1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'p2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'p3'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'p4'
        ParamType = ptUnknown
      end>
    object lrgntfld2: TLargeintField
      FieldName = 'GEN_ID'
      ProviderFlags = []
    end
  end
  object ibqryREquestGenID: TIBQuery
    Database = ibdtbs1
    Transaction = ibtrnsctn1
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'select  GEN_ID(G_REQESTS,1) from RDB$DATABASE;')
    Left = 720
    Top = 232
    object lrgntfld1: TLargeintField
      FieldName = 'GEN_ID'
      ProviderFlags = []
    end
  end
  object ibqryDeleteREquest: TIBQuery
    Database = ibdtbs1
    Transaction = ibtrnsctn1
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'Delete from T_REQUESTS'
      'where ID=:p1')
    Left = 504
    Top = 256
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'p1'
        ParamType = ptUnknown
      end>
  end
  object ibqryGetAllReguests: TIBQuery
    Database = ibdtbs1
    Transaction = ibtrnsctn1
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'Select ID,Group_ID,Name,RVALUE from t_Requests'
      'where Group_id = :p1')
    Left = 648
    Top = 208
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'p1'
        ParamType = ptUnknown
      end>
  end
  object ibqryGetRequest: TIBQuery
    Database = ibdtbs1
    Transaction = ibtrnsctn1
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'Select ID,Group_ID,Name,RVALUE from t_Requests'
      'where id = :p1')
    Left = 784
    Top = 216
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'p1'
        ParamType = ptUnknown
      end>
  end
  object ibqryREnameRequest: TIBQuery
    Database = ibdtbs1
    Transaction = ibtrnsctn1
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'UPDATE t_Requests'
      'SET NAME = :NAME'
      'where id = :p1')
    Left = 576
    Top = 272
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'NAME'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'p1'
        ParamType = ptUnknown
      end>
  end
  object ibqryStatistica: TIBQuery
    Database = ibdtbs1
    Transaction = ibtrnsctn1
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'select right_name ,'
      'cast(sum(Laera_category) as numeric(18,2)) s1,'
      'cast(sum(Larea_group) as numeric(18,2))  s2,'
      'cast(sum(Larea_type) as numeric(18,2))  s3,'
      'cast(sum(larea_reg1) as numeric(18,2))  s4,'
      'cast(sum(larea_reg2) as numeric(18,2))  s5'
      'from t_temp_statistica'
      'group by 1')
    Left = 464
    Top = 24
    object ibstrngfldStatisticaRIGHT_NAME: TIBStringField
      DisplayLabel = #1042#1080#1076' '#1087#1088#1072#1074#1072
      DisplayWidth = 50
      FieldName = 'RIGHT_NAME'
      Origin = '"T_TEMP_STATISTICA"."RIGHT_NAME"'
      Size = 200
    end
    object ibcdfldStatisticaS1: TIBBCDField
      DisplayLabel = #1050#1072#1090#1077#1075#1086#1088#1080#1103' '#1089'-'#1093' '#1085#1072#1079#1085'-'#1103
      FieldName = 'S1'
      ProviderFlags = []
      Precision = 18
      Size = 2
    end
    object ibcdfldStatisticaS2: TIBBCDField
      DisplayLabel = #1057#1077#1083#1100#1093#1086#1079' '#1091#1075#1086#1076#1100#1103
      FieldName = 'S2'
      ProviderFlags = []
      Precision = 18
      Size = 2
    end
    object ibcdfldStatisticaS3: TIBBCDField
      DisplayLabel = #1055#1072#1096#1085#1103
      FieldName = 'S3'
      ProviderFlags = []
      Precision = 18
      Size = 2
    end
    object ibcdfldStatisticaS4: TIBBCDField
      DisplayLabel = #1055#1088#1086#1096#1077#1076#1096#1080#1077' '#1082#1072#1076'.'#1091#1095#1077#1090
      FieldName = 'S4'
      ProviderFlags = []
      Precision = 18
      Size = 2
    end
    object ibcdfldStatisticaS5: TIBBCDField
      DisplayLabel = #1055#1088#1086#1096#1077#1076#1096#1080#1077' '#1059#1060#1056#1057
      FieldName = 'S5'
      ProviderFlags = []
      Precision = 18
      Size = 2
    end
  end
  object dsStat: TDataSource
    DataSet = ibqryStatistica
    Left = 512
    Top = 32
  end
  object ibqryReport: TIBQuery
    Database = ibdtbs1
    Transaction = ibtrnsctn1
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'SELECT * FROM PR_REPORT_PASPORT_FOR_ANALITICA(:p1)'
      '')
    Left = 472
    Top = 360
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'p1'
        ParamType = ptUnknown
      end>
  end
  object ibqryReportID: TIBQuery
    Database = ibdtbs1
    Transaction = ibtrnsctn1
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'SELECT * FROM PR_REPORT_PASPORT_FOR_ANALITICA(:p1)')
    Left = 392
    Top = 360
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'p1'
        ParamType = ptUnknown
      end>
  end
  object ibqryReportRight: TIBQuery
    Database = ibdtbs1
    Transaction = ibtrnsctn1
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'SELECT * FROM PR_REPORT_RIGHTS_FOR_ANALITICA(:p1)')
    Left = 561
    Top = 368
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'p1'
        ParamType = ptUnknown
      end>
  end
  object ibqryREportArenda: TIBQuery
    Database = ibdtbs1
    Transaction = ibtrnsctn1
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'SELECT * FROM PR_REPORT_ARENDA_FOR_ANALITICA(:p1)')
    Left = 665
    Top = 368
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'p1'
        ParamType = ptUnknown
      end>
  end
  object ibqryOWNERRIGHT: TIBQuery
    Database = ibdtbs1
    Transaction = ibtrnsctn1
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'SELECT * FROM  PR_REPORT_OWNERS_FOR_ANALITICA(:p1)')
    Left = 473
    Top = 408
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'p1'
        ParamType = ptUnknown
      end>
  end
  object ibqryOWNERARENDA: TIBQuery
    Database = ibdtbs1
    Transaction = ibtrnsctn1
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'SELECT * FROM  PR_REPORT_OWNERS_FOR_ANALITICA(:p1)')
    Left = 585
    Top = 416
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'p1'
        ParamType = ptUnknown
      end>
  end
end
