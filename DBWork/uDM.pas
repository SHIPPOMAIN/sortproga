unit uDM;

interface

uses
  System.SysUtils, System.Classes, IBX.IBDatabase, Data.DB, IBX.IBCustomDataSet,
  IBX.IBQuery,uParser,System.StrUtils;

//type
// tAttributs = (aCadNum,aArea,aRaion,aSP,aKLH,aKTG,aCADCOST,aISP,aFISP,aGR1,AGR2,aGR3,aGR4,aGR5,aGR6,aGR7,aUG1,aUG2,aUG3,aUG4,aUG5,aUG6,aUG7,aRight,aSobst,aKUCH, aUFRS, aArenda, aPeriod,aASOBST);

type
  Tdm = class(TDataModule)
    ibdtbs1: TIBDatabase;
    ibtrnsctn1: TIBTransaction;
    ibqry1: TIBQuery;
    ibqry2: TIBQuery;
    dsRaion: TDataSource;
    dsKolhoz: TDataSource;
    dsSP: TDataSource;
    dsCategory: TDataSource;
    dsUSE: TDataSource;
    ibqryRaion: TIBQuery;
    lrgntfldRaionID: TLargeintField;
    ibstrngfldRaionNAME: TIBStringField;
    intgrfldRaionLOCAL_PREFIX: TIntegerField;
    ibqryKolhoz: TIBQuery;
    lrgntfldKolhozID: TLargeintField;
    ibstrngfldKolhozNAME: TIBStringField;
    ibstrngfldKolhozNAME1: TIBStringField;
    ibstrngfldKolhozNAME2: TIBStringField;
    ibqrySP: TIBQuery;
    lrgntfldSPID: TLargeintField;
    ibstrngfldSPNAME: TIBStringField;
    ibstrngfldSPNAME1: TIBStringField;
    ibqryCategory: TIBQuery;
    lrgntfldCategoryID: TLargeintField;
    ibstrngfldCategoryNAME: TIBStringField;
    ibqryUSE: TIBQuery;
    lrgntfldUSEID1: TLargeintField;
    lrgntfldUSECODE1: TLargeintField;
    ibstrngfldUSENAME1: TIBStringField;
    ibqryRights: TIBQuery;
    lrgntfldRightsID: TLargeintField;
    lrgntfldRightsPARENT_ID: TLargeintField;
    ibstrngfldRightsNAME: TIBStringField;
    dsRights: TDataSource;
    ibqryOwners: TIBQuery;
    dsOwners: TDataSource;
    lrgntfldOwnersID: TLargeintField;
    ibstrngfldOwnersNAME: TIBStringField;
    intgrfldOwnersCONSTANT: TIntegerField;
    ibstrngfldOwnersF_1: TIBStringField;
    ibstrngfldOwnersNAME1: TIBStringField;
    dsResult: TDataSource;
    ibqryResult: TIBQuery;
    ibqryGetAllGroup: TIBQuery;
    ibqryGETGroup: TIBQuery;
    ibqryRenameGroup: TIBQuery;
    ibqryDeleteGroup: TIBQuery;
    ibqryDeleteRequests: TIBQuery;
    ibqryGroupGenId: TIBQuery;
    lrgntfldIBQGenIdGEN_ID: TLargeintField;
    ibqryInsertGroup: TIBQuery;
    ibqryInsertRequest: TIBQuery;
    lrgntfld2: TLargeintField;
    ibqryREquestGenID: TIBQuery;
    lrgntfld1: TLargeintField;
    ibqryDeleteREquest: TIBQuery;
    ibqryGetAllReguests: TIBQuery;
    ibqryGetRequest: TIBQuery;
    ibqryREnameRequest: TIBQuery;
    ibqryStatistica: TIBQuery;
    dsStat: TDataSource;
    ibqryReport: TIBQuery;
    ibqryReportID: TIBQuery;
    ibqryReportRight: TIBQuery;
    ibqryREportArenda: TIBQuery;
    ibstrngfldStatisticaRIGHT_NAME: TIBStringField;
    ibcdfldStatisticaS1: TIBBCDField;
    ibcdfldStatisticaS2: TIBBCDField;
    ibcdfldStatisticaS3: TIBBCDField;
    ibcdfldStatisticaS4: TIBBCDField;
    ibcdfldStatisticaS5: TIBBCDField;
    ibqryOWNERRIGHT: TIBQuery;
    ibqryOWNERARENDA: TIBQuery;
    procedure ibqryRaionAfterOpen(DataSet: TDataSet);
    procedure ibqryKolhozAfterOpen(DataSet: TDataSet);
    procedure ibqrySPAfterOpen(DataSet: TDataSet);
    procedure ibqryCategoryAfterOpen(DataSet: TDataSet);
    procedure ibqryUSEAfterOpen(DataSet: TDataSet);
    procedure ibqryRightsAfterOpen(DataSet: TDataSet);
    procedure ibqryOwnersAfterOpen(DataSet: TDataSet);

   
  private
    { Private declarations }
  public
    { Public declarations }

   // ����������� � ��
   //���� ��������� �� ����, ������ �� �������� ����� �� �������
   function LogIn(const pUser,pPass,pPathDB: string): string;
   procedure inquiry(Value : string);
   function GetIDNAMES(Value : string; _att : TAttributs):string;
   procedure CreateExcel(fpath:string);
   procedure OPenReport(pKN:string);
   procedure CloseReport;
  end;

var
  dm: Tdm;

implementation

uses Units.Paths,uText;{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}



procedure Tdm.CloseReport;
begin
    if ibqryReport.Active then ibqryReport.Close;
    if ibqryReportRight.Active then ibqryReportRight.Close;
    if ibqryREportArenda.Active then ibqryREportArenda.Close;
end;

procedure Tdm.CreateExcel(fpath: string);
var ff : TextFile;  _i:Integer;
begin
  if ibqryResult.Active then
  begin
    if not DirectoryExists(fpath) then MkDir(fpath);
    

    AssignFile(ff, fpath+FormatDatetime('dd.mm.yyyy hh.mm.ss', Now())+'.csv');
    Rewrite(ff);
    ibqryResult.DisableControls;
    ibqryResult.First;
    for _i := 0 to  ibqryResult.FieldCount-1 do
       Write(ff,ibqryResult.Fields[_i].DisplayLabel+';');
    Writeln(ff);

    while not ibqryResult.eof do
    begin
      for _i := 0 to  ibqryResult.FieldCount-1 do
       Write(ff,ibqryResult.Fields[_i].AsString+';');
      Writeln(ff);
      ibqryResult.Next;
    end;

   ibqryResult.First;
   ibqryResult.EnableControls;
   CloseFile(FF);
  end;
end;

function Tdm.GetIDNAMES(Value: string; _att : TAttributs): string;
var ss, ss2, ss3, _s1,_s2:string;
begin
with dm do
begin
 if Value <> '' then
 begin
     if ibqry2.active then ibqry2.Close;
     ibqry2.SQL.Clear;
     ss:=''; ss3 := ''; _s1:='';_s2 :='';

     if _att = aRaion then ss := 'Select Name from t_name_local_prefix where id in '+Value
     else
     if _att = aSP then ss := 'Select Name from t_SETTLE where id in '+Value
     else
     if _att = aKLH then ss := 'Select Name from t_KOLKHOZ where id in '+Value
     else
     if _att = aKTG then ss := 'Select Name from t_LAND_CATEGORY where id in '+Value
     else
     if _att = aISP then ss := 'Select Name from  t_permitted_use_type where id in '+Value
     else
     if _att = aRight then ss := 'Select Name from  t_RIGHT_TYPE where id in '+Value
     else
     if (_att = aSobst) or (_att = aASOBST) then
       begin
        if Pos('�',Value)<>0 then _s2 := Copy(Value,Pos('�',Value)+1,Pos(')',Value)-1);
        if Pos('�',Value)<>0 then _s1 := Copy(Value,Pos('�',Value)+1,PosEx(')',Value,Pos('�',Value)+1));

        ss := 'select  pp.family||'+''' '''+ '||pp.name||'+''' '''+ '||pp.last_name  Name from t_person pp where pp.id in '+_s1;
        ss3 := 'select ee.full_name Name from t_enterprise ee where ee.id in '+_s2;
       end;

     if (ss<>'') and (_s1='') and (_s2 ='') then
        ibqry2.SQL.Add(ss)
     else
      begin
        if (_s1 <>'') and (_s2<>'') then ibqry2.SQL.Add(ss+' UNION '+ss3)
        else
        if (_s1 <>'') and (_s2='') then  ibqry2.SQL.Add(ss)
        else
        if (_s1 ='') and (_s2<>'') then  ibqry2.SQL.Add(ss3);
      end ;

     ibqry2.Open;
     ibqry2.First;
     ss2 :='';
     while not ibqry2.eof do
     begin
       if ss2 = '' then ss2 := ibqry2.Fields[0].AsString
       else ss2 := ss2 +',' + ibqry2.Fields[0].AsString;
       ibqry2.Next;
     end;
     ibqry2.Close;

     Result := ss2;
 end
 else
    Result := '';
end;
end;

procedure Tdm.ibqryCategoryAfterOpen(DataSet: TDataSet);
begin
 DataSet.Fields[0].Visible := false;
end;

procedure Tdm.ibqryKolhozAfterOpen(DataSet: TDataSet);
begin
 DataSet.Fields[0].Visible := false;
end;

procedure Tdm.ibqryOwnersAfterOpen(DataSet: TDataSet);
begin
 DataSet.Fields[0].Visible := false;
end;

procedure Tdm.ibqryRaionAfterOpen(DataSet: TDataSet);
begin
 DataSet.Fields[0].Visible := false;
end;

procedure Tdm.ibqryRightsAfterOpen(DataSet: TDataSet);
begin
 DataSet.Fields[0].Visible := false;
end;

procedure Tdm.ibqrySPAfterOpen(DataSet: TDataSet);
begin
 DataSet.Fields[0].Visible := false;
end;

procedure Tdm.ibqryUSEAfterOpen(DataSet: TDataSet);
begin
 DataSet.Fields[0].Visible := false;
end;

procedure Tdm.inquiry(Value : string);
begin
 if ibqryResult.Active then ibqryResult.Close;
 ibtrnsctn1.CommitRetaining;
 ibqryResult.SQL.Clear;
 ibqryResult.SQL.Add(Value);
 ibqryResult.Open;
end;

function Tdm.LogIn(const pUser, pPass, pPathDB: string): string;
var
 ss1,ss2: string;
begin
Result:= '';
TRY
  if ibdtbs1.Connected then ibdtbs1.Connected := false;

  ibdtbs1.Params.Clear;
  ibdtbs1.DatabaseName:= pPathDB;
  ibdtbs1.Params.Add('user_name=sysdba');
  ibdtbs1.Params.Add('password=superVlands');
  ibdtbs1.Params.Add('lc_ctype=win1251');
  ibdtbs1.Connected:=True;
  ibqry1.Active := False;
  ibqry1.Params[0].Value := pUser;
  ibqry1.Params[1].Value := pPass;
  ibqry1.Open;

  if not ibqry1.IsEmpty then
   begin
    ss1 := ibqry1.Fields[0].AsString ;
    ss2 := ibqry1.Fields[1].AsString;
    AnPAths.DB_USER_ID := ibqry1.Fields[2].Value;

    ibdtbs1.Connected:=false;
    ibdtbs1.Params.Clear;
    ibdtbs1.DatabaseName:= pPathDB;
    ibdtbs1.Params.Add('user_name='+ss1);
    ibdtbs1.Params.Add('password='+ss2);
    ibdtbs1.Params.Add('lc_ctype=win1251');
    ibdtbs1.Connected:=True;
    ibtrnsctn1.StartTransaction;

    ibqry1.Close;
    //SaveUsertofile(pUser, pPass);

    //fConnect.Visible := False;
    //Fmain.Show;
   end
  else
   begin
     ibqry1.Close;
     ibdtbs1.Connected:=false;
     Result:= rsCheckCorrectnessUserPass;
   end;

except on E : Exception do
 Result:= rsErrorConnect;
end;
end;

procedure Tdm.OPenReport(pKN: string);
begin
    if ibqryReport.Active then ibqryReport.Close;
    if ibqryReportRight.Active then ibqryReportRight.Close;
    if ibqryREportArenda.Active then ibqryREportArenda.Close;
    if ibqryOWNERRIGHT.Active then ibqryOWNERRIGHT.Close;
    if ibqryOWNERARENDA.Active then ibqryOWNERARENDA.Close;

     ibqryReport.Params[0].Value := pKN; //pCadNUm;
     ibqryReport.Open;
     ibqryReportRight.Params[0].Value := ibqryReport.FieldByName('OUTLID').Value; //pCadNUm;
     ibqryReportRight.Open;
     ibqryREportArenda.Params[0].Value := ibqryReport.FieldByName('OUTLID').Value; //pCadNUm;
     ibqryREportArenda.Open;
     ibqryOWNERRIGHT.Params[0].Value := ibqryReportRight.FieldByName('OUTLRID').Value; //pCadNUm;
     ibqryOWNERRIGHT.Open;
     ibqryOWNERARENDA.Params[0].Value := ibqryREportArenda.FieldByName('OUTLRID').Value; //pCadNUm;
     ibqryOWNERARENDA.Open;
end;

end.
