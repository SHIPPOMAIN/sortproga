unit RegEditIEemul;

interface
uses
 System.Win.Registry;

 procedure RegEditHKEY_CURRENT_USER(const pAppName: string; pIE: integer = 10000);


implementation

procedure RegEditHKEY_CURRENT_USER(const pAppName: string; pIE: integer);
var
 _re: TRegistry;
begin
 _re:= TRegistry.Create;
 if _re.OpenKey('SOFTWARE\Microsoft\Internet Explorer\Main\FeatureControl\FEATURE_BROWSER_EMULATION',false) then
   _re.WriteInteger(pAppName,pIE);
 _re.Free;
end;

end.
