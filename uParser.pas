unit uParser;

interface
uses
 System.SysUtils,
 system.generics.collections;

type
{ TODO -c���������� : �������� ����� ��������� � ��. ������, � ���������� �� �� ����� }
 tAttributs = (aCadNum,aArea,aRaion,aSP,aKLH,aISP,aFISP,aKTG,aCADCOST,aGR1,AGR2,aGR3,aGR4,aGR5,aGR6,aGR7,aUG1,aUG2,aUG3,aUG4,aUG5,aUG6,aUG7,aRight,aSobst,aKUCH, aUFRS, aArenda, aPeriod,aASOBST);

var
 cKey: array[tAttributs.aCadNum..tAttributs.aASOBST] of string = ('����','��','���','��','���','���','����','���','�����','��1','��2','��3','��4','��5','��6','��7','��1','��2','��3','��4','��5','��6','��7','�����','����','���','����','������','���','�����');
 cCaptions: array[tAttributs.aCadNum..tAttributs.aASOBST] of string = ('����������� �����','�������','������������� �����','�������� ���������','������','��� ������������� ��','����������� �������������','��������� ������','����������� ���������', '�������������������� ������', '�����, ������� ���������.��������','����� ��� ��.-����.����-� � ��','����� ��� ������, �� ������������','����� ��� ���������� ���������','�����, ��������� ��� ��. ��������','������ �����','�����','������','��������','��������','����������� ����������','��������','������������ ������ ����������','��� �����','�����������','���.����','����','������','������ �������� ������','���������');
 cDBFields: array[tAttributs.aCadNum..tAttributs.aASOBST] of string = ('CADNUM','LAREA','RAION','SETTLE','KOLHOZ','LUSENAME','LFACTUAL_USE','CATEGORY','CADPRICE','AGROUP','AGROUP2','AGROUP3','AGROUP4','AGROUP5','AGROUP6', 'AGROUP7','ATYPE','ATYPE2','ATYPE3','ATYPE4','ATYPE5','ATYPE6','ATYPE7','RIGHTNAME','SOBNAME','KUCH','UFRS','ARENDA','PERIOD','ASOBNAME');


type
 tSortParser = class
   private
    fAttr: TList<tAttributs>;
    fWhere: TDictionary<tAttributs,string>;
   public
    constructor Create;
    destructor Destroy; override;
    procedure Parse(const Value: string);
    property Attr: TList<tAttributs> read fAttr;
    property Where: TDictionary<tAttributs,string> read fWhere;
 end;



implementation

const
 cWhere: string = '���';
 cDlm: string = ',';
 cDlmWhere: string = '=';
 cDlm2Where: string = ';';


{ tParser }

constructor tSortParser.Create;
begin
 self.fAttr:= TList<tAttributs>.Create;
 self.fWhere:= TDictionary<tAttributs,string>.Create;
end;

destructor tSortParser.Destroy;
begin
  self.fAttr.Free;
  self.fWhere.Destroy;
  inherited;
end;

procedure tSortParser.Parse(const Value: string);
var
 _val: TArray<string>;
 _attr: string;
 _where: string;
 _string: string;
 _att: tAttributs; //tAttributs;
begin
 self.fAttr.Clear;
 self.fWhere.Clear;


 //������ ������
 _val:= Value.Split(cWhere,ExcludeEmpty);
 _attr:= _val[0].Trim;    //select
 _where:= _val[1].Trim;   // where
 for _string in _attr.Split(cDlm,ExcludeEmpty) do
   for _att :=Low(tAttributs)  to  High(tAttributs) do
      if cKey[_att].Equals(_string) then
       begin
         self.fAttr.Add(_att);
         BREAK;
       end;

  for _string in _where.Split(cDlm2Where,ExcludeEmpty) do
  begin
  _val:= _string.Split(cDlmWhere,ExcludeEmpty);

  for _att :=Low(tAttributs)  to  High(tAttributs) do
   if cKey[_att].Equals(_val[0]) then begin
     self.fWhere.Add(_att,_val[1]);
     BREAK;
   end;
  end;



//  for _string in _where.Split(cDlm,ExcludeEmpty) do
//  _val:= _string.Split(cDlmWhere,ExcludeEmpty);
//  for _att in cKey do
//   if cKey[_att].Equals(_val[0]) then begin
//     self.fWhere.Add(_att,_val[1]);
//     BREAK;
//   end;

end;


 procedure test;
 var
  _str: string;
  _parse: tSortParser;
  _att: tAttributs;
 begin
  _str:= '���,����,��,��1,��1,��2,��3,��4,��5,��6,��7 ��� ���=(361000);����=36:10:000000;��=0,1000;��1=true,1;��1=true,0;��2=true,0;��3=true,0;��4=true,0;��5=true,0;��6=true,0;��7=true,0;';
 _parse:= tSortParser.Create;
 _parse.Parse(_str);

 //UI
  if _parse.Attr.Contains(tAttributs.aCadNum) then
   begin
     //dsfsdfsdf
   end;
  if _parse.fWhere.ContainsKey(tAttributs.aCadNum) then
  begin
  end;

  //SQL
  if _parse.Attr.Contains(tAttributs.aCadNum) then
   begin
     //dsfsdfsdf
   end;

//



 end;

end.

