unit uWorkShablon;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, VirtualTrees, uDBFlatAPI, Vcl.StdCtrls,
  Vcl.Buttons, Vcl.ExtCtrls, Vcl.Menus,
  uRedactorSHb, Vcl.Imaging.pngimage,UI.uGeneral;

type
  TForm5 = class(TForm)
    VirtTrView: TVirtualStringTree;
    mmo1: TMemo;
    pmShbl: TPopupMenu;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    N5: TMenuItem;
    N6: TMenuItem;
    N7: TMenuItem;
    N8: TMenuItem;
    N9: TMenuItem;
    pnlTools: TPanel;
    btnBack: TButton;
    btnNext: TButton;
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure VirtTrViewGetText(Sender: TBaseVirtualTree; Node: PVirtualNode;
      Column: TColumnIndex; TextType: TVSTTextType; var CellText: string);
    procedure VirtTrViewFreeNode(Sender: TBaseVirtualTree; Node: PVirtualNode);
    procedure VirtTrViewNodeClick(Sender: TBaseVirtualTree;
      const HitInfo: THitInfo);
    procedure btnCloseClick(Sender: TObject);
    procedure N3Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure VirtTrViewPaintText(Sender: TBaseVirtualTree;
      const TargetCanvas: TCanvas; Node: PVirtualNode; Column: TColumnIndex;
      TextType: TVSTTextType);
    procedure N2Click(Sender: TObject);
    procedure N9Click(Sender: TObject);
    procedure N7Click(Sender: TObject);
    procedure N4Click(Sender: TObject);
    procedure N6Click(Sender: TObject);
    procedure VirtTrViewNewText(Sender: TBaseVirtualTree; Node: PVirtualNode;
      Column: TColumnIndex; NewText: string);
    procedure VirtTrViewEditing(Sender: TBaseVirtualTree; Node: PVirtualNode;
      Column: TColumnIndex; var Allowed: Boolean);
    procedure btnBackClick(Sender: TObject);
    procedure btnNextClick(Sender: TObject);
  private
    fback: tNamedProc;
    fnext: tNamedProc;
    frqeditor: tNamedProc;

    function getQueryValue: string;
    procedure setQueryValue(const Value: string);
    procedure setBack(const Value: tNamedProc);
    procedure setNext(const Value: tNamedProc);

  public
    { Public declarations }
     _strSHName : string;
    _GR_ID : Double;
    function VirTrViewAddChild(RootNode: PVirtualNode; _bb:tGroup):PVirtualNode; overload;
    function VirTrViewAddChild(RootNode: PVirtualNode; _cc:tRequest):PVirtualNode; overload;
    procedure VirTrViewCreate();

    property back: tNamedProc read fback write setBack;
    property next: tNamedProc read fnext write setNext;
    property rqeditor: tNamedProc read frqeditor write frqeditor;
    property QueryValue: string read getQueryValue write setQueryValue;
  end;

 PVirtTrView = ^_TVirtTrViewRecord;//^TVirtTrViewRecord;
// TVirtTrViewRecord = record
//   ElementName: string;
//   ElementNumber: double;
//   group: tGroup;
//   request: tRequest;
// end;


 TElKind = (elGroup,elRequest);

 _TVirtTrViewRecord = record
   elkind: TElKind;// ������������� ����� ������� � ��������
   group: tGroup;
   request: tRequest;
 end;



var
  Form5: TForm5;
  _GroupArray : TArray<tGroup>;
  _ReguestArray : TArray<tREquest>;
  SelectedData: PVirtTrView;
  SelectedNode: pVirtualNode;
  _strRes : string;  // �������� �� ��������� �������� �������� �������

implementation
  uses {UMain,} uParser, uText, Units.Utils;
{$R *.dfm}

procedure TForm5.btnBackClick(Sender: TObject);
begin
 self.fback.go();
end;

procedure TForm5.btnCloseClick(Sender: TObject);
begin
 form5.Close;
end;

procedure TForm5.btnNextClick(Sender: TObject);
begin
 self.fnext.go();
end;

procedure TForm5.FormClose(Sender: TObject; var Action: TCloseAction);
begin
{ VirtTrView.Clear;
 mmo1.Lines.Clear;  }
 Application.MainForm.Close;
end;

procedure TForm5.FormCreate(Sender: TObject);
begin
  VirtTrView.NodeDataSize := SizeOf(_TVirtTrViewRecord);//SizeOf(TVirtTrViewRecord);
end;

function TForm5.VirTrViewAddChild(RootNode: PVirtualNode; _bb:tGroup):PVirtualNode;
var Data: PVirtTrView;  ChildNode:PVirtualNode;
begin
    ChildNode := VirtTrView.AddChild(RootNode);
    if not (vsInitialized in ChildNode.States) then  VirtTrView.ReinitNode(ChildNode, False);
    Data := VirtTrView.GetNodeData(ChildNode);
    Data.elkind := elGroup;
    Data.group:= _bb;

    Result := ChildNode;
end;


function TForm5.VirTrViewAddChild(RootNode: PVirtualNode; _cc:tRequest):PVirtualNode;
var Data: PVirtTrView;  ChildNode:PVirtualNode;
begin
    ChildNode := VirtTrView.AddChild(RootNode);
    if not (vsInitialized in ChildNode.States) then  VirtTrView.ReinitNode(ChildNode, False);
    Data := VirtTrView.GetNodeData(ChildNode);
    Data.elkind := elRequest;
    Data.request:= _cc;

    Result := ChildNode;
end;

procedure TForm5.VirTrViewCreate();
var
  RootNode, ChildNode, chNode2: PVirtualNode;
  I, j: Integer;
  Data: PVirtTrView;
begin
 VirtTrView.Clear;        // 04-07-16 ����� �� ������������� ���� ��� ������ ������ ������
 mmo1.Lines.Clear;        // 04-07-16

  _GroupArray := TDBGroupAPIScope.GetAllGroups;

  for I := 0 to High(_GroupArray) do
  begin
    ChildNode := VirTrViewAddChild(VirtTrView.RootNode,_GroupArray[i]);
    _ReguestArray := TDBRequestAPIScope.GetAllRequests(_GroupArray[i].ID);
    for j := 0 to High(_ReguestArray) do
    begin
      chNode2 := VirTrViewAddChild(ChildNode,_ReguestArray[j]);
    end;

  end;
end;

procedure TForm5.FormShow(Sender: TObject);
var
  RootNode, ChildNode, chNode2: PVirtualNode;
  I, j: Integer;
  Data: PVirtTrView;
begin
  VirTrViewCreate;
end;

function TForm5.getQueryValue: string;
begin
 if SelectedData.elkind = elRequest then
  result:= SelectedData.request.RequestVlaue;
end;

procedure TForm5.N2Click(Sender: TObject);
var m,m2 : Integer;  _RqAr : TArray<tRequest>;   ff : Boolean;
begin
  if SelectedData.elkind = elGroup then   // �� �������� ����� ������ ����
  begin
    _RqAr :=TDBRequestAPIScope.GetAllRequests(SelectedData.group.ID); // ��������� �� ������� ������
    if _RqAr = nil then
     m := MessageDlg(mdBeforeDelete,mtConfirmation,[mbYes,mbNo],0)
    else
     m2 := MessageDlg(mdBeforeDeleteNOtEmptyGroup,mtConfirmation,[mbYes,mbNo],0) ;
    if m=mrYes then
     begin
      ff := TDBGroupAPIScope.DeleteGroup(SelectedData.group.ID,false);
      if ff then VirtTrView.DeleteNode(SelectedNode);
     end;
    if m2=mrYes then
     begin
      ff := TDBGroupAPIScope.DeleteGroup(SelectedData.group.ID,true);
      if ff then VirtTrView.DeleteNode(SelectedNode);
     end;
  end;
end;

procedure TForm5.N3Click(Sender: TObject);
begin
 if SelectedData.elkind = elRequest then
 begin
  _strSHName := SelectedData.request.NameRequest;
  self.rqeditor.go();
 end;
end;

procedure TForm5.N4Click(Sender: TObject);
var _strVal : string;
begin
  _strVal := InputBox('�������� ����� ������', '������� ���:', '');

  if Trim(_strVal) <> '' then
  begin
   TDBGroupAPIScope.CreateNewGroup(_strVal);
   VirtTrView.Clear;
   mmo1.Lines.Clear;
   VirTrViewCreate;
  end;

end;

procedure TForm5.N6Click(Sender: TObject);
var _strVal, _strGR : string;
    nn : pVirtualNode;
    dd :PVirtTrView;
    _gr_id:Double;
    ff: TfRedactShbl;
    _ok : integer;
begin
  if SelectedData <> nil then
  BEGIN
    if SelectedData.elkind = elGroup then begin _strGR := SelectedData.group.Name; _gr_id := SelectedData.group.ID; end
    else
     begin
       nn := SelectedNode.Parent;
       dd:= VirtTrView.GetNodeData(Nn);
       _strGR := dd.group.Name;
       _gr_id := dd.group.ID;
     end;
    _strVal := InputBox('�������� ������ ������� � ������ '+_strGr, '������� ���:', '');
   if Trim(_strVal) <> '' then
    begin
    // ����� ���� ��� ����� ��� ������� - ������� ����� �������� ��������
     Self._strSHName := _strVal;
     Self._GR_ID := _gr_id;
     Self.rqeditor.go();

    end
  END
  else
   ShowMessage(rsBeforeCreateShablon);
end;

procedure TForm5.N7Click(Sender: TObject); // ������ ��� ������� � ������
var m,ii : Integer;  ff : Boolean; nn,nn2 : pVirtualNode;
begin
     m := MessageDlg(mdBeforeDeleteAllRequest,mtConfirmation,[mbYes,mbNo],0);
     if m=mrYes then
     begin
       if SelectedData.elkind = elRequest then
        ff := TDBGroupAPIScope.DeleteAllRequestfromGroup(SelectedData.request.groupid)
       else
        ff := TDBGroupAPIScope.DeleteAllRequestfromGroup(SelectedData.group.ID)  ;
       //if ff then VirtTrView.DeleteNode(SelectedNode);
       // ���� ������� ������ - �� ����� ��� ������, ���� ������ �����, �� ������� � �������� � ����� ��� ������
       if SelectedData.elkind = elGroup then
         begin
           for ii := 0 to SelectedNode.ChildCount-1 do
             begin
              nn := SelectedNode.FirstChild;
              VirtTrView.DeleteNode(nn);
             end;
         end
       else
        begin
         nn := SelectedNode.Parent;
         for ii := 0 to nn.ChildCount-1 do
          begin
            nn2 := nn.FirstChild;
            VirtTrView.DeleteNode(nn2);
          end;
        end;
     end;
end;

procedure TForm5.N9Click(Sender: TObject);    // ������� ��������� ������
var m : Integer;  ff : Boolean;
begin
 if SelectedData.elkind = elRequest then   // �� �������� ����� ������ ����
  begin
     m := MessageDlg(mdBeforeDeleteRequest,mtConfirmation,[mbYes,mbNo],0);
     if m=mrYes then
     begin
       ff := TDBRequestAPIScope.DeleteRequest(SelectedData.request.ID);
       if ff then VirtTrView.DeleteNode(SelectedNode);
     end;
  end;
end;

procedure TForm5.setBack(const Value: tNamedProc);
begin
 self.fback := Value;
 self.btnBack.Caption:= Self.fback.title;
end;

procedure TForm5.setNext(const Value: tNamedProc);
begin
  self.fnext := Value;
  if Assigned(Value.go) then begin
   self.btnNext.Caption:= Value.title;
   self.btnNext.Visible:= True;
  end else
   self.btnNext.Visible:= False;

end;

procedure TForm5.setQueryValue(const Value: string);
begin
{ TODO -c���� : ����� ���������� }
end;

procedure TForm5.VirtTrViewEditing(Sender: TBaseVirtualTree; Node: PVirtualNode;
  Column: TColumnIndex; var Allowed: Boolean);
var Data: PVirtTrView;
begin
 Data := Sender.GetNodeData(Node);
 if Assigned(Data) then
  if (Data.elkind = elGroup) or (Data.elkind = elRequest) then
    Allowed := true;
end;

procedure TForm5.VirtTrViewFreeNode(Sender: TBaseVirtualTree;  Node: PVirtualNode);
var Data: PVirtTrView;
begin
Data := Sender.GetNodeData(Node);
if Assigned(Data) then
   Finalize(Data^);

end;

procedure TForm5.VirtTrViewGetText(Sender: TBaseVirtualTree; Node: PVirtualNode;
  Column: TColumnIndex; TextType: TVSTTextType; var CellText: string);
var
Data: PVirtTrView;
begin
 Data := Sender.GetNodeData(Node);
 if Assigned(Data) then
    if Data.elkind = elGroup
      then CellText := Data.group.Name
      else CellText := Data.request.NameRequest;
end;

procedure TForm5.VirtTrViewNewText(Sender: TBaseVirtualTree; Node: PVirtualNode;
  Column: TColumnIndex; NewText: string);
var  Data: PVirtTrView;
begin
 if Length(NewText) = 0 then
    Exit;
 Data := Sender.GetNodeData(Node);
 if Assigned(Data) then
  if Data.elkind = elGroup then
  begin
     TDBGroupAPIScope.RenameGroup(Data.group.ID,NewText);
     Data.group.Name := NewText;
  end
  else
  begin
     TDBRequestAPIScope.RenameRequest(Data.request.ID,NewText);
     Data.request.NameRequest := NewText;
  end;

end;

procedure TForm5.VirtTrViewNodeClick(Sender: TBaseVirtualTree;
  const HitInfo: THitInfo);
var //Data: PVirtTrView;
    _parse: tSortParser;
    _att: tAttributs;
begin
  mmo1.Clear;
  SelectedData:= Sender.GetNodeData(HitInfo.HitNode);
  SelectedNode:= HitInfo.HitNode;
  if Assigned(SelectedData) then
   if SelectedData.elkind = elRequest then
    begin
      if SelectedData.request.RequestVlaue <> '' then
      begin
          _parse:= tSortParser.Create;
          _parse.Parse(SelectedData.request.RequestVlaue);
          mmo1.Lines.Add('�������� � ��������� ��������� �������:');
          mmo1.Lines.Add('');
          for _att in _parse.Attr do begin
           mmo1.Lines.Add(cCaptions[_att]);
          end;
          mmo1.Lines.Add('');
          mmo1.Lines.Add('�������� ������� �������:');
          mmo1.Lines.Add('');
          for _att in _parse.Where.Keys do begin
           mmo1.Lines.Add(requestscope.decode(_att,_parse.Where.Items[_att],2));
          end;
          _parse.Free;
      end;
    end;
end;

procedure TForm5.VirtTrViewPaintText(Sender: TBaseVirtualTree;
  const TargetCanvas: TCanvas; Node: PVirtualNode; Column: TColumnIndex;
  TextType: TVSTTextType);
var Data : PVirtTrView;
begin
 // if (Column = 0) and (TextType = ttStatic) then begin
    Data := VirtTrView.GetNodeData(Node);
    if Data.elkind = elGroup
      then  TargetCanvas.Font.Color := clWebDarkBlue
      else  TargetCanvas.Font.Color := clWebDarkOliveGreen;
//  end;
end;

end.
