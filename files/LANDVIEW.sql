CREATE OR ALTER VIEW LANDVIEW(
    LID,
    LPARENTID,
    CADNUM,
    LAREA,
    KID,
    KOLHOZ,
    SID,
    SETTLE,
    NID,
    RAION,
    LP,
    LCID,
    CATEGORY,
    CADPRICE,
    AGROUP,
    AGROUP2,
    AGROUP3,
    AGROUP4,
    AGROUP5,
    AGROUP6,
    AGROUP7,
    ATYPE,
    ATYPE2,
    ATYPE3,
    ATYPE4,
    ATYPE5,
    ATYPE6,
    ATYPE7,
    LUSEID,
    LUSENAME,
    LFACTUAL_USE)
AS
select l.id,
       l.parent_id,
       l.cad_num,
       l.area*k1.koef,

       kl.id,
       kl.name,

       ss.id,
       ss.name,

       nlp.id,
       nlp.name,

       l.local_prefix,
       lc.id, 
       lc.name,
       llc.cad_price* k2.koef ,

       AG.lgroup_id,
       AG.lgroup_id2,
       AG.lgroup_id3,
       AG.lgroup_id4,
       AG.lgroup_id5,
       AG.lgroup_id6,
       AG.lgroup_id7,

       AType.area_type1,
       AType.area_type2,
       AType.area_type3,
       AType.area_type4,
       AType.area_type5,
       AType.area_type6,
       AType.area_type7,

       put.id,
       put.name,
       lu.factual_use

from T_Land l
left join PR_UNIT_KOEF(l.UNIT_TYPE_ID, 21) k1 on ((l.ID=l.ID))
left join t_kolkhoz kl on (l.kolkhoz_id = kl.id)
left join t_settle ss on (ss.id = kl.settle_id)
left join t_name_local_prefix nlp on(nlp.local_prefix = l.local_prefix)

left join t_land_characteristic llc on (l.id = llc.land_id)
left join PR_UNIT_KOEF(llc.UNIT_TYPE_ID, 258) k2 on (llc.ID=llc.ID)
left join t_land_category lc on (llc.land_category_id = lc.id)

left join t_temp_groups ag on (l.id = ag.land_id)
left join t_temp_types atype on (l.id = atype.land_id)

left join t_land_use lu on (l.id = lu.land_id)
left join t_permitted_use_type put on (lu.permitted_use_type_id = put.id)

where
((l.status <> 3) or (l.status is null))
and ((l.identify <> 3) or (l.identify is null) )
and ((lu.identify <> 3) or (lu.identify is null) )
and ((lu.status <> 3) or (lu.status is null))
and ((llc.identify <> 3) or (llc.identify is null))
and ((llc.status <> 3) or (llc.status is null) )


--order by l.cad_num  */
;
